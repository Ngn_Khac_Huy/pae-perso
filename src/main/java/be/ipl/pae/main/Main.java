package be.ipl.pae.main;

import be.ipl.pae.biz.amenagements.AmenagementUcc;
import be.ipl.pae.biz.clients.ClientUcc;
import be.ipl.pae.biz.devis.DevisUcc;
import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.biz.photos.PhotoUcc;
import be.ipl.pae.biz.utilisateurs.UtilisateurUcc;
import be.ipl.pae.dal.amenagements.AmenagementDao;
import be.ipl.pae.dal.clients.ClientDao;
import be.ipl.pae.dal.devis.DevisDao;
import be.ipl.pae.dal.photos.PhotoDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.dal.travaux.TravailDao;
import be.ipl.pae.dal.utilisateurs.UtilisateurDao;
import be.ipl.pae.ihm.servlet.ClientServlet;
import be.ipl.pae.ihm.servlet.ConnexionServlet;
import be.ipl.pae.ihm.servlet.DevisServlet;
import be.ipl.pae.ihm.servlet.InscriptionServlet;
import be.ipl.pae.ihm.servlet.PhotosServlet;
import be.ipl.pae.ihm.servlet.UtilisateurServlet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import javax.servlet.http.HttpServlet;

public class Main {

  /**
   * Main qui lance notre application.
   * 
   * @param args arguments
   * @throws Exception exception correspondate
   */
  public static void main(String[] args) throws Exception {

    Properties props = new Properties();
    try {
      props.load(new FileInputStream("./prod.properties"));

    } catch (IOException error) {
      error.printStackTrace();
    }
    // Creation de la dependance sur base de ce qu'on trouve dans le fichier
    // properties
    Factory factory;
    UtilisateurUcc utilisateurUcc;
    UtilisateurDao utilisateurDao;
    ClientUcc clientUcc;
    ClientDao clientDao;
    DevisUcc devisUcc;
    DevisDao devisDao;
    AmenagementDao amenagementDao;
    AmenagementUcc amenagementUcc;
    TravailDao travailDao;
    PhotoDao photoDao;
    PhotoUcc photoUcc;
    DalServicesBackend dalServicesBackend;
    DalServices dalServices;
    String jwtSecret;
    try {
      jwtSecret = props.getProperty("JWTSECRET");

      // Factory
      Class<?> clsFacto = Class.forName(props.getProperty("be.ipl.pae.biz.Factory"));
      factory = (Factory) clsFacto.getDeclaredConstructor().newInstance();

      // Dal backend
      Class<?> clsDalBackend =
          Class.forName(props.getProperty("be.ipl.pae.dal.DalServices.DalServices"));
      dalServicesBackend =
          (DalServicesBackend) clsDalBackend.getDeclaredConstructor().newInstance();

      // Dal pour Ucc
      dalServices = (DalServices) dalServicesBackend;

      // Dao Utilisateur
      Class<?> clsDao =
          Class.forName(props.getProperty("be.ipl.pae.dal.utilisateurs.UtilisateurDao"));
      utilisateurDao =
          (UtilisateurDao) clsDao.getDeclaredConstructor(Factory.class, DalServicesBackend.class)
              .newInstance(factory, dalServicesBackend);

      // Ucc Utilisateur
      Class<?> clsUcc =
          Class.forName(props.getProperty("be.ipl.pae.biz.utilisateurs.UtilisateurUcc"));
      utilisateurUcc =
          (UtilisateurUcc) clsUcc.getDeclaredConstructor(UtilisateurDao.class, DalServices.class)
              .newInstance(utilisateurDao, dalServices);

      // Dao Client
      Class<?> clsDaoClient = Class.forName(props.getProperty("be.ipl.pae.dal.clients.ClientDao"));
      clientDao =
          (ClientDao) clsDaoClient.getDeclaredConstructor(Factory.class, DalServicesBackend.class)
              .newInstance(factory, dalServicesBackend);

      // Ucc Client
      Class<?> clsUccClient = Class.forName(props.getProperty("be.ipl.pae.biz.clients.ClientUcc"));
      clientUcc =
          (ClientUcc) clsUccClient.getDeclaredConstructor(ClientDao.class, DalServices.class)
              .newInstance(clientDao, dalServices);

      // Dao Travail
      Class<?> clsTravailDao =
          Class.forName(props.getProperty("be.ipl.pae.dal.travaux.TravailDao"));
      travailDao = (TravailDao) clsTravailDao.getDeclaredConstructor(DalServicesBackend.class)
          .newInstance(dalServicesBackend);


      // Dao Amenagement
      Class<?> clsAmenagementDao =
          Class.forName(props.getProperty("be.ipl.pae.dal.amenagements.AmenagementDao"));
      amenagementDao = (AmenagementDao) clsAmenagementDao
          .getDeclaredConstructor(Factory.class, DalServicesBackend.class)
          .newInstance(factory, dalServicesBackend);

      // Ucc Amenagement
      Class<?> clsAmenagementUcc =
          Class.forName(props.getProperty("be.ipl.pae.biz.amenagements.AmenagementUcc"));
      amenagementUcc = (AmenagementUcc) clsAmenagementUcc
          .getDeclaredConstructor(AmenagementDao.class, DalServices.class)
          .newInstance(amenagementDao, dalServices);

      // Dao Photo
      Class<?> clsPhotoDao = Class.forName(props.getProperty("be.ipl.pae.dal.photos.PhotoDao"));
      photoDao =
          (PhotoDao) clsPhotoDao.getDeclaredConstructor(Factory.class, DalServicesBackend.class)
              .newInstance(factory, dalServicesBackend);

      // Ucc Photo
      Class<?> clsPhototUcc = Class.forName(props.getProperty("be.ipl.pae.biz.photos.PhotoUcc"));
      photoUcc = (PhotoUcc) clsPhototUcc.getDeclaredConstructor(PhotoDao.class, DalServices.class)
          .newInstance(photoDao, dalServices);

      // Dao Devis
      Class<?> clsDevisDao = Class.forName(props.getProperty("be.ipl.pae.dal.devis.DevisDao"));
      devisDao =
          (DevisDao) clsDevisDao.getDeclaredConstructor(Factory.class, DalServicesBackend.class)
              .newInstance(factory, dalServicesBackend);

      // Ucc Devis
      Class<?> clsDevisUcc = Class.forName(props.getProperty("be.ipl.pae.biz.devis.DevisUcc"));
      devisUcc = (DevisUcc) clsDevisUcc
          .getDeclaredConstructor(DevisDao.class, UtilisateurDao.class, TravailDao.class,
              PhotoDao.class, DalServices.class)
          .newInstance(devisDao, utilisateurDao, travailDao, photoDao, dalServices);

    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException error) {
      throw new InternalError(error);
    }


    WebAppContext context = new WebAppContext();
    context.setMaxFormContentSize(10000000);
    context.setContextPath("/");
    context.setInitParameter("cacheControl", "no-store,no-cache,must-revalidate");

    // servlet gerant la connexion
    HttpServlet connexionServlet = new ConnexionServlet(utilisateurUcc, jwtSecret);
    context.addServlet(new ServletHolder(connexionServlet), "/connexion");

    // servlet gerant une inscription
    HttpServlet inscriptionServlet = new InscriptionServlet(utilisateurUcc, jwtSecret, factory);
    context.addServlet(new ServletHolder(inscriptionServlet), "/inscription");

    // servlet gerant des actions relatives a un utilisateur
    HttpServlet utilisateurServlet = new UtilisateurServlet(utilisateurUcc, jwtSecret);
    context.addServlet(new ServletHolder(utilisateurServlet), "/utilisateur");

    // servlet gerant des actions relatives a un client
    HttpServlet clientServlet = new ClientServlet(utilisateurUcc, jwtSecret, clientUcc, factory);
    context.addServlet(new ServletHolder(clientServlet), "/client/*");

    // servlet gerant des actions relatives a un devis
    HttpServlet devisServlet = new DevisServlet(devisUcc, jwtSecret, utilisateurUcc, clientUcc,
        factory, amenagementUcc, photoUcc);
    context.addServlet(new ServletHolder(devisServlet), "/devis/*");

    // servlet gerant des actions relatives aux photos
    HttpServlet photosServlet = new PhotosServlet(photoUcc, amenagementUcc);
    context.addServlet(new ServletHolder(photosServlet), "/photo/*");


    // servlet manipulant le contenu statique

    String portServer = props.getProperty("PortServer");
    HttpServlet statiContentServlet = new DefaultServlet(); //
    context.addServlet(new ServletHolder(statiContentServlet), "/");
    context.setResourceBase("www");
    Server server = new Server(Integer.parseInt(portServer));
    server.setHandler(context);
    server.start();
  }

}
