package be.ipl.pae.ihm.servlet;

import be.ipl.pae.biz.clients.ClientDto;
import be.ipl.pae.biz.clients.ClientUcc;
import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurUcc;
import be.ipl.pae.exception.BizException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.owlike.genson.Genson;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class ClientServlet extends HttpServlet {

  private String jwtSsecret;
  private UtilisateurUcc utilisateurUcc;
  private UtilisateurDto utilisateurDto;
  private ClientUcc clientUcc;
  private Factory bizFacto;
  private String jsonResponse;

  /**
   * Constructeur du Client servlet.
   * 
   * @param utilisateurUcc l'ucc pour acceder au uc de l'utilisateur
   * @param jwtSecret String pour dechiffrer le token
   * @param clientUcc l'ucc pour acceder au uc du client
   * @param bizFacto la factory pour acceder aux dto
   */
  public ClientServlet(UtilisateurUcc utilisateurUcc, String jwtSecret, ClientUcc clientUcc,
      Factory bizFacto) {
    this.utilisateurUcc = utilisateurUcc;
    this.clientUcc = clientUcc;
    this.jwtSsecret = jwtSecret;
    this.bizFacto = bizFacto;
  }

  // Methode permettant d'obtenir la liste des utilisateurs dont il faut confirmer l'inscription, et
  // la liste des clients auxquels ils seront potentiellement lies
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {
      String pathInfo = req.getRequestURI();
      String[] pathParts = pathInfo.split("/");
      String action = "";
      if (pathParts.length > 2) {
        action = pathParts[2];
      }
      String ltoken = req.getHeader("Authorization");
      if (ltoken != null) {
        int idUtilisateur = JWT.require(Algorithm.HMAC256(jwtSsecret)).build().verify(ltoken)
            .getClaim("idUtilisateur").asInt();
        utilisateurDto = utilisateurUcc.recupererUtilisateur(idUtilisateur);
        if (utilisateurDto.getStatut().equals("o")) {
          // effectue une recherche de clients
          if (action.equals("rechercherClients")) {
            String nom = req.getParameter("nom");
            String ville = req.getParameter("ville");
            String cp = req.getParameter("cp");
            int codePostal;
            if (cp.isEmpty()) {
              codePostal = 0;
            } else {
              codePostal = Integer.parseInt(cp);
            }

            List<ClientDto> clients = clientUcc.rechercherClients(nom, ville, codePostal);

            jsonResponse = "{\"success\":\"true\", \"data\":[";
            Genson genson = new Genson();
            int nbrClients = 1;
            for (ClientDto client : clients) {
              boolean estUtilisateur =
                  utilisateurUcc.verifierClientEstUtilisateur(client.getIdClient());

              // On remplace la 1ère occurence de la guillemet par estUtilisateur afin de pouvoir le
              // manipuler dans le "data" retourné
              jsonResponse += genson.serialize(client).replaceFirst("\"",
                  "\"estUtilisateur\":" + estUtilisateur + ",\"");
              if (nbrClients < clients.size()) {
                jsonResponse += ",";
              }
              nbrClients++;
            }
            jsonResponse += "]}";
            resp.setStatus(HttpServletResponse.SC_OK);
          } else {
            List<UtilisateurDto> utilisateurs = utilisateurUcc.recupererUtilisateursAConfirmer();
            List<ClientDto> clients = clientUcc.recupererTousLesClients();

            Genson genson = new Genson();
            String listeUtilisateurs = genson.serialize(utilisateurs);
            String listeClients = genson.serialize(clients);
            jsonResponse = "{\"success\":\"true\", \"listeU\":";
            jsonResponse += listeUtilisateurs;
            jsonResponse += ", \"listeC\":" + listeClients;
            jsonResponse += "}";
            resp.setStatus(HttpServletResponse.SC_OK);
          }
        } else {
          jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse : Pas ouvrier\"";
          resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }

      } else {
        jsonResponse = "{\"success\":\"false\", \"error\":\"Token invalide\"";
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      }

    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    } finally {
      // System.out.println("JSON generated :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);

    }
  }

  // Methode servant a introduire un nouveau client ou a confirmer l'inscription d'un utilisateur
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {

      String pathInfo = req.getRequestURI();
      String[] pathParts = pathInfo.split("/");
      String action = "";
      if (pathParts.length > 2) {
        action = pathParts[2];
      }
      String ltoken = req.getHeader("Authorization");

      if (ltoken != null) {

        int idUtilisateurConnecte = JWT.require(Algorithm.HMAC256(jwtSsecret)).build()
            .verify(ltoken).getClaim("idUtilisateur").asInt();
        utilisateurDto = utilisateurUcc.recupererUtilisateur(idUtilisateurConnecte);

        if (utilisateurDto.getStatut().equals("o")) {

          // ajoute le client si l'inscription est correcte
          if (action.equals("client")) {
            ClientDto clientDto = bizFacto.getClient();
            String nom = req.getParameter("nom");
            if (!nom.equals("") && nom.length() <= 100) {
              clientDto.setNom(nom);
            } else {
              throw new IllegalArgumentException("Champ nom invalide");
            }
            String prenom = req.getParameter("prenom");
            if (!prenom.equals("") && prenom.length() <= 30) {
              clientDto.setPrenom(prenom);
            } else {
              throw new IllegalArgumentException("Champ prenom invalide");
            }
            String rue = req.getParameter("rue");
            if (!rue.equals("") && rue.length() <= 100) {
              clientDto.setRue(rue);
            } else {
              throw new IllegalArgumentException("Champ rue invalide");
            }
            String numero = req.getParameter("numero");
            if (!numero.equals("")) {
              clientDto.setNumero(Integer.parseInt(numero));
            } else {
              throw new IllegalArgumentException("Champ numero invalide");
            }

            String boite = req.getParameter("boite");
            if (boite.length() <= 10) {
              clientDto.setBoite(boite);
            } else {
              throw new IllegalArgumentException("Champ boite invalide");
            }
            String cp = req.getParameter("cp");
            if (!cp.equals("")) {
              clientDto.setCp(Integer.parseInt(cp));
            } else {
              throw new IllegalArgumentException("Champ cp invalide");
            }
            String ville = req.getParameter("ville");
            // Verifie la presence d'au moins 1 lettre parmis a-zA-Z dans ville
            if (!ville.equals("") && Pattern.matches("[a-zA-Z]+", ville) && ville.length() <= 50) {
              clientDto.setVille(ville);
            } else {
              throw new IllegalArgumentException("Champ ville invalide");
            }
            String email = req.getParameter("email");
            // Verifie la presence d'un @ dans l'email
            if (Pattern.matches("..*[@]..*", email) && email.length() <= 100) {
              clientDto.setEmail(email);
            } else {
              throw new IllegalArgumentException("Champ email invalide");
            }
            String tel = req.getParameter("tel");
            // Verifie la presence d'au moins 1 chiffre entre 0-9 dans tel, et la potentielle
            // presence d'un symbole + precedemment
            if (Pattern.matches("[+]*[0-9]+", tel)) {
              clientDto.setTel(tel);
            } else {
              throw new IllegalArgumentException("Champ tel invalide");
            }

            clientUcc.introduireClient(clientDto);
            Genson genson = new Genson();
            jsonResponse = "{\"success\":\"true\", \"data\":";
            jsonResponse += genson.serialize(clientDto);
            jsonResponse += "}";
            resp.setStatus(HttpServletResponse.SC_OK);

            // lie le client a un utilisateur
          } else {
            int idClient = Integer.parseInt(req.getParameter("idClient"));
            int idUtilisateurALier = Integer.parseInt(req.getParameter("idUtilisateurALier"));
            UtilisateurDto utilisateurALier =
                utilisateurUcc.recupererUtilisateur(idUtilisateurALier);

            utilisateurUcc.confirmerInscription(utilisateurALier, idClient);

            jsonResponse = "{\"success\":\"true\", \"data\":\"Liaison faite\"}";
            resp.setStatus(HttpServletResponse.SC_OK);
          }
        } else {
          jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse : Pas ouvrier\"}";
          resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
      } else {
        jsonResponse = "{\"success\":\"false\", \"error\":\"Token invalide\"}";
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      }
    } catch (IllegalArgumentException exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":";
      jsonResponse += "\"" + exception.getMessage() + "\"";
      jsonResponse += "}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    } catch (BizException exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";

      if (exception.getTypeError().equals("409")) {
        resp.setStatus(HttpServletResponse.SC_CONFLICT);
      } else {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    } finally {
      // System.out.println("JSON generated :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }
}

