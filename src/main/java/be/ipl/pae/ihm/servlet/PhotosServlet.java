package be.ipl.pae.ihm.servlet;

import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.amenagements.AmenagementUcc;
import be.ipl.pae.biz.photos.PhotoDto;
import be.ipl.pae.biz.photos.PhotoUcc;
import be.ipl.pae.exception.BizException;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class PhotosServlet extends HttpServlet {

  private PhotoUcc photoUcc;
  private AmenagementUcc amenagementUcc;
  private String jsonResponse;
  private ArrayList<PhotoDto> listePhoto;

  /**
   * Constructeur du Devis Servlet.
   * 
   * @param photoUcc l'ucc pour acceder aux uc d'une photo
   * @param amenagementUcc l'ucc pour acceder aux uc de l'amenagement
   */
  public PhotosServlet(PhotoUcc photoUcc, AmenagementUcc amenagementUcc) {

    this.photoUcc = photoUcc;
    this.amenagementUcc = amenagementUcc;

  }

  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {

      String pathInfo = req.getRequestURI();
      String[] pathParts = pathInfo.split("/");
      String action = "";
      if (pathParts.length > 2) {
        action = pathParts[2];
      }
      // recupere les photos pas amenagement
      if (action.equals("amenagement")) {
        String amenagement = req.getParameter("amenagement");
        int idAmenagement = amenagementUcc.recupererAmenagement(amenagement).getId();
        listePhoto = photoUcc.recupererPhotosAmenagementVisible(idAmenagement);

        // recupere toutes les photos
      } else if (action.equals("toutesPhotos")) {
        listePhoto = photoUcc.recupererPhotosVisible();
      }
      jsonResponse = "{\"success\":\"true\", \"data\":";
      jsonResponse += "[";
      // Ajoute chaque photo dans la reponse json
      for (PhotoDto photo : listePhoto) {
        if (listePhoto.get(0).getIdPhoto() != photo.getIdPhoto()) {
          jsonResponse += ",";
        }
        jsonResponse += "{\"amenagement\": \""
            + amenagementUcc.recupererAmenagementParId(photo.getIdAmenagement()).getNom() + "\","
            + "\"photoData\":\"" + photo.getPhotoData() + " \"}";
      }
      jsonResponse += "]";
      jsonResponse += "}";
      resp.setStatus(HttpServletResponse.SC_OK);

    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":";
      jsonResponse += "\"" + exception.getMessage() + "\"";
      jsonResponse += "}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);


    } finally {
      // System.out.println("JSON generated :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {


      // verifie les données envoyées et ajoute la photo
      if (req.getParameterValues("photoData[]") == null
          || req.getParameterValues("photoData[]").length == 0) {
        throw new IllegalArgumentException("Pas de photo choisie");
      }


      if (req.getParameter("idDevis").equals("")) {
        throw new IllegalArgumentException("Devis introduit vide");
      }
      String idDevis = req.getParameter("idDevis");

      if (req.getParameterValues("amen[]") == null
          || req.getParameterValues("amen[]").length == 0) {
        throw new IllegalArgumentException("Pas d'amenagement choisie");
      }
      String[] amen = req.getParameterValues("amen[]");

      if (req.getParameterValues("properties[]") == null
          || req.getParameterValues("properties[]").length == 0) {
        throw new IllegalArgumentException("Pas de propriete choisie");
      }
      String[] properties = req.getParameterValues("properties[]");


      ArrayList<Boolean> favori = new ArrayList<Boolean>();
      ArrayList<Boolean> visible = new ArrayList<Boolean>();
      // recupere les proprotes de la photos pour mettre le boolean
      for (int i = 0; i < properties.length; i++) {
        if (i % 2 == 0) {
          favori.add(properties[i].equals("true"));
        } else {
          visible.add(properties[i].equals("true"));
        }
      }
      String[] photoData = req.getParameterValues("photoData[]");
      for (int i = 0; i < photoData.length; i++) {
        AmenagementDto amenagementDto = amenagementUcc.recupererAmenagement(amen[i]);
        photoUcc.ajoutPhotos(photoData[i], Integer.parseInt(idDevis), amenagementDto.getId(),
            favori.get(i), visible.get(i));
      }

      jsonResponse = "{\"success\":\"true\"}";
      resp.setStatus(HttpServletResponse.SC_OK);

    } catch (BizException | IllegalArgumentException exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    } finally {
      // System.out.println("JSON Photos Servlet :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }


}
