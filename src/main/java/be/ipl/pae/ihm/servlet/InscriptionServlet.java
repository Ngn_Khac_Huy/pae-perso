package be.ipl.pae.ihm.servlet;

import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurUcc;
import be.ipl.pae.exception.BizException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.owlike.genson.Genson;

import java.io.IOException;
import java.time.LocalDate;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class InscriptionServlet extends HttpServlet {

  private String jwtSsecret;
  private UtilisateurUcc utilisateurUcc;
  private UtilisateurDto utilisateurDto;
  private Factory bizFacto;

  private String jsonResponse;

  /**
   * Constructeur du inscription servlet.
   * 
   * @param utilisateurUcc l'ucc pour acceder aux uc de l'utilisateur
   * @param jwtSecret String pour dechiffrer le token
   * @param bizFacto la factory pour acceder aux dto
   */
  public InscriptionServlet(UtilisateurUcc utilisateurUcc, String jwtSecret, Factory bizFacto) {
    this.utilisateurUcc = utilisateurUcc;
    this.jwtSsecret = jwtSecret;
    this.bizFacto = bizFacto;

  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {
      utilisateurDto = bizFacto.getUtilisateur();


      String nom = req.getParameter("nom");
      if (!nom.equals("") && nom.length() <= 100) {
        utilisateurDto.setNom(nom);
      } else {
        throw new IllegalArgumentException("Champ Nom invalide");
      }
      String prenom = req.getParameter("prenom");
      if (!prenom.equals("") && prenom.length() <= 30) {
        utilisateurDto.setPrenom(prenom);
      } else {
        throw new IllegalArgumentException("Champ prénom invalide");
      }
      String ville = req.getParameter("ville");
      if (!ville.equals("") && Pattern.matches("[a-zA-Z]+", ville) && ville.length() <= 50) {
        utilisateurDto.setVille(ville);
      } else {
        throw new IllegalArgumentException("Champ ville invalide");
      }
      String email = req.getParameter("email");
      if (Pattern.matches("..*[@]..*", email) && email.length() <= 100) {
        utilisateurDto.setEmail(email);
      } else {
        throw new IllegalArgumentException("Adresse mail non valide");
      }
      String pseudo = req.getParameter("pseudo");
      if (!pseudo.equals("") && pseudo.length() <= 50) {
        utilisateurDto.setPseudo(pseudo);
      } else {
        throw new IllegalArgumentException("Champ Pseudo invalide");
      }
      String mdp = req.getParameter("password");
      if (!mdp.equals("") && mdp.length() <= 100) {
        utilisateurDto.setMdp(mdp);
      } else {
        throw new IllegalArgumentException("Champ Mot de passe invalide");
      }

      LocalDate date = LocalDate.now();
      utilisateurDto.setDateInscription(date);
      utilisateurDto.setStatut("u");

      utilisateurDto = utilisateurUcc.sinscrire(utilisateurDto);


      String ltoken = JWT.create().withClaim("ip", req.getRemoteAddr())
          .withClaim("idUtilisateur", utilisateurDto.getIdUtilisateur())
          .sign(Algorithm.HMAC256(jwtSsecret));

      Genson genson = new Genson();
      jsonResponse = "{\"success\":\"true\", \"token\":\"" + ltoken + "\", \"data\":";
      jsonResponse += genson.serialize(utilisateurDto);
      jsonResponse += "}";
      resp.setStatus(HttpServletResponse.SC_OK);

    } catch (IllegalArgumentException exception) {
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    } catch (BizException exception) {
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_CONFLICT);

    } catch (Exception exception) {
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    } finally {
      // System.out.println("JSON generated " + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);

    }
  }
}
