package be.ipl.pae.ihm.servlet;

import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.amenagements.AmenagementUcc;
import be.ipl.pae.biz.clients.ClientDto;
import be.ipl.pae.biz.clients.ClientUcc;
import be.ipl.pae.biz.devis.DevisDto;
import be.ipl.pae.biz.devis.DevisUcc;
import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.biz.photos.PhotoDto;
import be.ipl.pae.biz.photos.PhotoUcc;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurUcc;
import be.ipl.pae.exception.BizException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.owlike.genson.Genson;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class DevisServlet extends HttpServlet {

  private String jwtSsecret;
  private DevisUcc devisUcc;
  private DevisDto devisDto;

  private ClientUcc clientUcc;

  private UtilisateurDto utilisateurDto;
  private UtilisateurUcc utilisateurUcc;

  private PhotoUcc photoUcc;

  private Factory bizFacto;
  private AmenagementUcc amenagementUcc;

  private String jsonResponse;

  /**
   * Constructeur du Devis Servlet.
   * 
   * @param devisUcc l'ucc pour acceder au uc de du devis
   * @param jwtSecret String pour dechiffrer le token
   * @param utilisateurUcc l'ucc pour acceder au uc de l'utilisateur
   * @param clientUcc l'ucc pour acceder au uc du client
   * @param bizFacto la factory pour acceder aux dto
   * @param amenagementUcc l'ucc pour acceder au uc de l'amenagement
   */
  public DevisServlet(DevisUcc devisUcc, String jwtSecret, UtilisateurUcc utilisateurUcc,
      ClientUcc clientUcc, Factory bizFacto, AmenagementUcc amenagementUcc, PhotoUcc photoUcc) {

    this.devisUcc = devisUcc;
    this.photoUcc = photoUcc;
    this.jwtSsecret = jwtSecret;
    this.clientUcc = clientUcc;
    this.utilisateurUcc = utilisateurUcc;
    this.bizFacto = bizFacto;
    this.amenagementUcc = amenagementUcc;
  }

  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {
      String pathInfo = req.getRequestURI();
      String[] pathParts = pathInfo.split("/");
      String action = "";
      if (pathParts.length > 2) {
        action = pathParts[2];
      }

      Genson genson = new Genson();

      String token = req.getHeader("Authorization");
      // recupere la liste des aménagements
      if (action.equals("amenagement")) {
        List<AmenagementDto> amenagements = amenagementUcc.recupererListeAmenagements();
        List<String> listeA = new ArrayList<String>();
        for (AmenagementDto a : amenagements) {
          listeA.add(a.getNom());
        }

        String listeAmen = genson.serialize(listeA);

        jsonResponse = "{\"success\":\"true\", \"listeA\":";
        jsonResponse += listeAmen;
        jsonResponse += "}";
        resp.setStatus(HttpServletResponse.SC_OK);


        // recupere la liste des devis
      } else {

        if (token != null) {
          int idUtilisateur = JWT.require(Algorithm.HMAC256(jwtSsecret)).build().verify(token)
              .getClaim("idUtilisateur").asInt();
          utilisateurDto = utilisateurUcc.recupererUtilisateur(idUtilisateur);


          if (!utilisateurDto.getStatut().equals("u")) {

            // recupere les info detaillees d'un devis
            if (action.contentEquals("devisPlusInfo")) {
              int idDevis = Integer.parseInt(req.getParameter("idDevis"));
              DevisDto devisDto = devisUcc.recupererDevis(idDevis);

              // mets les amenagements en string
              List<AmenagementDto> listeAmenagements =
                  amenagementUcc.recupererListeAmenagementsParDevis(devisDto.getIdDevis());
              ArrayList<String> listeA = new ArrayList<String>();
              for (AmenagementDto a : listeAmenagements) {
                listeA.add(a.getNom());
              }

              jsonResponse = "{\"success\":\"true\", \"data\":";
              jsonResponse += "[" + genson.serialize(devisDto).replaceFirst("\"",
                  "\"amenagements\":" + genson.serialize(listeA) + "," + "\"photoFavorite2\":\""
                      + devisUcc.recupererPhoto(devisDto.getPhotoFavorite()) + "\",\"");
              jsonResponse += ",";
              ClientDto clientDto = clientUcc.recupererClient(devisDto.getIdClient());
              jsonResponse += genson.serialize(clientDto);
              jsonResponse += ",";
              // recupere les photos du devis
              ArrayList<PhotoDto> listePhotoDevis = photoUcc.recupererPhotosParDevis(idDevis);
              jsonResponse += genson.serialize(listePhotoDevis);
              jsonResponse += "]";
              jsonResponse += "}";
              resp.setStatus(HttpServletResponse.SC_OK);
            }
            // recupere la liste des devis
            if (action.contentEquals("rechercheDevis")) {
              String nom = null;
              if (!req.getParameter("nom").equals("")) {
                nom = req.getParameter("nom");
              }

              String amenagement = null;
              if (!req.getParameter("amenagement").equals("")) {
                amenagement = req.getParameter("amenagement");
              }

              int montantMin = 0;

              if (!req.getParameter("montantMin").equals("")) {
                montantMin = Integer.parseInt(req.getParameter("montantMin"));
              }

              int montantMax = 0;
              if (!req.getParameter("montantMax").equals("")) {
                montantMax = Integer.parseInt(req.getParameter("montantMax"));
              }

              Date date = null;
              if (!req.getParameter("date").equals("")) {
                date = Date.valueOf(req.getParameter("date"));
              }


              List<DevisDto> listeDevis =
                  devisUcc.voirDevis(idUtilisateur, montantMin, montantMax, date, amenagement, nom);
              jsonResponse = affichageDevis(listeDevis);
              resp.setStatus(HttpServletResponse.SC_OK);
            }
            // affiche les devis par client
            if (action.contentEquals("devisClient")) {
              int idClient = Integer.parseInt(req.getParameter("idClient"));
              List<DevisDto> devis = devisUcc.recupererDevisClient(idClient);
              jsonResponse = affichageDevis(devis);

              resp.setStatus(HttpServletResponse.SC_OK);
            }

          } else {
            jsonResponse = "{\"success\":\"false\", \"error\":\"Pas ouvrier ou client\"}";
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);

          }

        } else {
          jsonResponse = "{\"success\":\"false\", \"error\":\"Token invalide\"}";
          resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
      }

    } catch (BizException exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);


    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);


    } finally {
      // System.out.println("JSON generated :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }


  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {
      String chemin = req.getRequestURI();
      String[] pathParts = chemin.split("/");
      String action = "";
      if (pathParts.length > 2) {
        action = pathParts[2];
      }
      devisDto = bizFacto.getDevis();
      Genson genson = new Genson();

      String ltoken = req.getHeader("Authorization");
      if (ltoken != null) {
        int idUtilisateurConnecte = JWT.require(Algorithm.HMAC256(jwtSsecret)).build()
            .verify(ltoken).getClaim("idUtilisateur").asInt();
        utilisateurDto = utilisateurUcc.recupererUtilisateur(idUtilisateurConnecte);

        // ajoute un amenagement
        if (action.equals("amenagement")) {

          // Verifie que l'utilisateur connecte est bien un ouvrier
          if (utilisateurDto.getStatut().equals("o")) {

            if (!req.getParameter("amenagement").equals("")) {
              amenagementUcc.ajouterAmenagement(req.getParameter("amenagement"));
            } else {
              throw new IllegalArgumentException("Veuillez remplir le champ");
            }

            jsonResponse = "{\"success\":\"true\"";
            jsonResponse += "}";
            resp.setStatus(HttpServletResponse.SC_OK);
          } else {
            jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse : Pas ouvrier\"}";
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
          }

          // ajoute un devis
        } else {

          // Verifie que l'utilisateur connecte est bien un ouvrier
          if (utilisateurDto.getStatut().equals("o")) {
            devisDto.setEtat("Devis introduit");


            String date = req.getParameter("date");


            if (!req.getParameter("idClient").equals("")) {
              devisDto.setIdClient(Integer.parseInt(req.getParameter("idClient")));
            } else {
              throw new IllegalArgumentException("Client invalide");
            }
            if (!req.getParameter("date").equals("")) {
              devisDto.setDateDevis(LocalDate.parse(date));
            } else {
              throw new IllegalArgumentException("Veuillez choisir une date valide");
            }

            // Verifie que dateDebutTravaux suit le format "yyyy-mm-dd"
            if (!Pattern.matches("[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}", date)
                && !date.isEmpty()) {
              throw new IllegalArgumentException("Veuillez choisir une date valide");
            }

            if (!req.getParameter("montant").equals("")) {
              int montant = Integer.parseInt(req.getParameter("montant"));
              devisDto.setMontant(montant);
            } else {
              throw new IllegalArgumentException("Montant invalide");
            }

            if (!req.getParameter("duree").equals("")) {
              int duree = Integer.parseInt(req.getParameter("duree"));
              devisDto.setDuree(duree);
            } else {
              throw new IllegalArgumentException("duree invalide");
            }


            devisDto.setDateDebutTravaux(null);

            String[] tableaAmenagement = req.getParameterValues("nomAmenagement[]");
            ArrayList<String> listeAmenagement =
                new ArrayList<String>(Arrays.asList(tableaAmenagement));

            if (listeAmenagement.size() == 0) {
              throw new IllegalArgumentException("Veuillez choisir un amenagement");
            }



            devisDto.setPhotoFavorite(0);
            ArrayList<AmenagementDto> listeAmenDto = new ArrayList<AmenagementDto>();
            for (String nomAmenagement : listeAmenagement) {
              listeAmenDto.add(amenagementUcc.recupererAmenagement(nomAmenagement));
            }

            ArrayList<String> listeImage = new ArrayList<String>();
            String[] tableaImage = req.getParameterValues("photo[]");
            if (tableaImage != null) {
              listeImage = new ArrayList<String>(Arrays.asList(tableaImage));
            }


            devisUcc.introduireDevis(devisDto, listeAmenDto, listeImage);
            jsonResponse = "{\"success\":\"true\", \"devis\":";
            jsonResponse += genson.serialize(devisDto);
            jsonResponse += "}";
            resp.setStatus(HttpServletResponse.SC_OK);
          } else {
            jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse : Pas ouvrier\"}";
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
          }
        }

      } else {
        jsonResponse = "{\"success\":\"false\", \"error\":\"Token invalide\"}";
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      }


    } catch (BizException | IllegalArgumentException exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    } finally {
      // System.out.println("JSON devis servlet :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }

  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {
      // mets a jour l'etat et les dates si necessaire
      String strIdDevis = req.getParameter("id");
      String date = req.getParameter("date");

      String etat = req.getParameter("etat");
      if (!Pattern.matches("[0-9]+", strIdDevis)) {
        throw new IllegalArgumentException();
      }
      int idDevis = Integer.parseInt(strIdDevis);
      String token = req.getHeader("Authorization");
      if (token != null) {

        int idUtilisateur = JWT.require(Algorithm.HMAC256(jwtSsecret)).build().verify(token)
            .getClaim("idUtilisateur").asInt();
        utilisateurDto = utilisateurUcc.recupererUtilisateur(idUtilisateur);

        if (utilisateurDto.getStatut().equals("o")) {
          jsonResponse = "{\"success\":\"true\", \"data\":";
          boolean result = false;

          if (etat != null && !etat.equals("")) {
            result = devisUcc.modifierEtat(idDevis, etat);
          } else {
            if (date != null && !date.equals("")) {
              result = devisUcc.modifierDate(idDevis, date);

            } else {
              result = devisUcc.confirmerDevis(idDevis);

            }

          }
          jsonResponse += "\"" + String.valueOf(result) + " id " + idDevis + " date \"";
          jsonResponse += "}";
          resp.setStatus(HttpServletResponse.SC_OK);

        } else {
          jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse: pas ouvrier\"}";
          resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
      } else {
        jsonResponse = "{\"success\":\"false\", \"error\":\"Token invalide\"}";
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      }
    } catch (BizException | IllegalArgumentException exception) {

      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    } catch (Exception exception) {

      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);


    } finally {
      // System.out.println("JSON generated :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }



  private String affichageDevis(List<DevisDto> listeDevis) {
    String json = "{\"success\":\"true\", \"data\":";
    Genson genson = new Genson();

    json += "[";
    // Ajoute chaque devis dans la reponse json
    for (DevisDto d : listeDevis) {
      List<AmenagementDto> listeAmenagements =
          amenagementUcc.recupererListeAmenagementsParDevis(d.getIdDevis());
      ArrayList<String> listeA = new ArrayList<String>();
      for (AmenagementDto a : listeAmenagements) {
        listeA.add(a.getNom());
      }
      if (listeDevis.get(0).getIdDevis() != d.getIdDevis()) {
        json += ",";
      }
      ClientDto clientRecup = clientUcc.recupererClient(d.getIdClient());
      json += "{\"idDevis\":" + d.getIdDevis() + "," + "\"etat\":\"" + d.getEtat() + "\","
          + "\"amenagements\":" + genson.serialize(listeA) + "," + "\"dateDevis\":"
          + genson.serialize(d.getDateDevis()) + "," + "\"dateDebutTravaux\":"
          + genson.serialize(d.getDateDebutTravaux()) + "," + "\"montant\":" + d.getMontant() + ","
          + "\"duree\":" + d.getDuree() + "," + "\"nomClient\":\"" + clientRecup.getNom() + " "
          + clientRecup.getPrenom() + "\",\"photoFavorite\": \""
          + devisUcc.recupererPhoto(d.getPhotoFavorite()) + " \"}";
    }

    json += "]";
    json += "}";
    return json;

  }
}
