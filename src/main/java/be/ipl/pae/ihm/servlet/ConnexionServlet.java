package be.ipl.pae.ihm.servlet;

import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurUcc;
import be.ipl.pae.exception.BizException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.owlike.genson.Genson;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class ConnexionServlet extends HttpServlet {

  private String jwtSsecret;
  private UtilisateurUcc utilisateurUcc;
  private UtilisateurDto utilisateurDto;

  private String jsonResponse;

  /**
   * Constructeur du Connexionservlet.
   * 
   * @param utilisateurUcc l'ucc pour acceder aux uc de l'utilisateur
   * @param jwtSecret String pour dechiffrer le token
   */
  public ConnexionServlet(UtilisateurUcc utilisateurUcc, String jwtSecret) {
    this.utilisateurUcc = utilisateurUcc;
    this.jwtSsecret = jwtSecret;

  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {

      String pseudo = req.getParameter("pseudo");
      String mdp = req.getParameter("password");

      utilisateurDto = utilisateurUcc.seConnecter(pseudo, mdp);

      if (!utilisateurDto.getStatut().equals("u")) {
        String ltoken = JWT.create().withClaim("ip", req.getRemoteAddr())
            .withClaim("idUtilisateur", utilisateurDto.getIdUtilisateur())
            .sign(Algorithm.HMAC256(jwtSsecret));

        Genson genson = new Genson();
        jsonResponse = "{\"success\":\"true\", \"token\":\"" + ltoken + "\", \"data\":";
        jsonResponse += genson.serialize(utilisateurDto);
        jsonResponse += "}";
        resp.setStatus(HttpServletResponse.SC_OK);
      } else {
        jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse : Pas encore confirme\"}";
        resp.setStatus(HttpServletResponse.SC_FORBIDDEN);

      }



    } catch (BizException exception) {
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);



    } catch (Exception exception) {
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);


    } finally {
      // System.out.println("JSON generated " + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);
    }
  }

}
