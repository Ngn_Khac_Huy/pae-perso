package be.ipl.pae.ihm.servlet;

import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurUcc;
import be.ipl.pae.exception.BizException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.owlike.genson.Genson;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class UtilisateurServlet extends HttpServlet {

  private String jwtSsecret;
  private UtilisateurUcc utilisateurUcc;
  private UtilisateurDto utilisateurDto;

  private String jsonResponse;

  /**
   * Constructeur du Connexionservlet.
   * 
   * @param utilisateurUcc l'ucc pour acceder au uc de l'utilisateur
   * @param jwtSecret String pour dechiffrer le token
   */
  public UtilisateurServlet(UtilisateurUcc utilisateurUcc, String jwtSecret) {
    this.utilisateurUcc = utilisateurUcc;
    this.jwtSsecret = jwtSecret;

  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    try {

      String ltoken = req.getHeader("Authorization");
      if (ltoken != null) {
        int idUtilisateur = JWT.require(Algorithm.HMAC256(jwtSsecret)).build().verify(ltoken)
            .getClaim("idUtilisateur").asInt();
        utilisateurDto = utilisateurUcc.recupererUtilisateur(idUtilisateur);

        if (utilisateurDto.getStatut().equals("o")) {
          // effectue une recherche sur les utilisateurs inscrits
          String nom = req.getParameter("nom");
          String ville = req.getParameter("ville");

          List<UtilisateurDto> utilisateurs =
              utilisateurUcc.rechercherUtilisateursInscrits(nom, ville);

          jsonResponse = "{\"success\":\"true\", \"data\":";
          Genson genson = new Genson();
          jsonResponse += genson.serialize(utilisateurs);
          jsonResponse += "}";
          resp.setStatus(HttpServletResponse.SC_OK);
        } else {
          jsonResponse = "{\"success\":\"false\", \"error\":\"Acces refuse : Pas ouvrier\"";
          resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }

      } else {
        jsonResponse = "{\"success\":\"false\", \"error\":\"Token invalide\"";
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      }

    } catch (BizException exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

    } catch (Exception exception) {
      exception.printStackTrace();
      jsonResponse = "{\"success\":\"false\", \"error\":\"" + exception.getMessage() + "\"}";
      resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

    } finally {
      System.out.println("JSON Utilisateurs Servlet  :" + jsonResponse);
      resp.setContentType("application/json");
      resp.setCharacterEncoding("UTF-8");
      resp.getWriter().write(jsonResponse);

    }
  }

}
