package be.ipl.pae.exception;

@SuppressWarnings("serial")
public class DalException extends RuntimeException {

  public DalException() {
    super();
  }

  public DalException(String message) {
    super(message);
  }

}
