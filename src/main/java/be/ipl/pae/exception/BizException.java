package be.ipl.pae.exception;

@SuppressWarnings("serial")
public class BizException extends RuntimeException {

  private String typeError;

  public BizException() {
    super();
  }

  public BizException(String message) {
    super(message);
  }

  public String getTypeError() {
    return typeError;
  }

  public void setTypeError(String numero) {
    this.typeError = numero;
  }


}
