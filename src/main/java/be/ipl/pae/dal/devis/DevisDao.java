package be.ipl.pae.dal.devis;

import be.ipl.pae.biz.devis.DevisDto;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public interface DevisDao {

  /**
   * Recupere la liste des devis d'un utilisateur grace a son id dans la DB.
   * 
   * @param idUtilisateur id de l'utilisateur (entier)
   * @return la liste des devis
   */
  List<DevisDto> recupererDevisViaIdUtilisateur(int idUtilisateur);

  /**
   * Recupere la liste des devis d'un client grace a son id dans la DB.
   * 
   * @param idClient id d'un client (entier)
   * @return la liste des devis
   */
  List<DevisDto> recupererDevisClient(int idClient);

  /**
   * Recupere la liste de tout les devis dans la DB.
   * 
   * 
   * @return la liste des devis
   */
  List<DevisDto> recupererTousLesDevis();

  /**
   * Recupere un devis a une date specifique et avec l'id du client.
   * 
   * @param date specifique (Localdate)
   * @param idClient id d'un client (entier)
   * @return le devis correspondant
   */
  DevisDto recupererUnDevis(LocalDate date, int idClient);

  /**
   * Modifie l'etat d'un devis dans la DB grace a son id.
   * 
   * @param etat le nouvel etat du devis (entier)
   * @param idDevis id d'un client (entier)
   * @return true si l'etat a bien ete modifie
   */
  boolean modifierEtat(String etat, int idDevis);

  /**
   * ajoute la date du debut des travaux d'un devis dans la DB grace a son id.
   * 
   * @param idDevis id d'un client (entier)
   * @param dateDebutTravaux la date du debut des travaux (entier)
   * @return true si l'etat a bien ete modifie
   */
  boolean ajoutDate(int idDevis, LocalDate dateDebutTravaux);

  /**
   * Introduit un nouveau devis dans la DB.
   * 
   * @param devis le devis a introduire (devisDto)
   * @return true si le devis a bien ete introduit, false sinon
   */
  boolean introduireDevis(DevisDto devis);

  /**
   * Recupere un devis en fonction de son id.
   * 
   * @param idDevis id d'un devis (entier)
   * @return le devis correspondant
   */
  DevisDto recupererDevis(int idDevis);

  /**
   * Renvoi une liste de tout les devis selon 5 criteres de recherches.
   * 
   * @param nom le du client (entier)
   * @param amenagement l'amenagement du devis (String)
   * @param montantMin le montant minimum du devis (entier)
   * @param montantMax le montant minimum du devis (entier)
   * @param date date du devis (Date)
   * @return la liste des devis
   */
  List<DevisDto> rechercherAdmin(String nom, String amenagement, int montantMin, int montantMax,
      Date date);

  /**
   * Renvoi une liste de tout les devis d'un utilisateur selon 5 criteres de recherches.
   * 
   * @param idUtilisateur l'id de l'utilisateur (entier)
   * @param nom le du client (entier)
   * @param amenagement l'amenagement du devis (String)
   * @param montantMin le montant minimum du devis (entier)
   * @param montantMax le montant minimum du devis (entier)
   * @return date date du devis (Date)
   */
  List<DevisDto> rechercherUtilisateur(int idUtilisateur, String nom, String amenagement,
      int montantMin, int montantMax, Date date);
}
