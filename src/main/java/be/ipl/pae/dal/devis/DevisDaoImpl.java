package be.ipl.pae.dal.devis;

import be.ipl.pae.biz.devis.DevisDto;
import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.exception.DalException;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DevisDaoImpl implements DevisDao {

  private DalServicesBackend dalservices;
  private Factory bizFacto;

  /**
   * Constructeur devisDao.
   * 
   * @param bizFacto factory pour recuperer le dto
   * @param dalservices dalservices pour le PS
   */
  public DevisDaoImpl(Factory bizFacto, DalServicesBackend dalservices) {
    this.bizFacto = bizFacto;
    this.dalservices = dalservices;
  }

  @Override
  public List<DevisDto> recupererDevisViaIdUtilisateur(int idUtilisateur) {
    List<DevisDto> listRet = new ArrayList<>();

    PreparedStatement ps = dalservices.getPreparedStatement("SELECT "
        + "de.id_devis, de.etat, de.date_devis, de.date_debut_travaux, de.montant, de.duree, "
        + "de.id_client, de.photo_favorite FROM projet.utilisateurs ut,projet.clients"
        + " cl, projet.devis de WHERE ut.id_utilisateur=? AND"
        + " ut.id_client=cl.id_client AND de.id_client=cl.id_client;");
    try {

      ps.setInt(1, idUtilisateur);

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        DevisDto devisDto = bizFacto.getDevis();
        devisDto = setUnDevis(devisDto, rs);

        listRet.add(devisDto);
      }
    } catch (SQLException exception) {
      // Auto-generated catch block
      exception.printStackTrace();
      throw new DalException();
    }

    return listRet;
  }

  @Override
  public List<DevisDto> recupererDevisClient(int idClient) {
    List<DevisDto> listRet = new ArrayList<>();

    PreparedStatement ps = dalservices.getPreparedStatement("SELECT "
        + "de.id_devis, de.etat, de.date_devis, de.date_debut_travaux, de.montant, de.duree, "
        + "de.id_client, de.photo_favorite FROM projet.clients cl, projet.devis de "
        + "WHERE cl.id_client=? AND " + "de.id_client=cl.id_client;");
    try {
      ps.setInt(1, idClient);

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        DevisDto devisDto = bizFacto.getDevis();
        devisDto = setUnDevis(devisDto, rs);

        listRet.add(devisDto);
      }
    } catch (SQLException exception) {
      // Auto-generated catch block
      exception.printStackTrace();
      throw new DalException();
    }
    return listRet;
  }


  @Override
  public List<DevisDto> recupererTousLesDevis() {
    List<DevisDto> listRet = new ArrayList<>();

    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_devis, etat, date_devis, date_debut_travaux, montant, duree, "
            + "id_client, photo_favorite FROM projet.devis;  ");
    try {
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        DevisDto devisDto = bizFacto.getDevis();
        devisDto = setUnDevis(devisDto, rs);

        listRet.add(devisDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return listRet;
  }

  @Override
  public boolean modifierEtat(String etat, int idDevis) {
    PreparedStatement ps = dalservices
        .getPreparedStatement("UPDATE projet.devis " + "SET etat = ? " + "WHERE id_devis=?;");
    try {
      ps.setString(1, etat);
      ps.setInt(2, idDevis);
      ps.executeUpdate();
      return true;

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
  }


  @Override
  public boolean ajoutDate(int idDevis, LocalDate dateDebutTravaux) {
    PreparedStatement ps = dalservices.getPreparedStatement(
        "UPDATE projet.devis " + "SET date_debut_travaux = ? " + "WHERE id_devis = ?;");
    try {
      ps.setDate(1, Date.valueOf(dateDebutTravaux));
      ps.setInt(2, idDevis);
      ps.executeUpdate();
      return true;

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
  }


  @Override
  public boolean introduireDevis(DevisDto devis) {
    PreparedStatement ps = dalservices.getPreparedStatement("INSERT INTO projet.devis("
        + "id_devis, etat, date_devis, date_debut_travaux, montant, duree, id_client)"
        + " VALUES(DEFAULT,?,?,?,?,?,?)");
    try {
      ps.setString(1, devis.getEtat());
      ps.setDate(2, Date.valueOf(devis.getDateDevis()));
      ps.setDate(3, null);
      ps.setDouble(4, devis.getMontant());
      ps.setInt(5, devis.getDuree());
      ps.setInt(6, devis.getIdClient());
      ps.executeUpdate();

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return true;
  }

  @Override
  public DevisDto recupererUnDevis(LocalDate date, int idClient) {
    DevisDto devisDto = bizFacto.getDevis();

    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_devis, etat, date_devis, date_debut_travaux, montant, duree, "
            + "id_client, photo_favorite FROM projet.devis WHERE date_devis=? AND id_client=?;");
    try {
      ps.setDate(1, Date.valueOf(date));
      ps.setInt(2, idClient);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        setUnDevis(devisDto, rs);

      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return devisDto;
  }

  private DevisDto setUnDevis(DevisDto devisDto, ResultSet rs) {
    try {
      devisDto.setIdDevis(rs.getInt(1));
      devisDto.setEtat(rs.getString(2));
      if (rs.getDate(3) == null) {
        devisDto.setDateDevis(null);
      } else {
        devisDto.setDateDevis(rs.getDate(3).toLocalDate());
      }
      if (rs.getDate(4) == null) {
        devisDto.setDateDebutTravaux(null);
      } else {
        devisDto.setDateDebutTravaux(rs.getDate(4).toLocalDate());
      }
      devisDto.setMontant(rs.getFloat(5));
      devisDto.setDuree(rs.getInt(6));
      devisDto.setIdClient(rs.getInt(7));
      devisDto.setPhotoFavorite(rs.getInt(8));

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return devisDto;
  }



  @Override
  public DevisDto recupererDevis(int idDevis) {
    DevisDto devisDto = bizFacto.getDevis();

    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_devis, etat, date_devis, date_debut_travaux, montant, duree, "
            + "id_client, photo_favorite FROM projet.devis WHERE id_devis=?;");
    try {
      ps.setInt(1, idDevis);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        devisDto.setIdDevis(rs.getInt(1));
        devisDto.setEtat(rs.getString(2));
        devisDto.setDateDevis(rs.getDate(3).toLocalDate());
        if (rs.getDate(4) == null) {
          devisDto.setDateDebutTravaux(null);
        } else {
          devisDto.setDateDebutTravaux(rs.getDate(4).toLocalDate());
        }
        devisDto.setMontant(rs.getFloat(5));
        devisDto.setDuree(rs.getInt(6));
        devisDto.setIdClient(rs.getInt(7));
        devisDto.setPhotoFavorite(rs.getInt(8));
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return devisDto;
  }

  @Override
  public List<DevisDto> rechercherAdmin(String nom, String amenagement, int montantMin,
      int montantMax, Date date) {
    // variable local
    List<DevisDto> listRet = new ArrayList<>();
    boolean whereIsEmpty = true;
    char[] tablepresent = new char[5];
    int tailleTable = 0;

    String from = "FROM projet.devis de ";

    String where = "WHERE ";

    // si le parametre n est pas null on complete notre sql
    if (nom != null) {
      tablepresent[tailleTable] = 'n';
      tailleTable++;
      from += ",projet.clients cl ";
      if (!whereIsEmpty) {
        where += "AND de.id_client= cl.id_client AND LOWER(cl.nom) like ? ";
      } else {
        where += "de.id_client= cl.id_client AND LOWER(cl.nom) like ? ";
      }
      whereIsEmpty = false;
    }

    if (amenagement != null) {
      tablepresent[tailleTable] = 'a';
      tailleTable++;
      from += ",projet.types_Amenagements ta, projet.travaux tr ";
      if (!whereIsEmpty) {
        where += "AND de.id_devis = tr.id_devis AND tr.id_amenagement=ta.id_amenagement"
            + " AND LOWER(ta.nom) like ? ";
      } else {
        where += "de.id_devis = tr.id_devis AND tr.id_amenagement=ta.id_amenagement"
            + " AND LOWER(ta.nom) like ? ";
      }
      whereIsEmpty = false;
    }

    if (date != null) {
      tablepresent[tailleTable] = 'd';
      tailleTable++;
      if (!whereIsEmpty) {
        where += "AND de.date_devis = ? ";
      } else {
        where += "de.date_devis = ? ";
      }
      whereIsEmpty = false;
    }


    if (montantMax != 0) {
      tablepresent[tailleTable] = 'm';
      tailleTable++;
      if (!whereIsEmpty) {
        where += "AND de.montant >=? AND de.montant<=? ";
      } else {
        where += "de.montant >=? AND de.montant<=? ";
      }
      whereIsEmpty = false;
    }

    String sql = "SELECT distinct de.id_devis, de.etat, de.date_devis,"
        + " de.date_debut_travaux, de.montant, de.duree,de.id_client, de.photo_favorite ";
    sql += from;
    sql += where;

    PreparedStatement ps = dalservices.getPreparedStatement(sql);


    try {
      // set les differentes variable du sql
      for (int i = 0; i < tailleTable; i++) {
        if (tablepresent[i] == 'n') {
          ps.setString(i + 1, "%" + nom.toLowerCase() + "%");
        } else {
          if (tablepresent[i] == 'a') {
            ps.setString(i + 1, amenagement.toLowerCase());
          } else {
            if (tablepresent[i] == 'd') {
              ps.setDate(i + 1, date);
            } else {
              ps.setInt(i + 1, montantMin);
              ps.setInt(i + 2, montantMax);
            }
          }
        }

      }

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {

        DevisDto devisDto = bizFacto.getDevis();
        devisDto = setUnDevis(devisDto, rs);
        listRet.add(devisDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return listRet;
  }


  @Override
  public List<DevisDto> rechercherUtilisateur(int idUtilisateur, String nom, String amenagement,
      int montantMin, int montantMax, Date date) {

    // variable local
    List<DevisDto> listRet = new ArrayList<>();
    char[] tablepresent = new char[4];
    int tailleTable = 0;

    String from = "FROM projet.devis de, projet.utilisateurs ut, projet.clients cl ";

    String where = "WHERE ut.id_client = cl.id_client  AND cl.id_client= de.id_client "
        + "AND ut.id_utilisateur=? ";

    // si le parametre n est pas null on complete notre sql
    if (nom != null) {
      tablepresent[tailleTable] = 'n';
      tailleTable++;
      where += "AND LOWER(cl.nom) like ? ";
    }

    if (amenagement != null) {
      tablepresent[tailleTable] = 'a';
      tailleTable++;
      from += ",projet.types_Amenagements ta, projet.travaux tr ";
      where += "AND de.id_devis = tr.id_devis AND tr.id_amenagement=ta.id_amenagement "
          + " AND LOWER(ta.nom) like ? ";
    }

    if (date != null) {
      tablepresent[tailleTable] = 'd';
      tailleTable++;
      where += "AND de.date_devis = ? ";
    }
    // important de mettre a la fin
    if (montantMax != 0) {
      tablepresent[tailleTable] = 'm';
      tailleTable++;
      where += "AND de.montant >=? AND de.montant<=? ";

    }
    String sql = "SELECT distinct de.id_devis, de.etat, de.date_devis, "
        + " de.date_debut_travaux, de.montant, de.duree,de.id_client, de.photo_favorite ";
    sql += from;
    sql += where;

    PreparedStatement ps = dalservices.getPreparedStatement(sql);


    try {
      // set les differentes variable du sql

      ps.setInt(1, idUtilisateur);

      for (int i = 0; i < tailleTable; i++) {
        if (tablepresent[i] == 'n') {
          ps.setString(i + 2, "%" + nom.toLowerCase() + "%");
        } else {
          if (tablepresent[i] == 'a') {
            ps.setString(i + 2, amenagement.toLowerCase());
          } else {
            if (tablepresent[i] == 'd') {
              ps.setDate(i + 2, date);
            } else {
              ps.setInt(i + 2, montantMin);
              ps.setInt(i + 3, montantMax);
            }
          }
        }
      }

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {

        DevisDto devisDto = bizFacto.getDevis();
        devisDto = setUnDevis(devisDto, rs);
        listRet.add(devisDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return listRet;
  }
}


