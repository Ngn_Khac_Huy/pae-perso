package be.ipl.pae.dal.amenagements;

import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.exception.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AmenagementDaoImpl implements AmenagementDao {

  private DalServicesBackend dalservices;
  private Factory bizFacto;

  public AmenagementDaoImpl(Factory bizFacto, DalServicesBackend dalservices) {
    this.bizFacto = bizFacto;
    this.dalservices = dalservices;
  }

  @Override
  public List<AmenagementDto> recupererListeAmenagements() {
    List<AmenagementDto> amenagements = new ArrayList<AmenagementDto>();
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_amenagement, nom, description" + " FROM projet.types_amenagements");

    try {
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        AmenagementDto amenagementDto = bizFacto.getAmenagement();

        amenagementDto.setId(rs.getInt(1));
        amenagementDto.setNom(rs.getString(2));
        amenagementDto.setDesc(rs.getString(3));

        amenagements.add(amenagementDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return amenagements;
  }

  @Override
  public AmenagementDto recupererAmenagement(String nom) {
    AmenagementDto amenagementDto = bizFacto.getAmenagement();
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_amenagement, description" + " FROM projet.types_amenagements WHERE nom=?");

    try {
      ps.setString(1, nom);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        amenagementDto.setId(rs.getInt(1));
        amenagementDto.setNom(nom);
        amenagementDto.setDesc(rs.getString(2));
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    if (amenagementDto.getId() == 0) {
      return null;
    }
    return amenagementDto;
  }

  @Override
  public AmenagementDto recupererAmenagementParId(int id) {
    AmenagementDto amenagementDto = bizFacto.getAmenagement();
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "nom, description" + " FROM projet.types_amenagements WHERE id_amenagement=?");

    try {
      ps.setInt(1, id);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        amenagementDto.setId(id);
        amenagementDto.setNom(rs.getString(1));
        amenagementDto.setDesc(rs.getString(2));
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    if (amenagementDto.getId() == 0) {
      return null;
    }
    return amenagementDto;
  }

  @Override
  public List<AmenagementDto> recupererListeAmenagementsParDevis(int idDevis) {
    List<AmenagementDto> amenagements = new ArrayList<AmenagementDto>();
    PreparedStatement ps =
        dalservices.getPreparedStatement("SELECT " + "projet.types_amenagements.id_amenagement, p"
            + "rojet.types_amenagements.nom, projet.types_amenagements.description"
            + " FROM projet.types_amenagements, projet.devis, projet.travaux "
            + "WHERE projet.devis.id_devis=projet.travaux.id_devis "
            + "AND projet.travaux.id_amenagement=projet.types_amenagements.id_amenagement "
            + "AND projet.devis.id_devis=?");

    try {
      ps.setInt(1, idDevis);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        AmenagementDto amenagementDto = bizFacto.getAmenagement();

        amenagementDto.setId(rs.getInt(1));
        amenagementDto.setNom(rs.getString(2));
        amenagementDto.setDesc(rs.getString(3));

        amenagements.add(amenagementDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return amenagements;
  }

  @Override
  public boolean ajouterAmenagement(String nom) {
    PreparedStatement ps = dalservices.getPreparedStatement("INSERT INTO projet.types_amenagements("
        + "id_amenagement, nom, description)" + " VALUES(DEFAULT,?,?)");

    try {
      ps.setString(1, nom);
      ps.setString(2, nom);

      ps.executeUpdate();

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return true;
  }


}
