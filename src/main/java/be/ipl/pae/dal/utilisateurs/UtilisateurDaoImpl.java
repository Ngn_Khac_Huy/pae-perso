package be.ipl.pae.dal.utilisateurs;

import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.exception.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UtilisateurDaoImpl implements UtilisateurDao {

  private DalServicesBackend dalservices;
  private Factory bizFacto;

  public UtilisateurDaoImpl(Factory bizFacto, DalServicesBackend dalservices) {
    this.bizFacto = bizFacto;
    this.dalservices = dalservices;
  }

  private void setUnUtilisateur(UtilisateurDto utilisateurDto, ResultSet rs) {
    try {

      utilisateurDto.setIdUtilisateur(rs.getInt(1));
      utilisateurDto.setEmail(rs.getString(2));
      utilisateurDto.setMdp(rs.getString(3));
      utilisateurDto.setNom(rs.getString(4));
      utilisateurDto.setPrenom(rs.getString(5));
      utilisateurDto.setVille(rs.getString(6));
      utilisateurDto.setStatut(rs.getString(7));
      utilisateurDto.setDateInscription(rs.getDate(8).toLocalDate());
      if (rs.getString(9) == null) {
        utilisateurDto.setIdClient(0);
      } else {
        utilisateurDto.setIdClient(rs.getInt(9));
      }
      utilisateurDto.setPseudo(rs.getString(10));

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
  }

  @Override
  public UtilisateurDto recupererUtilisateurViaPseudo(String pseudo) {
    UtilisateurDto utilisateurDto = bizFacto.getUtilisateur();
    PreparedStatement ps;
    ps = dalservices
        .getPreparedStatement("SELECT " + "id_utilisateur, email, mdp, nom, prenom, ville, statut,"
            + " date_inscription, id_client, pseudo" + " FROM projet.utilisateurs WHERE pseudo=?");
    try {
      ps.setString(1, pseudo);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        setUnUtilisateur(utilisateurDto, rs);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    if (utilisateurDto.getPrenom() == null) {
      return null;
    }
    return utilisateurDto;
  }

  @Override
  public UtilisateurDto recupererUtilisateurViaIdUtilisateur(int idUtilisateur) {
    UtilisateurDto utilisateurDto = bizFacto.getUtilisateur();
    PreparedStatement ps = dalservices
        .getPreparedStatement("SELECT " + "id_utilisateur, email, mdp, nom, prenom, ville, statut,"
            + " date_inscription, id_client, pseudo"
            + " FROM projet.utilisateurs WHERE id_utilisateur=?");
    try {
      ps.setInt(1, idUtilisateur);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        setUnUtilisateur(utilisateurDto, rs);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    if (utilisateurDto.getPrenom() == null) {
      return null;
    }
    return utilisateurDto;
  }

  @Override
  public boolean inscrireUtilisateur(UtilisateurDto dto) {

    PreparedStatement ps = dalservices.getPreparedStatement("INSERT INTO projet.utilisateurs("
        + "id_utilisateur, email, mdp, nom, prenom, ville, statut,"
        + " date_inscription, id_client, pseudo)" + " VALUES(DEFAULT,?,?,?,?,?,?,?,NULL,? )");
    try {
      // DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
      ps.setString(1, dto.getEmail());
      ps.setString(2, dto.getMdp());
      ps.setString(3, dto.getNom());
      ps.setString(4, dto.getPrenom());
      ps.setString(5, dto.getVille());
      ps.setString(6, dto.getStatut());
      ps.setDate(7, java.sql.Date.valueOf(dto.getDateInscription()));
      ps.setString(8, dto.getPseudo());
      ps.executeUpdate();

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return true;
  }

  @Override
  public List<UtilisateurDto> recupererUtilisateursAConfirmer() {
    List<UtilisateurDto> utilisateurs = new ArrayList<UtilisateurDto>();
    PreparedStatement ps = dalservices
        .getPreparedStatement("SELECT " + "id_utilisateur, email, mdp, nom, prenom, ville, statut,"
            + " date_inscription, id_client, pseudo"
            + " FROM projet.utilisateurs WHERE id_client IS NULL AND statut=?");

    try {
      ps.setObject(1, (char) 'u');
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        UtilisateurDto utilisateurDto = bizFacto.getUtilisateur();
        setUnUtilisateur(utilisateurDto, rs);
        utilisateurs.add(utilisateurDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return utilisateurs;
  }

  @Override
  public boolean lierClient(UtilisateurDto utilisateur, int idClient) {
    PreparedStatement ps = dalservices.getPreparedStatement(
        "UPDATE " + "projet.utilisateurs" + " SET id_client=?, statut=? WHERE id_utilisateur=?");
    try {
      ps.setInt(1, idClient);
      ps.setString(2, "c");
      ps.setInt(3, utilisateur.getIdUtilisateur());
      ps.executeUpdate();
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return true;
  }

  @Override
  public UtilisateurDto verifierClientEstUtilisateur(int idClient) {
    UtilisateurDto utilisateurDto = bizFacto.getUtilisateur();
    PreparedStatement ps = dalservices.getPreparedStatement("SELECT "
        + "id_utilisateur, email, mdp, nom, prenom, ville, statut,"
        + " date_inscription, id_client, pseudo" + " FROM projet.utilisateurs WHERE id_client=?");

    try {
      ps.setInt(1, idClient);
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        setUnUtilisateur(utilisateurDto, rs);
      }

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return utilisateurDto;
  }

  @Override
  public List<UtilisateurDto> rechercherUtilisateursInscrits(String nom, String ville) {
    List<UtilisateurDto> utilisateurs = new ArrayList<UtilisateurDto>();
    String sql = "SELECT " + "id_utilisateur, email, mdp, nom, prenom, ville, statut,"
        + " date_inscription, id_client, pseudo" + " FROM projet.utilisateurs";
    if (!nom.isEmpty()) {
      sql += " WHERE LOWER(nom) like ?";
    }

    if (!ville.isEmpty()) {
      if (!nom.isEmpty()) {
        sql += " AND LOWER(ville) like ?";
      } else {
        sql += " WHERE LOWER(ville) like ?";
      }
    }
    PreparedStatement ps = dalservices.getPreparedStatement(sql);
    try {
      int positionSuivante = 1;
      if (!nom.isEmpty()) {
        ps.setString(positionSuivante, "%" + nom.toLowerCase() + "%");
        positionSuivante++;
      }
      if (!ville.isEmpty()) {
        ps.setString(positionSuivante, "%" + ville.toLowerCase() + "%");
      }
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        UtilisateurDto utilisateurDto = bizFacto.getUtilisateur();
        setUnUtilisateur(utilisateurDto, rs);
        utilisateurs.add(utilisateurDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return utilisateurs;
  }
}
