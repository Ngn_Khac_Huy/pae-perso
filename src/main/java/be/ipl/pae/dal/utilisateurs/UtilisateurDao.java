package be.ipl.pae.dal.utilisateurs;

import be.ipl.pae.biz.utilisateurs.UtilisateurDto;

import java.util.List;

public interface UtilisateurDao {

  /**
   * Recupere l'utilisateur correpondant au pseudo de l'utilisateur dans la DB.
   * 
   * @param pseudo Le pseudo de l'utilisateur
   * @return UtilisateurDTO si l'utilisateur existe dans la DB; null dans le cas contraire
   */
  UtilisateurDto recupererUtilisateurViaPseudo(String pseudo);

  /**
   * Recupere l'utilisateur correpondant à l'id de l'utilisateur dans la DB.
   * 
   * @param idUtilisateur L'id de l'utilisateur
   * @return UtilisateurDTO si l'utilisateur existe dans la DB; null dans le cas contraire
   */
  UtilisateurDto recupererUtilisateurViaIdUtilisateur(int idUtilisateur);

  /**
   * Inscrit un nouvel utilisateur dans le DB et renvoi un utilisateurDTO.
   * 
   * @param dto Le dto correspondant a l'utilisateur
   */
  boolean inscrireUtilisateur(UtilisateurDto dto);

  /**
   * Lie un utilisateur a un client.
   * 
   * @param utilisateur L'utilisateur qui doit etre lie au client
   * @param idClient L'id du client a lier a l'utilisateur
   * @return True si l'operation a reussi ; false dans le cas contraire
   */
  boolean lierClient(UtilisateurDto utilisateur, int idClient);

  /**
   * Recupere la liste de tous les utilisateurs presents dans la DB.
   * 
   * @return Une liste des utilisateurs
   */
  List<UtilisateurDto> recupererUtilisateursAConfirmer();

  /**
   * Recupere un utilisateur grace a un id de client.
   * 
   * @param idClient l'id du client (entier)
   * @return l'utilisateur en question
   */
  UtilisateurDto verifierClientEstUtilisateur(int idClient);

  /**
   * renvoi tout les utilisateurs en fonction d'un nom d'utilisateur ou d'un ville.
   * 
   * @param nom nom de l'utilisateur (String)
   * @param ville la ville de l'utilisateur (String)
   * @return la liste des utilisateur renvoyes
   */
  List<UtilisateurDto> rechercherUtilisateursInscrits(String nom, String ville);
}
