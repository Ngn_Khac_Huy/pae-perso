package be.ipl.pae.dal.clients;

import be.ipl.pae.biz.clients.ClientDto;
import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.exception.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoImpl implements ClientDao {

  private DalServicesBackend dalservices;
  private Factory bizFacto;

  public ClientDaoImpl(Factory bizFacto, DalServicesBackend dalservices) {
    this.bizFacto = bizFacto;
    this.dalservices = dalservices;
  }

  @Override
  public List<ClientDto> recupererTousLesClients() {
    List<ClientDto> clients = new ArrayList<ClientDto>();
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_client, nom, prenom, rue, numero, boite, cp, ville, email, tel"
            + " FROM projet.clients");

    try {
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        ClientDto clientDto = bizFacto.getClient();

        clientDto = setUnClient(clientDto, rs);

        clients.add(clientDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return clients;
  }

  @Override
  public ClientDto recupererClient(int idClient) {
    ClientDto clientDto = bizFacto.getClient();
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_client, nom, prenom, rue, numero, boite, cp, ville, email, tel"
            + " FROM projet.clients WHERE id_client=?");

    try {
      ps.setInt(1, idClient);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {

        clientDto = setUnClient(clientDto, rs);

      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return clientDto;
  }

  @Override
  public boolean inscrireClient(ClientDto clDto) {
    PreparedStatement ps = dalservices.getPreparedStatement(
        "INSERT INTO projet.clients(" + "id_client, nom, prenom, rue, numero, boite, cp,ville,"
            + " email, tel)" + " VALUES(DEFAULT,?,?,?,?,?,?,?,?,? )");
    try {

      ps.setString(1, clDto.getNom());
      ps.setString(2, clDto.getPrenom());
      ps.setString(3, clDto.getRue());
      ps.setInt(4, clDto.getNumero());
      if (!clDto.getBoite().equals("")) {
        ps.setString(5, clDto.getBoite());
      } else {
        ps.setNull(5, java.sql.Types.INTEGER);
      }

      ps.setInt(6, clDto.getCp());
      ps.setString(7, clDto.getVille());
      ps.setString(8, clDto.getEmail());
      ps.setString(9, clDto.getTel());
      ps.executeUpdate();

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return true;
  }

  @Override
  public boolean verifPresenceClient(String email) {
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "id_client, nom, prenom, rue, numero, boite, cp, ville, email, tel"
            + " FROM projet.clients WHERE email=?");

    try {
      ps.setString(1, email);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {


        return true;


      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return false;
  }


  private ClientDto setUnClient(ClientDto clientDto, ResultSet rs) {

    try {

      clientDto.setIdClient(rs.getInt(1));
      clientDto.setNom(rs.getString(2));
      clientDto.setPrenom(rs.getString(3));
      clientDto.setRue(rs.getString(4));
      clientDto.setNumero(rs.getInt(5));
      clientDto.setBoite(rs.getString(6));
      clientDto.setCp(rs.getInt(7));
      clientDto.setVille(rs.getString(8));
      clientDto.setEmail(rs.getString(9));
      clientDto.setTel(rs.getString(10));

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return clientDto;
  }

  @Override
  public List<ClientDto> rechercherClients(String nom, String ville, int cp) {
    List<ClientDto> clients = new ArrayList<ClientDto>();
    boolean wherePasVide = false;
    String sql = "SELECT " + "id_client, nom, prenom, rue, numero, boite, cp, ville, email, tel"
        + " FROM projet.clients";
    if (!nom.isEmpty()) {
      sql += " WHERE LOWER(nom) like ?";
      wherePasVide = true;
    }

    if (!ville.isEmpty()) {
      if (wherePasVide) {
        sql += " AND LOWER(ville) like ?";
      } else {
        sql += " WHERE LOWER(ville) like ?";
      }
    }

    if (cp != 0) {
      if (wherePasVide) {
        sql += " AND cp=?";
      } else {
        sql += " WHERE cp=?";
      }
    }
    PreparedStatement ps = dalservices.getPreparedStatement(sql);
    try {
      int position = 1;
      if (!nom.isEmpty()) {
        ps.setString(position, "%" + nom.toLowerCase() + "%");
        position++;
      }
      if (!ville.isEmpty()) {
        ps.setString(position, "%" + ville.toLowerCase() + "%");
        position++;
      }
      if (cp != 0) {
        ps.setInt(position, cp);
      }

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        ClientDto clientDto = bizFacto.getClient();

        clientDto = setUnClient(clientDto, rs);

        clients.add(clientDto);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
    return clients;
  }

}
