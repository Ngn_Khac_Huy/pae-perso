package be.ipl.pae.dal.services;

public interface DalServices {

  void startTransaction();

  void rollbackTransaction();

  void commitTransaction();

}
