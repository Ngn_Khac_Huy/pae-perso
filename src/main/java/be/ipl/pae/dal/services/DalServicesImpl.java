package be.ipl.pae.dal.services;

import be.ipl.pae.exception.DalException;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class DalServicesImpl implements DalServicesBackend, DalServices {

  private String login;
  private String mdp;
  private String url;
  private ThreadLocal<Connection> connection;

  /**
   * Constructeur qui initialie les donnes avec injections.
   */
  public DalServicesImpl() {
    Properties props = new Properties();
    try {
      props.load(new FileInputStream("./prod.properties"));
    } catch (IOException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    login = props.getProperty("login");
    mdp = props.getProperty("mdp");
    url = props.getProperty("URL");
    connection = new ThreadLocal<Connection>();

    try {
      Class.forName("org.postgresql.Driver");
    } catch (ClassNotFoundException exception) {
      throw new DalException("Driver PostgreSQL manquant !");
    }

  }

  @Override
  public PreparedStatement getPreparedStatement(String sql) {
    try {
      return connection.get().prepareStatement(sql);
    } catch (SQLException exception) {
      throw new DalException();
    }
  }

  @Override
  public void startTransaction() {
    try {
      Connection conn = DriverManager.getConnection(url, login, mdp);
      conn.setAutoCommit(false);
      connection.set(conn);

    } catch (SQLException exception) {
      throw new DalException("Erreur start transaction !");

    }

  }

  @Override
  public void rollbackTransaction() {
    try {
      Connection conn = connection.get();
      conn.rollback();
      conn.close();
      connection.remove();
    } catch (SQLException exception) {
      throw new DalException("Erreur rollback transaction !");

    }

  }

  @Override
  public void commitTransaction() {
    try {
      Connection conn = connection.get();
      if (conn == null) {
        return;
      }
      conn.commit();
      conn.close();
      connection.remove();
    } catch (SQLException exception) {
      throw new DalException("Erreur rollback transaction !");

    }

  }

}
