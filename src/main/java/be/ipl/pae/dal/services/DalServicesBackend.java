package be.ipl.pae.dal.services;

import java.sql.PreparedStatement;

public interface DalServicesBackend {

  /**
   * Permet d'avoir un PS.
   * 
   * @param sql La requete SQL
   * @return un Prepared statement de la requete sql
   */
  PreparedStatement getPreparedStatement(String sql);



}
