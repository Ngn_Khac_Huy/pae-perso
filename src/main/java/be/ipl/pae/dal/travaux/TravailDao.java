package be.ipl.pae.dal.travaux;

public interface TravailDao {

  /**
   * Introduit un nouveau travail dans la DB.
   * 
   * @param idDevis id du devis (entier)
   * @param idAmenagement id de l'amenagement (entier)
   * @return true si le travail a bien ete introduit
   */
  boolean introduireTravail(int idDevis, int idAmenagement);
}
