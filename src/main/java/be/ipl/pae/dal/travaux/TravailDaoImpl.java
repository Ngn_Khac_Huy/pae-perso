package be.ipl.pae.dal.travaux;


import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.exception.DalException;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TravailDaoImpl implements TravailDao {

  private DalServicesBackend dalservices;
  

  public TravailDaoImpl(DalServicesBackend dalservices) {
    this.dalservices = dalservices;
  }

  @Override
  public boolean introduireTravail(int idDevis, int idAmenagement) {
    PreparedStatement ps = dalservices.getPreparedStatement("INSERT INTO projet.travaux("
        + "id_travail, id_devis, id_amenagement)" + " VALUES(DEFAULT,?,?)");
    try {
      ps.setInt(1, idDevis);
      ps.setInt(2, idAmenagement);
      ps.executeUpdate();

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return true;
  }
}
