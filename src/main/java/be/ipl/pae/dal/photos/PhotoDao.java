package be.ipl.pae.dal.photos;

import be.ipl.pae.biz.photos.PhotoDto;

import java.util.ArrayList;

public interface PhotoDao {

  /**
   * Recupere la liste de toutes les photos visibles presentes dans la DB.
   * 
   * 
   * @return Une liste des photos
   */
  ArrayList<PhotoDto> recupererPhotosVisible();

  /**
   * Recupere la liste de toutes les photos en fonction d'un type d'amenagement.
   * 
   * @param idAmenagement id de l'amenagement (entier)
   * @return Une liste des photos
   */
  ArrayList<PhotoDto> recupererPhotosVisibleParAmenagement(int idAmenagement);

  /**
   * Ajoute une image a un devis.
   * 
   * @param idDevis id du devis (entier)
   * @param image donnees de l'image (String)
   * @param idAmenagement id de l'amenagement de la photo (entier)
   * @param estVisible indique si la photo est visible sur l'acceuil du site (boolean)
   * @return Id de la photo
   */
  int ajoutImage(int idDevis, String image, int idAmenagement, boolean estVisible);

  /**
   * Recupere une photo sous forme de chaine de caractere.
   * 
   * @param idPhoto id de la photo (entier)
   * @return Chaine de caractere representant la photo
   */
  String recupererPhoto(int idPhoto);

  /**
   * Recupere la liste de toutes les photos pour un devis specifique.
   * 
   * @param idDevis id du devis (entier)
   * @return Une liste de photos
   */
  ArrayList<PhotoDto> recupererPhotosParDevis(int idDevis);

  /**
   * Indique qu'une photo est la favorite pour un devis.
   * 
   * @param idPhoto id de la photo (entier)
   * @param idDevis id du devis (entier)
   * @return True si la photo a ete indique comme favorite dans la DB
   */
  boolean ajouterPhotoFav(int idPhoto, int idDevis);



}
