package be.ipl.pae.dal.photos;

import be.ipl.pae.biz.factory.Factory;
import be.ipl.pae.biz.photos.PhotoDto;
import be.ipl.pae.dal.services.DalServicesBackend;
import be.ipl.pae.exception.DalException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PhotoDaoImpl implements PhotoDao {

  private Factory bizFacto;
  private DalServicesBackend dalservices;

  public PhotoDaoImpl(Factory bizFacto, DalServicesBackend dalservices) {
    this.bizFacto = bizFacto;
    this.dalservices = dalservices;
  }

  private void setUnePhoto(PhotoDto photo, ResultSet rs) {
    try {

      photo.setIdPhoto(rs.getInt(1));
      photo.setPhotoData(rs.getString(2));
      photo.setIdDevis(rs.getInt(3));
      photo.setIdAmenagement(rs.getInt(4));
      photo.setEstVisible(rs.getBoolean(5));

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
  }

  @Override
  public ArrayList<PhotoDto> recupererPhotosVisibleParAmenagement(int idAmenagement) {
    ArrayList<PhotoDto> listePhoto = new ArrayList<>();

    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT ph.id_photo, ph.photo,ph.id_devis,ph.id_amenagement,ph.est_visible "
            + " FROM projet.photos ph, projet.types_amenagements am "
            + " WHERE ph.id_amenagement=am.id_amenagement AND ph.id_amenagement=? "
            + "AND est_visible=true");

    try {
      ps.setInt(1, idAmenagement);
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PhotoDto photo = bizFacto.getPhoto();
        setUnePhoto(photo, rs);
        listePhoto.add(photo);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return listePhoto;
  }

  @Override
  public ArrayList<PhotoDto> recupererPhotosVisible() {
    ArrayList<PhotoDto> listePhoto = new ArrayList<>();

    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT ph.id_photo, ph.photo,ph.id_devis,ph.id_amenagement,ph.est_visible "
            + " FROM projet.photos ph, projet.types_amenagements am "
            + " WHERE ph.id_amenagement=am.id_amenagement AND est_visible=true");

    try {
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PhotoDto photo = bizFacto.getPhoto();
        setUnePhoto(photo, rs);
        listePhoto.add(photo);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return listePhoto;
  }

  @Override
  public int ajoutImage(int idDevis, String image, int idAmenagement, boolean estVisible) {
    int idPhoto = 0;
    PreparedStatement ps = dalservices.getPreparedStatement(
        "INSERT INTO projet.photos(" + "id_photo, photo, id_devis, id_amenagement, est_visible)"
            + " VALUES(DEFAULT,?,?,?,?)" + "RETURNING id_photo;");

    try {
      ps.setString(1, image);
      ps.setInt(2, idDevis);
      if (idAmenagement == 0) {
        ps.setNull(3, java.sql.Types.INTEGER);
      } else {
        ps.setInt(3, idAmenagement);
      }

      ps.setBoolean(4, estVisible);

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        idPhoto = rs.getInt(1);
      }

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return idPhoto;
  }

  @Override
  public String recupererPhoto(int idPhoto) {

    String photo = "";
    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT " + "photo FROM projet.devis, projet.photos WHERE id_photo=?");
    try {
      ps.setInt(1, idPhoto);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        photo = rs.getString(1);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return photo;

  }

  @Override
  public ArrayList<PhotoDto> recupererPhotosParDevis(int idDevis) {
    ArrayList<PhotoDto> listePhoto = new ArrayList<>();

    PreparedStatement ps = dalservices.getPreparedStatement(
        "SELECT ph.id_photo, ph.photo,ph.id_devis,ph.id_amenagement,ph.est_visible "
            + " FROM projet.photos ph WHERE ph.id_devis=?");

    try {
      ps.setInt(1, idDevis);
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        PhotoDto photo = bizFacto.getPhoto();
        setUnePhoto(photo, rs);
        listePhoto.add(photo);
      }
    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }

    return listePhoto;
  }

  @Override
  public boolean ajouterPhotoFav(int idPhoto, int idDevis) {
    PreparedStatement ps = dalservices.getPreparedStatement(
        "UPDATE projet.devis " + "SET photo_favorite = ? " + "WHERE id_devis=?;");
    try {

      ps.setInt(1, idPhoto);
      ps.setInt(2, idDevis);
      ps.executeUpdate();
      return true;

    } catch (SQLException exception) {
      exception.printStackTrace();
      throw new DalException();
    }
  }
}


