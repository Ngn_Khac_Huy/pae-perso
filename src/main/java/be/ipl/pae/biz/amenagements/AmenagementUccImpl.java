package be.ipl.pae.biz.amenagements;

import be.ipl.pae.dal.amenagements.AmenagementDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.exception.BizException;

import java.util.List;

public class AmenagementUccImpl implements AmenagementUcc {

  private DalServices dalServices;
  private AmenagementDao amenagementDao;

  /**
   * Constructeur ucc.
   * 
   * @param amenagementDao le dao de l'amenagement
   * @param dalServices le dalservices pour les transaction
   */
  public AmenagementUccImpl(AmenagementDao amenagementDao, DalServices dalServices) {
    this.dalServices = dalServices;
    this.amenagementDao = amenagementDao;
  }


  @Override
  public List<AmenagementDto> recupererListeAmenagements() {
    try {
      dalServices.startTransaction();
      return amenagementDao.recupererListeAmenagements();

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }


  @Override
  public AmenagementDto recupererAmenagement(String nom) {
    try {
      dalServices.startTransaction();
      AmenagementDto amenagement = amenagementDao.recupererAmenagement(nom);
      if (amenagement == null) {
        BizException bizE = new BizException("Amenagement non existant");
        bizE.setTypeError("400");
        throw bizE;
      }
      return amenagement;
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }


  @Override
  public List<AmenagementDto> recupererListeAmenagementsParDevis(int idDevis) {
    try {
      dalServices.startTransaction();
      return amenagementDao.recupererListeAmenagementsParDevis(idDevis);

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }


  @Override
  public AmenagementDto recupererAmenagementParId(int id) {
    try {
      dalServices.startTransaction();
      AmenagementDto amenagement = amenagementDao.recupererAmenagementParId(id);
      if (amenagement == null) {
        BizException bizE = new BizException("Amenagement non existant");
        bizE.setTypeError("400");
        throw bizE;
      }
      return amenagement;
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }


  @Override
  public boolean ajouterAmenagement(String nom) {
    try {
      dalServices.startTransaction();
      return amenagementDao.ajouterAmenagement(nom);
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }

  }

}
