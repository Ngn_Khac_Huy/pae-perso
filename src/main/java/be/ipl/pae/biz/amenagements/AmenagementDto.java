package be.ipl.pae.biz.amenagements;

public interface AmenagementDto {

  String getNom();

  int getId();

  String getDesc();

  void setNom(String nom);

  void setId(int id);

  void setDesc(String desc);

}
