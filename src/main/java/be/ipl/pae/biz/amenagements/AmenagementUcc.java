package be.ipl.pae.biz.amenagements;

import java.util.List;

public interface AmenagementUcc {

  /**
   * Recupere la liste de tous les amenagements presents dans la DB.
   * 
   * @return Une liste des amenagements
   */
  List<AmenagementDto> recupererListeAmenagements();

  /**
   * Recupere la liste de tous les amenagements presents dans la DB pour un devis specifique.
   * 
   * @param idDevis id du client (entier)
   * @return Une liste des amenagements
   */
  List<AmenagementDto> recupererListeAmenagementsParDevis(int idDevis);

  /**
   * Recupere un amenagement grace a son nom.
   * 
   * @param nom le nom de l'amenagement (string)
   * @return Un amenagement specifique
   */
  AmenagementDto recupererAmenagement(String nom);

  /**
   * Recupere un amenagement grace a son id.
   * 
   * @param id l'id du client (entier)
   * @return Une liste des amenagements
   */
  AmenagementDto recupererAmenagementParId(int id);

  /**
   * Ajoute un nouvel amenagement dans la db.
   * 
   * @param nom le nom du nouveau type d'amenagement (String)
   * @return true si tout s'est bien passe, false sinon
   */
  boolean ajouterAmenagement(String nom);

}

