package be.ipl.pae.biz.amenagements;

public class AmenagementImpl implements AmenagementDto {

  private String nom;
  private String desc;
  private int id;

  public AmenagementImpl() {}

  /**
   * Constructeur amenagement.
   * 
   * @param nom le nom de l amenagement
   * @param desc la desc de l amenagement
   * @param id l id de l amenagement
   */

  public AmenagementImpl(String nom, String desc, int id) {
    this.nom = nom;
    this.desc = desc;
    this.id = id;
  }

  @Override
  public String getNom() {
    return this.nom;
  }

  @Override
  public int getId() {
    return this.id;
  }

  @Override
  public String getDesc() {
    return this.desc;
  }

  @Override
  public void setNom(String nom) {
    this.nom = nom;

  }

  @Override
  public void setId(int id) {
    this.id = id;

  }

  @Override
  public void setDesc(String desc) {
    this.desc = desc;

  }

}
