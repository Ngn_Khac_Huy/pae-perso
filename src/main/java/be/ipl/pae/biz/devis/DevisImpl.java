package be.ipl.pae.biz.devis;

import java.time.LocalDate;

public class DevisImpl implements DevisDto {

  private int idDevis;
  private String etat;
  private LocalDate dateDevis;
  private LocalDate dateDebutTravaux;
  private double montant;
  private int duree;
  private int idClient;
  private int photoFavorite;

  public DevisImpl() {}

  /**
   * Constructeur du devis.
   * 
   * @param idDevis l'id du devis
   * @param etat l'etat du devis
   * @param dateDevis la date du devis
   * @param dateDebutTravaux la date des travaux du devis
   * @param montant le montant du devis
   * @param duree la duree du devis
   * @param idClient l'id du client du devis
   */
  public DevisImpl(int idDevis, String etat, LocalDate dateDevis, LocalDate dateDebutTravaux,
      double montant, int duree, int idClient) {
    super();
    this.idDevis = idDevis;
    this.etat = etat;
    this.dateDevis = dateDevis;
    this.dateDebutTravaux = dateDebutTravaux;
    this.montant = montant;
    this.duree = duree;
    this.idClient = idClient;
  }

  public int getIdDevis() {
    return idDevis;
  }

  public void setIdDevis(int idDevis) {
    this.idDevis = idDevis;
  }

  public String getEtat() {
    return etat;
  }

  public void setEtat(String etat) {
    this.etat = etat;
  }

  public LocalDate getDateDevis() {
    return dateDevis;
  }

  public void setDateDevis(LocalDate dateDevis) {
    this.dateDevis = dateDevis;
  }

  public LocalDate getDateDebutTravaux() {
    return dateDebutTravaux;
  }

  public void setDateDebutTravaux(LocalDate dateDebutTravaux) {
    this.dateDebutTravaux = dateDebutTravaux;
  }

  public double getMontant() {
    return montant;
  }

  public void setMontant(double montant) {
    this.montant = montant;
  }

  public int getDuree() {
    return duree;
  }

  public void setDuree(int duree) {
    this.duree = duree;
  }

  public int getIdClient() {
    return idClient;
  }

  public void setIdClient(int idClient) {
    this.idClient = idClient;
  }

  public int getPhotoFavorite() {
    return photoFavorite;
  }

  public void setPhotoFavorite(int photoFavorite) {
    this.photoFavorite = photoFavorite;
  }

  @Override
  public String toString() {
    return "DevisImpl [idDevis=" + idDevis + ", etat=" + etat + ", dateDevis=" + dateDevis
        + ", dateDebutTravaux=" + dateDebutTravaux + ", montant=" + montant + ", duree=" + duree
        + ", idClient=" + idClient + "]";
  }

}
