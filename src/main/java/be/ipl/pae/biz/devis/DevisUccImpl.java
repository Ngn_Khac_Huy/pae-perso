package be.ipl.pae.biz.devis;


import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.dal.devis.DevisDao;
import be.ipl.pae.dal.photos.PhotoDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.dal.travaux.TravailDao;
import be.ipl.pae.dal.utilisateurs.UtilisateurDao;
import be.ipl.pae.exception.BizException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class DevisUccImpl implements DevisUcc {

  private DevisDao devisDao;
  private PhotoDao photoDao;
  private UtilisateurDao utilisateurDao;
  private DalServices dalServices;
  private TravailDao travailDao;

  /**
   * Constructeur du devis ucc.
   * 
   * @param devisDao le dao du devis
   * @param utilisateurDao le dao de l'utilisateur
   * @param travaiLDao le dao du travil
   * @param dalServices le dalservices pour les transaction
   */
  public DevisUccImpl(DevisDao devisDao, UtilisateurDao utilisateurDao, TravailDao travaiLDao,
      PhotoDao photoDao, DalServices dalServices) {
    this.devisDao = devisDao;
    this.photoDao = photoDao;
    this.utilisateurDao = utilisateurDao;
    this.dalServices = dalServices;
    this.travailDao = travaiLDao;
  }

  @Override
  public List<DevisDto> voirDevis(int idUtilisateur, int montantMin, int montantMax, Date date,
      String amenagement, String nom) {
    try {
      dalServices.startTransaction();
      UtilisateurDto utilisateurDto =
          utilisateurDao.recupererUtilisateurViaIdUtilisateur(idUtilisateur);
      if (utilisateurDto == null) {
        BizException bizE = new BizException("Utilisateur non existant");
        bizE.setTypeError("400");
        throw bizE;
      }

      if (montantMax == 0 && date == null && amenagement == null && nom == null) {

        if (utilisateurDto.getStatut().toString().equals("o")) {
          return devisDao.recupererTousLesDevis();
        } else {
          return devisDao.recupererDevisViaIdUtilisateur(idUtilisateur);
        }

      } else {
        if (utilisateurDto.getStatut().toString().equals("o")) {
          return devisDao.rechercherAdmin(nom, amenagement, montantMin, montantMax, date);
        } else {
          return devisDao.rechercherUtilisateur(idUtilisateur, nom, amenagement, montantMin,
              montantMax, date);
        }
      }

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public List<DevisDto> recupererDevisClient(int idClient) {
    try {
      dalServices.startTransaction();
      return devisDao.recupererDevisClient(idClient);

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean confirmerDevis(int idDevis) {
    boolean ok = true;
    try {
      dalServices.startTransaction();
      devisDao.modifierEtat("Date de début confirmée", idDevis);
      return ok;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean modifierEtat(int idDevis, String etat) {
    boolean ok = true;
    try {
      dalServices.startTransaction();
      devisDao.modifierEtat(etat, idDevis);
      return ok;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean modifierDate(int idDevis, String date) {
    boolean ok = true;
    try {

      dalServices.startTransaction();
      devisDao.ajoutDate(idDevis, LocalDate.parse(date));
      return ok;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean introduireDevis(DevisDto devis, ArrayList<AmenagementDto> listeAmenagement,
      ArrayList<String> listeImage) {
    boolean success = true;
    try {
      dalServices.startTransaction();
      devisDao.introduireDevis(devis);
      DevisDto devisRecup = devisDao.recupererUnDevis(devis.getDateDevis(), devis.getIdClient());
      if (devisRecup.getIdDevis() == 0) {
        BizException bizE = new BizException("Devis non existant");
        bizE.setTypeError("400");
        throw bizE;
      }

      for (AmenagementDto amenagement : listeAmenagement) {
        travailDao.introduireTravail(devisRecup.getIdDevis(), amenagement.getId());
      }
      for (String image : listeImage) {
        photoDao.ajoutImage(devisRecup.getIdDevis(), image, 0, false);
      }
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }

    return success;
  }

  @Override
  public String recupererPhoto(int idPhoto) {
    try {
      if (idPhoto == 0) {
        return "";
      }
      dalServices.startTransaction();
      return photoDao.recupererPhoto(idPhoto);
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public DevisDto recupererDevis(int idDevis) {
    try {
      dalServices.startTransaction();
      return devisDao.recupererDevis(idDevis);

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

}
