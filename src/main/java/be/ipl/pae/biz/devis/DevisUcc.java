package be.ipl.pae.biz.devis;

import be.ipl.pae.biz.amenagements.AmenagementDto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public interface DevisUcc {
  /**
   * Methode qui permet de voir tous les devis d'un utilisateur.
   * 
   * @param idUtilisateur l'id de l'utilisateur dont on souhaite obtenir les devis
   * @return List DevisDto si l'utilisateur est bien un client possedant des devis; null dans le cas
   *         contraire
   */
  List<DevisDto> voirDevis(int idUtilisateur, int montantMin, int montantMax, Date date,
      String amenagement, String nom);

  /**
   * Recupere la liste des devis d'un client grace a son id dans la DB.
   * 
   * @param idClient id d'un client (entier)
   * @return la liste des devis
   */
  List<DevisDto> recupererDevisClient(int idClient);

  /**
   * Methode qui permet de confirmer la date de debut des travaux d'un devis.
   * 
   * @param idDevis l'id du devis dont il faut confirmer la date de debut des travaux
   * @return True si la date a pu correctement etre mise a jour dans le devis; False dans le cas
   *         contraire
   */
  boolean confirmerDevis(int idDevis);

  /**
   * Methode qui permet d'introduire un nouveau devis dans la DB.
   * 
   * @param devisDto le devis à introduire en DB
   * @param amenagementListe la liste d'amenagements relatifs a ce devis
   * @param imageListe la liste de textBase 64 de l'image
   * @return True si le devis a correctement pu etre insere en DB; False dans le cas contraire
   */
  boolean introduireDevis(DevisDto devisDto, ArrayList<AmenagementDto> amenagementListe,
      ArrayList<String> imageListe);

  /**
   * Recupere une photo sous forme de chaine de caractere.
   * 
   * @param idPhoto id de la photo (entier)
   * @return chaine de caractere representant la photo
   */
  String recupererPhoto(int idPhoto);


  /**
   * Recupere un devis en fonction de son id.
   * 
   * @param idDevis id d'un devis (entier)
   * @return le devis correspondant
   */
  DevisDto recupererDevis(int idDevis);

  /**
   * Modifie l'etat d'un devis dans la DB grace a son id.
   * 
   * @param etat le nouvel etat du devis (entier)
   * @param idDevis id d'un client (entier)
   * @return true si l'etat a bien ete modifie
   */
  boolean modifierEtat(int idDevis, String etat);

  /**
   * modifie la date du debut des travaux d'un devis dans la DB grace a son id.
   * 
   * @param idDevis id d'un client (entier)
   * @param date la date du debut des travaux (entier)
   * @return true si l'etat a bien ete modifie
   */
  boolean modifierDate(int idDevis, String date);

}
