package be.ipl.pae.biz.devis;

import java.time.LocalDate;

public interface DevisDto {

  // getters
  int getIdDevis();

  String getEtat();

  LocalDate getDateDevis();

  LocalDate getDateDebutTravaux();

  double getMontant();

  int getDuree();

  int getIdClient();

  int getPhotoFavorite();

  // setters
  void setIdDevis(int idDevis);

  void setEtat(String etat);

  void setDateDevis(LocalDate dateDevis);

  void setDateDebutTravaux(LocalDate dateDebutTravaux);

  void setMontant(double montant);

  void setDuree(int duree);

  void setIdClient(int idClient);

  void setPhotoFavorite(int photoFavorite);

}
