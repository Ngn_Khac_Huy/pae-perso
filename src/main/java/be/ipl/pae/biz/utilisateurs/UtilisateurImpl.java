package be.ipl.pae.biz.utilisateurs;

import org.mindrot.bcrypt.BCrypt;

import java.time.LocalDate;

public class UtilisateurImpl implements Utilisateur {

  private String pseudo;
  private String email;
  private String nom;
  private String mdp;
  private String prenom;
  private String statut;
  private String ville;
  private LocalDate dateInscription;
  private int idUtilisateur;
  private int idClient;
  private String sel;

  public UtilisateurImpl() {
    this.sel = BCrypt.gensalt();
  }

  /**
   * Constructeur utilisateur.
   * 
   * @param pseudo le pseudo de l'utilisateur
   * @param email l'email de l'utilisateur
   * @param nom le nom de l'utilisateur
   * @param mdp le mdp de l'utilisateur
   * @param prenom le prenom de l'utilisateur
   * @param statut le statut de l'utilisateur
   * @param ville le ville de l'utilisateur
   * @param dateInscription le date d'inscription de l'utilisateur
   * @param idUtilisateur le idUtilisateur correspondant
   * @param idClient le idClient lie a l'utilisateur
   */
  public UtilisateurImpl(String pseudo, String email, String nom, String mdp, String prenom,
      String statut, String ville, LocalDate dateInscription, int idUtilisateur, int idClient) {
    this.pseudo = pseudo;
    this.email = email;
    this.nom = nom;
    this.mdp = mdp;
    this.prenom = prenom;
    this.statut = statut;
    this.ville = ville;
    this.dateInscription = dateInscription;
    this.idUtilisateur = idUtilisateur;
    this.idClient = idClient;
    this.sel = BCrypt.gensalt();

  }

  // getters

  @Override
  public String getPseudo() {
    // TODO Auto-generated method stub
    return this.pseudo;
  }

  @Override
  public String getEmail() {
    // TODO Auto-generated method stub
    return this.email;
  }

  @Override
  public String getMdp() {
    // TODO Auto-generated method stub
    return this.mdp;
  }

  @Override
  public String getNom() {
    // TODO Auto-generated method stub
    return this.nom;
  }

  @Override
  public String getPrenom() {
    // TODO Auto-generated method stub
    return this.prenom;
  }

  @Override
  public String getStatut() {
    // TODO Auto-generated method stub
    return statut;
  }

  @Override
  public LocalDate getDateInscription() {
    // TODO Auto-generated method stub
    return dateInscription;
  }

  @Override
  public String getVille() {
    // TODO Auto-generated method stub
    return ville;
  }

  @Override
  public int getIdUtilisateur() {
    // TODO Auto-generated method stub
    return idUtilisateur;
  }

  @Override
  public int getIdClient() {
    // TODO Auto-generated method stub
    return idClient;
  }

  // setters

  @Override
  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public void setMdp(String mdp) {
    this.mdp = mdp;
  }

  @Override
  public void setNom(String nom) {
    this.nom = nom;
  }

  @Override
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  @Override
  public void setStatut(String statut) {
    this.statut = statut;
  }

  @Override
  public void setDateInscription(LocalDate dateInsription) {
    this.dateInscription = dateInsription;
  }

  @Override
  public void setVille(String ville) {
    this.ville = ville;
  }

  @Override
  public void setIdUtilisateur(int idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  @Override
  public void setIdClient(int idClient) {
    this.idClient = idClient;
  }

  @Override
  public void setPseudo(String pseudo) {
    this.pseudo = pseudo;
  }

  @Override
  public boolean verifierMdp(String mdp, String mdpStocker) {
    return BCrypt.checkpw(mdp, mdpStocker);
  }

  public void crypterMdp() {
    this.mdp = BCrypt.hashpw(mdp, sel);
  }

  @Override
  public String toString() {
    return "UtilisateurImpl [pseudo=" + pseudo + ", email=" + email + ", nom=" + nom + ", mdp="
        + mdp + ", prenom=" + prenom + ", statut=" + statut + ", ville=" + ville
        + ", dateInscription=" + dateInscription + ", idUtilisateur=" + idUtilisateur
        + ", idClient=" + idClient + "]";
  }

}
