package be.ipl.pae.biz.utilisateurs;

interface Utilisateur extends UtilisateurDto {

  /**
   * Methode qui verifie que le mdp introduit correspond a celui dans la base de donnees.
   * 
   * @param mdp le mdp rentre par l'utilisateur
   * @param mdpStocker le mdp stocke en DB
   * @return True si le mdp est correct; False si le mdp est faux
   */
  boolean verifierMdp(String mdp, String mdpStocker);

  void crypterMdp();

}
