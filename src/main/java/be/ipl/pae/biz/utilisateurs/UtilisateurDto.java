package be.ipl.pae.biz.utilisateurs;

import java.time.LocalDate;

public interface UtilisateurDto {

  // getters and setters

  // getters

  String getPseudo();

  String getEmail();

  String getMdp();

  String getNom();

  String getPrenom();

  String getStatut();

  LocalDate getDateInscription();

  String getVille();

  int getIdUtilisateur();

  int getIdClient();

  // setters

  void setPseudo(String pseudo);

  void setEmail(String email);

  void setMdp(String mdp);

  void setNom(String nom);

  void setPrenom(String prenom);

  void setStatut(String statut);

  void setDateInscription(LocalDate dateInscription);

  void setVille(String ville);

  void setIdUtilisateur(int idUtilisateur);

  void setIdClient(int idClient);

  String toString();

}
