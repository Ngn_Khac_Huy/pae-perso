// package be.ipl.pae.BIZ.utilisateurs;
//
// import java.time.LocalDateTime;
// import be.ipl.pae.DAL.UtilisateurDAO;
//
// public class UtilisateurUCC {
//
// private UtilisateurDAO utilsDAO = new UtilisateurDAO();
//
// public UtilisateurUCC() {
//
// }
//
// public UtilisateurDTO seConnecter(String pseudo, String mdp) {
// // Utilisateur utils = (Utilisateur) utilsDAO.recupererUtilisateur(pseudo);
// Utilisateur utils = new UtilisateurImpl("pseudo", "email", "nom", "mdpTemp", "prenom", false,
// " ville", LocalDateTime.now(), " id");
//
// if (utils.verifierMdp(mdp, utils.getMdp())) {
// return utils;
// }
//
// // a finir
// return null;
//
// }
//
// }

package be.ipl.pae.biz.utilisateurs;

import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.dal.utilisateurs.UtilisateurDao;
import be.ipl.pae.exception.BizException;

import java.util.List;

public class UtilisateurUccImpl implements UtilisateurUcc {

  private UtilisateurDao utilisateurDao;
  private DalServices dalServices;

  /**
   * Constructeur Ucc.
   * 
   * @param utilisateurDao le dao de l'utilisateur
   * @param dalServices la dals services pour les transactions
   */
  public UtilisateurUccImpl(UtilisateurDao utilisateurDao, DalServices dalServices) {
    this.utilisateurDao = utilisateurDao;
    this.dalServices = dalServices;

  }

  @Override
  public UtilisateurDto seConnecter(String pseudo, String mdp) {
    try {
      dalServices.startTransaction();
      UtilisateurDto utilisateur = utilisateurDao.recupererUtilisateurViaPseudo(pseudo);
      if (utilisateur != null
          && ((Utilisateur) utilisateur).verifierMdp(mdp, utilisateur.getMdp())) {
        return utilisateur;
      }
      BizException bizE = new BizException("Mauvais identifiants");
      bizE.setTypeError("400");
      throw bizE;
    } catch (Exception exception) {
      exception.printStackTrace();
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public UtilisateurDto sinscrire(UtilisateurDto dto) {
    try {
      dalServices.startTransaction();
      UtilisateurDto utilisateur = utilisateurDao.recupererUtilisateurViaPseudo(dto.getPseudo());
      if (utilisateur == null) {
        ((Utilisateur) dto).crypterMdp();
        utilisateurDao.inscrireUtilisateur(dto);
        return dto;
      }
      BizException bizE = new BizException("Pseudo déjà existant");
      bizE.setTypeError("409");
      throw bizE;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      exception.printStackTrace();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public List<UtilisateurDto> recupererUtilisateursAConfirmer() {
    try {
      dalServices.startTransaction();
      return utilisateurDao.recupererUtilisateursAConfirmer();

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      exception.printStackTrace();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean confirmerInscription(UtilisateurDto dto, int idClient) {
    try {
      dalServices.startTransaction();
      return utilisateurDao.lierClient(dto, idClient);

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      exception.printStackTrace();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }


  @Override
  public UtilisateurDto recupererUtilisateur(int idUtilisateur) {
    try {
      dalServices.startTransaction();
      UtilisateurDto utilisateur =
          utilisateurDao.recupererUtilisateurViaIdUtilisateur(idUtilisateur);
      if (utilisateur == null) {
        BizException bizE = new BizException("Utilisateur non existant");
        bizE.setTypeError("400");
        throw bizE;
      }
      return utilisateur;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean verifierClientEstUtilisateur(int idClient) {
    try {
      dalServices.startTransaction();
      UtilisateurDto utilisateurDto = utilisateurDao.verifierClientEstUtilisateur(idClient);
      if (utilisateurDto.getIdClient() == 0) {
        return false;
      }
      return true;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public List<UtilisateurDto> rechercherUtilisateursInscrits(String nom, String ville) {
    try {
      dalServices.startTransaction();
      return utilisateurDao.rechercherUtilisateursInscrits(nom, ville);
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }
}
