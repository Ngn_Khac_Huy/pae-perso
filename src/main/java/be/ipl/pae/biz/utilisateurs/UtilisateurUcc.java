package be.ipl.pae.biz.utilisateurs;

import java.util.List;

public interface UtilisateurUcc {

  /**
   * Methode qui permet de se connecter à l'application.
   * 
   * @param pseudo le pseudo rentre par l'utilisateur
   * @param mdp le mdp rentre par l'utilisateur
   * @return UtilisateurDto correspondant au pseudo si la connexion a pu etre effectuee; null dans
   *         le cas contraire
   */
  UtilisateurDto seConnecter(String pseudo, String mdp);

  /**
   * Methode qui permet de s'inscrire sur l'application.
   * 
   * @param dto l'utilisateur a inserer dans la base de donnees
   * @return UtilisateurDto qui vient d'etre cree si l'inscription a fonctionnee; null dans le cas
   *         contraire
   */
  UtilisateurDto sinscrire(UtilisateurDto dto);

  /**
   * Methode qui permet de confirmer l'inscription d'un utilisateur.
   * 
   * @param dto l'utilisateur a confirmer
   * @param idClient l'id du client auquel lier cet utilisateur
   * @return True si l'utilisateur a pu etre lie au client; False dans le cas contraire
   */
  boolean confirmerInscription(UtilisateurDto dto, int idClient);

  /**
   * Methode qui permet de recuperer la liste des utilisateurs dont il faut confirmer l'inscription.
   * 
   * @return List UtilisateurDto s'il y a des utilisateurs à confirmer; null dans le cas contraire
   */
  List<UtilisateurDto> recupererUtilisateursAConfirmer();

  /**
   * Methode qui permet de recuperer dans la DB un utilisateur à partir de son id.
   * 
   * @param idUtilisateur l'id de l'utilisateur qu'on souhaite recuperer
   * @return UtilisateurDto si un utilisateur correspond à l'id; null dans le cas contraire
   */
  UtilisateurDto recupererUtilisateur(int idUtilisateur);

  /**
   * Verifie qu'un client est bien un utilisateur du site.
   * 
   * @param idClient l'id du client (entier)
   * @return True s'il est utilisateur
   */
  boolean verifierClientEstUtilisateur(int idClient);

  /**
   * renvoi tout les utilisateurs en fonction d'un nom d'utilisateur ou d'un ville.
   * 
   * @param nom nom de l'utilisateur (String)
   * @param ville la ville de l'utilisateur (String)
   * @return la liste des utilisateur renvoyes
   */
  List<UtilisateurDto> rechercherUtilisateursInscrits(String nom, String ville);
}
