package be.ipl.pae.biz.photos;

import java.util.ArrayList;

public interface PhotoUcc {

  /**
   * Recupere la liste de toutes les photos visibles presentes dans la DB.
   * 
   * 
   * @return Une liste des photos
   */
  ArrayList<PhotoDto> recupererPhotosVisible();

  /**
   * Recupere la liste de toutes les photos en fonction d'un type d'amenagement.
   * 
   * @param idAmenagement id de l'amenagement (entier)
   * @return Une liste des photos
   */
  ArrayList<PhotoDto> recupererPhotosAmenagementVisible(int idAmenagement);

  /**
   * Recupere la liste de toutes les photos pour un devis specifique.
   * 
   * @param idDevis id du devis (entier)
   * @return Une liste de photos
   */
  ArrayList<PhotoDto> recupererPhotosParDevis(int idDevis);

  /**
   * Ajoute une image a un devis.
   * 
   * @param photoData donnees de l'image (String)
   * @param idDevis id du devis (entier)
   * @param idAmenagement id de l'amenagement de la photo (entier)
   * @param favori indique si la photo est favorite (boolean)
   * @param visible indique si la photo est visible sur l'acceuil du site (boolean)
   * @return True si la photo a bien ete ajoutee, false sinon
   */
  boolean ajoutPhotos(String photoData, int idDevis, int idAmenagement, boolean favori,
      boolean visible);



}
