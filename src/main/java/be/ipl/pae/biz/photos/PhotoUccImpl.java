package be.ipl.pae.biz.photos;

import be.ipl.pae.dal.photos.PhotoDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.exception.BizException;

import java.util.ArrayList;

public class PhotoUccImpl implements PhotoUcc {

  private PhotoDao photoDao;
  private DalServices dalServices;

  /**
   * Constructeur ucc.
   * 
   * @param photoDao le dao de la photo
   * @param dalServices le dalservices pour les transaction
   */
  public PhotoUccImpl(PhotoDao photoDao, DalServices dalServices) {
    this.photoDao = photoDao;
    this.dalServices = dalServices;

  }



  @Override
  public ArrayList<PhotoDto> recupererPhotosAmenagementVisible(int idAmenagement) {
    try {
      dalServices.startTransaction();
      ArrayList<PhotoDto> listePhotos =
          photoDao.recupererPhotosVisibleParAmenagement(idAmenagement);
      if (listePhotos.size() == 0) {
        BizException bizE = new BizException("Pas de photo disponible");
        bizE.setTypeError("400");
        throw bizE;
      }
      return listePhotos;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public ArrayList<PhotoDto> recupererPhotosVisible() {
    try {
      dalServices.startTransaction();
      ArrayList<PhotoDto> listePhotos = photoDao.recupererPhotosVisible();
      if (listePhotos.size() == 0) {
        BizException bizE = new BizException("Pas de photo disponible");
        bizE.setTypeError("400");
        throw bizE;
      }
      return listePhotos;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public ArrayList<PhotoDto> recupererPhotosParDevis(int idDevis) {
    try {
      dalServices.startTransaction();
      ArrayList<PhotoDto> listePhotos = photoDao.recupererPhotosParDevis(idDevis);
      return listePhotos;
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public boolean ajoutPhotos(String photoData, int idDevis, int idAmenagement, boolean favori,
      boolean visible) {
    try {
      int idPhoto = 0;
      dalServices.startTransaction();
      idPhoto = photoDao.ajoutImage(idDevis, photoData, idAmenagement, visible);
      if (favori) {
        photoDao.ajouterPhotoFav(idPhoto, idDevis);
      }
      return true;
    } catch (

    Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }



}
