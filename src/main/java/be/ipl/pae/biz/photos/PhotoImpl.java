package be.ipl.pae.biz.photos;

public class PhotoImpl implements PhotoDto {

  private int idPhoto;
  private String photoData;
  private int idDevis;
  private int idAmenagement;
  private boolean estVisible;

  public PhotoImpl() {}

  /**
   * Constructeur d'une photo.
   * 
   * @param idPhoto l'id de la photo
   * @param photoData l'etat du devis
   * @param idDevis l'id du devis auquel correspond la photo
   * @param idAmenagement l'id de l'amenagement auquel correspond la photo
   * @param estVisible pour dire si la photo est visible ou pas
   */
  public PhotoImpl(int idPhoto, String photoData, int idDevis, int idAmenagement,
      boolean estVisible) {
    super();
    this.idPhoto = idPhoto;
    this.photoData = photoData;
    this.idDevis = idDevis;
    this.idAmenagement = idAmenagement;
    this.estVisible = estVisible;
  }

  public int getIdPhoto() {
    return idPhoto;
  }

  public String getPhotoData() {
    return photoData;
  }

  public int getIdDevis() {
    return idDevis;
  }

  public int getIdAmenagement() {
    return idAmenagement;
  }

  public boolean isEstVisible() {
    return estVisible;
  }

  public void setIdPhoto(int idPhoto) {
    this.idPhoto = idPhoto;
  }

  public void setPhotoData(String photoData) {
    this.photoData = photoData;
  }

  public void setIdDevis(int idDevis) {
    this.idDevis = idDevis;
  }

  public void setIdAmenagement(int idAmenagement) {
    this.idAmenagement = idAmenagement;
  }

  public void setEstVisible(boolean estVisible) {
    this.estVisible = estVisible;
  }


}
