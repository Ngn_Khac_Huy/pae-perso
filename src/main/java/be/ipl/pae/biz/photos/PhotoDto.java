package be.ipl.pae.biz.photos;

public interface PhotoDto {

  int getIdPhoto();

  String getPhotoData();

  int getIdDevis();

  int getIdAmenagement();

  boolean isEstVisible();

  void setIdPhoto(int idPhoto);

  void setPhotoData(String photoData);

  void setIdDevis(int idDevis);

  void setIdAmenagement(int idAmenagement);

  void setEstVisible(boolean estVisible);

}
