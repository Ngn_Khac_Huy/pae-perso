package be.ipl.pae.biz.factory;

import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.clients.ClientDto;
import be.ipl.pae.biz.devis.DevisDto;
import be.ipl.pae.biz.photos.PhotoDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;

/**
 * Permet de creer instance d'un objet.
 *
 */
public interface Factory {

  /**
   * Permet de recuperer un utilisateur.
   * 
   * @return L'instance d'un Utilisateur DTO
   */
  UtilisateurDto getUtilisateur();

  /**
   * Permet de recuperer un client.
   * 
   * @return L'instance d'un Client DTO
   */
  ClientDto getClient();

  /**
   * Permet de recuperer un devis.
   * 
   * @return L'instance d'un Devis DTO
   */
  DevisDto getDevis();

  /**
   * Permet de recuperer un amenagement.
   * 
   * @return L'instance d'un Amenagement DTO
   */
  AmenagementDto getAmenagement();

  PhotoDto getPhoto();

}
