package be.ipl.pae.biz.factory;

import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.amenagements.AmenagementImpl;
import be.ipl.pae.biz.clients.ClientDto;
import be.ipl.pae.biz.clients.ClientImpl;
import be.ipl.pae.biz.devis.DevisDto;
import be.ipl.pae.biz.devis.DevisImpl;
import be.ipl.pae.biz.photos.PhotoDto;
import be.ipl.pae.biz.photos.PhotoImpl;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurImpl;

/***
 * @see Factory.
 *
 */
public class FactoryImpl implements Factory {

  @Override
  public UtilisateurDto getUtilisateur() {
    return new UtilisateurImpl();
  }

  @Override
  public ClientDto getClient() {
    return new ClientImpl();
  }

  @Override
  public DevisDto getDevis() {
    return new DevisImpl();
  }

  @Override
  public AmenagementDto getAmenagement() {
    return new AmenagementImpl();
  }

  @Override
  public PhotoDto getPhoto() {
    return new PhotoImpl();
  }


}
