package be.ipl.pae.biz.clients;

public interface ClientDto {

  String getNom();

  String getPrenom();

  String getRue();

  int getNumero();

  String getBoite();

  int getCp();

  String getVille();

  String getEmail();

  String getTel();

  int getIdClient();

  void setNom(String nom);

  void setPrenom(String prenom);

  void setRue(String rue);

  void setNumero(int numero);

  void setBoite(String boite);

  void setCp(int cp);

  void setVille(String ville);

  void setEmail(String email);

  void setTel(String tel);

  void setIdClient(int idClient);

}
