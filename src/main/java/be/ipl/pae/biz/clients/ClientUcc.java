package be.ipl.pae.biz.clients;

import java.util.List;

public interface ClientUcc {

  /**
   * Recupere la liste de tous les clients presents dans la DB.
   * 
   * @return Une liste des clients
   */
  List<ClientDto> recupererTousLesClients();

  /**
   * Recupere le client grace a son id dans la DB.
   * 
   * @param idClient id du client (entier)
   * @return le client recupere
   */
  ClientDto recupererClient(int idClient);

  /**
   * Introduit un nouveau client dans la DB.
   * 
   * @param clDto Le client (dans un dto)
   * @return true si tout c'est bien passe,false sinon
   */
  ClientDto introduireClient(ClientDto clDto);

  /**
   * Recupere la liste de tous les clients presents dans la DB selon 3 criteres de selection.
   * 
   * @param nom le nom du client (String)
   * @param ville la ville de residance du cleint (String)
   * @param cp le code postal de la ville (entier)
   * @return La liste des clients recherches
   */
  List<ClientDto> rechercherClients(String nom, String ville, int cp);

}
