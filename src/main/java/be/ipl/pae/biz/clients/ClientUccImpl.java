package be.ipl.pae.biz.clients;

import be.ipl.pae.dal.clients.ClientDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.exception.BizException;

import java.util.List;

public class ClientUccImpl implements ClientUcc {

  private ClientDao clientDao;
  private DalServices dalServices;

  /**
   * Consturcteur de l'implementation.
   * 
   * @param clientDao Le dao du client
   * @param dalServices le dalServices pour les transactions
   */
  public ClientUccImpl(ClientDao clientDao, DalServices dalServices) {
    this.clientDao = clientDao;
    this.dalServices = dalServices;
  }

  @Override
  public List<ClientDto> recupererTousLesClients() {
    try {
      dalServices.startTransaction();
      return clientDao.recupererTousLesClients();

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public ClientDto recupererClient(int idClient) {
    try {
      dalServices.startTransaction();
      return clientDao.recupererClient(idClient);

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public ClientDto introduireClient(ClientDto clDto) {
    try {
      dalServices.startTransaction();
      if (clientDao.verifPresenceClient(clDto.getEmail())) {
        BizException bizE = new BizException("Client deja existant");
        bizE.setTypeError("409");
        throw bizE;
      }
      clientDao.inscrireClient(clDto);
      return clDto;

    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }

  @Override
  public List<ClientDto> rechercherClients(String nom, String ville, int cp) {
    try {
      dalServices.startTransaction();
      if (nom.isEmpty() && ville.isEmpty() && cp == 0) {
        return clientDao.recupererTousLesClients();
      } else {
        return clientDao.rechercherClients(nom, ville, cp);
      }
    } catch (Exception exception) {
      dalServices.rollbackTransaction();
      throw exception;
    } finally {
      dalServices.commitTransaction();
    }
  }
}
