package be.ipl.pae.biz.clients;

public class ClientImpl implements ClientDto {

  private String nom;
  private String prenom;
  private String rue;
  private int numero;
  private String boite;
  private int cp;
  private String ville;
  private String email;
  private String tel;
  private int idClient;

  public ClientImpl() {

  }

  /**
   * Construsteur client.
   * 
   * @param nom le nom du client
   * @param prenom le prenom du client
   * @param rue la rue du client
   * @param numero le numero du client
   * @param boite la boite du client
   * @param cp le cp du client
   * @param ville la ville du client
   * @param email le email du client
   * @param tel le tel du client
   * @param idClient l'idClient du client
   */
  public ClientImpl(String nom, String prenom, String rue, int numero, String boite, int cp,
      String ville, String email, String tel, int idClient) {
    super();
    this.nom = nom;
    this.prenom = prenom;
    this.rue = rue;
    this.numero = numero;
    this.boite = boite;
    this.cp = cp;
    this.ville = ville;
    this.email = email;
    this.tel = tel;
    this.idClient = idClient;
  }

  public String getNom() {
    return nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public String getRue() {
    return rue;
  }

  public int getNumero() {
    return numero;
  }

  public String getBoite() {
    return boite;
  }

  public int getCp() {
    return cp;
  }

  public String getVille() {
    return ville;
  }

  public String getEmail() {
    return email;
  }

  public String getTel() {
    return tel;
  }

  public int getIdClient() {
    return idClient;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public void setRue(String rue) {
    this.rue = rue;
  }

  public void setNumero(int numero) {
    this.numero = numero;
  }

  public void setBoite(String boite) {
    this.boite = boite;
  }

  public void setCp(int cp) {
    this.cp = cp;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setTel(String tel) {
    this.tel = tel;
  }

  public void setIdClient(int idClient) {
    this.idClient = idClient;
  }
}
