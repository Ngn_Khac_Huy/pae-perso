package be.ipl.pae.main;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import be.ipl.pae.biz.amenagements.AmenagementDto;
import be.ipl.pae.biz.amenagements.AmenagementUccImpl;
import be.ipl.pae.biz.clients.ClientDto;
import be.ipl.pae.biz.clients.ClientUccImpl;
import be.ipl.pae.biz.devis.DevisDto;
import be.ipl.pae.biz.devis.DevisUccImpl;
import be.ipl.pae.biz.photos.PhotoDto;
import be.ipl.pae.biz.photos.PhotoUccImpl;
import be.ipl.pae.biz.utilisateurs.UtilisateurDto;
import be.ipl.pae.biz.utilisateurs.UtilisateurImpl;
import be.ipl.pae.biz.utilisateurs.UtilisateurUccImpl;
import be.ipl.pae.dal.amenagements.AmenagementDao;
import be.ipl.pae.dal.clients.ClientDao;
import be.ipl.pae.dal.devis.DevisDao;
import be.ipl.pae.dal.photos.PhotoDao;
import be.ipl.pae.dal.services.DalServices;
import be.ipl.pae.dal.travaux.TravailDao;
import be.ipl.pae.dal.utilisateurs.UtilisateurDao;
import be.ipl.pae.exception.BizException;
import be.ipl.pae.exception.DalException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MainTest {

  // utilisateur
  private UtilisateurUccImpl utilisateur;
  private UtilisateurImpl utilisateurRecup;
  private UtilisateurDao utilisateurDao;
  private String pseudoCorrect;
  private String pseudoIncorrect;
  private String bonMdp;
  private String mauvaisMdp;
  private String image;
  private ArrayList<AmenagementDto> listeAmenagement = new ArrayList<AmenagementDto>();
  private ArrayList<String> listeImage = new ArrayList<String>();
  private UtilisateurDto utilisateurDto;
  private List<UtilisateurDto> listeUtilisateur;
  private DalServices dalServices;

  // devis
  private DevisDao devisDao;
  private List<DevisDto> listeDevisDto;
  private DevisUccImpl devis;
  private DevisDto devisDto;

  // client
  private ClientDao clientDao;
  private List<ClientDto> listeClientDto;
  private ClientDto clientDto;
  private ClientUccImpl client;

  // amenagement
  private AmenagementDao amenagementDao;
  private List<AmenagementDto> listeAmenagementDto;
  private AmenagementUccImpl amenagement;
  private AmenagementDto amenagementDto;


  // travaux
  private TravailDao travailDao;
  private String dataImage;

  // photo
  private PhotoDao photoDao;
  private ArrayList<PhotoDto> listePhotoDto;
  private ArrayList<PhotoDto> listePhotoDtoVide;
  private PhotoUccImpl photo;
  private PhotoDto photoDto;



  /**
   * Setup de notre classe de test.
   * 
   * @throws Exception exception correspondate
   */
  @BeforeEach
  public void setup() throws Exception {
    // utilisateur
    pseudoCorrect = "Huyfi";
    pseudoIncorrect = "HUyfI";
    bonMdp = "mdp";
    mauvaisMdp = "m_mdp";
    image = "image";
    listeImage.add(image);

    utilisateurDao = mock(UtilisateurDao.class);
    utilisateurRecup = mock(UtilisateurImpl.class);
    dalServices = mock(DalServices.class);
    utilisateur = new UtilisateurUccImpl(utilisateurDao, dalServices);
    utilisateurDto = mock(UtilisateurDto.class);
    listeUtilisateur = new ArrayList<>();
    listeUtilisateur.add(utilisateurDto);
    UtilisateurDto utilisateurDto2 = mock(UtilisateurDto.class);
    listeUtilisateur.add(utilisateurDto2);

    // photo
    photoDao = mock(PhotoDao.class);
    listePhotoDto = new ArrayList<>();
    listePhotoDtoVide = new ArrayList<>();
    photo = new PhotoUccImpl(photoDao, dalServices);
    photoDto = mock(PhotoDto.class);
    listePhotoDto.add(photoDto);

    // devis
    devisDao = mock(DevisDao.class);
    listeDevisDto = new ArrayList<>();
    devisDto = mock(DevisDto.class);
    listeDevisDto.add(devisDto);
    travailDao = mock(TravailDao.class);

    devis = new DevisUccImpl(devisDao, utilisateurDao, travailDao, photoDao, dalServices);



    // client
    clientDao = mock(ClientDao.class);
    listeClientDto = new ArrayList<>();
    client = new ClientUccImpl(clientDao, dalServices);
    clientDto = mock(ClientDto.class);

    // amenagement
    amenagementDao = mock(AmenagementDao.class);
    listeAmenagementDto = new ArrayList<>();
    amenagementDto = mock(AmenagementDto.class);
    listeAmenagement.add(amenagementDto);
    amenagement = new AmenagementUccImpl(amenagementDao, dalServices);


    // travaux



  }

  // ------------------------test
  // UtilisateurUcc-------------------------------------

  // Test de la méthode se connecter avec un mdp correct et un pseudo correct
  @Test
  public void testSeConnecter1() {
    when(utilisateurDao.recupererUtilisateurViaPseudo(pseudoCorrect)).thenReturn(utilisateurRecup);
    when(utilisateurRecup.getMdp()).thenReturn(bonMdp);
    when(utilisateurRecup.verifierMdp(bonMdp, utilisateurRecup.getMdp())).thenReturn(true);
    assertEquals(utilisateur.seConnecter(pseudoCorrect, bonMdp), utilisateurRecup);
  }

  // Test de la méthode se connecter avec un mdp incorrect et un pseudo correct
  @Test
  public void testSeConnecter2() {
    when(utilisateurDao.recupererUtilisateurViaPseudo(pseudoCorrect)).thenReturn(utilisateurRecup);
    when(utilisateurRecup.getMdp()).thenReturn(mauvaisMdp);
    when(utilisateurRecup.verifierMdp(bonMdp, utilisateurRecup.getMdp())).thenReturn(false);

    assertThrows(BizException.class, () -> utilisateur.seConnecter(pseudoCorrect, mauvaisMdp));
  }

  // Test de la méthode se connecter avec un mdp correct et un pseudo incorrect
  @Test
  public void testSeConnecter3() {
    when(utilisateurDao.recupererUtilisateurViaPseudo(pseudoIncorrect)).thenReturn(null);
    assertThrows(BizException.class, () -> utilisateur.seConnecter(pseudoIncorrect, mauvaisMdp));
  }

  // Test de la méthode se connecter lorsqu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testSeConnecter4() {
    when(utilisateurDao.recupererUtilisateurViaPseudo(pseudoIncorrect))
        .thenThrow(DalException.class);
    assertThrows(DalException.class, () -> utilisateur.seConnecter(pseudoIncorrect, mauvaisMdp));
  }

  // test de la methode s'inscrire
  @Test
  public void testSinscrire1() {
    UtilisateurImpl ui = utilisateurRecup;
    when(utilisateurDao.recupererUtilisateurViaPseudo(ui.getPseudo())).thenReturn(null);

    when(utilisateurDao.inscrireUtilisateur(ui)).thenReturn(true);
    utilisateurRecup.crypterMdp();

    assertEquals(utilisateur.sinscrire(ui), utilisateurRecup);
  }

  // test de la methode s'inscrire lorsque l'utilisateur est deja present dans la
  // db
  @Test
  public void testSinscrire2() {
    when(utilisateurDao.recupererUtilisateurViaPseudo(utilisateurDto.getPseudo()))
        .thenReturn(utilisateurDto);
    assertThrows(BizException.class, () -> utilisateur.sinscrire(utilisateurDto));
  }

  // test de la methode s'inscrire lorsque qu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testSinscrire3() {
    UtilisateurImpl ui = utilisateurRecup;
    when(utilisateurDao.recupererUtilisateurViaPseudo(ui.getPseudo())).thenReturn(null);
    utilisateurRecup.crypterMdp();
    when(utilisateurDao.inscrireUtilisateur(ui)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> utilisateur.sinscrire(ui));
  }

  // test de la methode confirmer inscription avec le bon id client et le bon
  // utilisateurDto
  @Test
  public void testConfirmerInscription1() {
    when(utilisateurDao.lierClient(utilisateurDto, 3)).thenReturn(true);
    assertTrue(utilisateur.confirmerInscription(utilisateurDto, 3));
  }

  // test de la methode confirmer inscription lorsqu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testConfirmerInscription2() {
    when(utilisateurDao.lierClient(utilisateurDto, 3)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> utilisateur.confirmerInscription(utilisateurDto, 3));
  }

  // test de la methode RecupererUtilisateursAConfirmer lorsque tout est ok
  @Test
  public void testRecupererUtilisateursAConfirmer1() {
    when(utilisateurDao.recupererUtilisateursAConfirmer()).thenReturn(listeUtilisateur);
    assertEquals(utilisateur.recupererUtilisateursAConfirmer(), listeUtilisateur);
  }

  // test de la methode RecupererUtilisateursAConfirmer lorsqu'un probleme
  // survient
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererUtilisateursAConfirmer2() {
    when(utilisateurDao.recupererUtilisateursAConfirmer()).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> utilisateur.recupererUtilisateursAConfirmer());
  }

  // test de la methode RecupererUtilisateur avec le bon id utilisateur
  @Test
  public void testRecupererUtilisateur1() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(utilisateurDto);
    assertEquals(utilisateur.recupererUtilisateur(1), utilisateurDto);
  }

  // test de la methode RecupererUtilisateur avec un mauvais id utilisateur
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererUtilisateur2() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> utilisateur.recupererUtilisateur(1));
  }

  // test de la methode RecupererUtilisateur si l'Utilisateur est null
  @Test
  public void testRecupererUtilisateur3() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(null);
    assertThrows(BizException.class, () -> utilisateur.recupererUtilisateur(1));
  }

  // test verifierClientEstUtilisateur quand tous est ok et utilisateurDto.getIdClient() !=0
  @Test
  public void testVerifierClientEstUtilisateur1() {
    when(utilisateurDao.verifierClientEstUtilisateur(1)).thenReturn(utilisateurDto);
    when(utilisateurDto.getIdClient()).thenReturn(1);
    assertTrue(utilisateur.verifierClientEstUtilisateur(1));
  }

  // test verifierClientEstUtilisateur quand tous est ok et utilisateurDto.getIdClient() = 0
  @Test
  public void testVerifierClientEstUtilisateur2() {
    when(utilisateurDao.verifierClientEstUtilisateur(1)).thenReturn(utilisateurDto);
    when(utilisateurDto.getIdClient()).thenReturn(0);
    assertTrue(!utilisateur.verifierClientEstUtilisateur(1));
  }


  // test verifierClientEstUtilisateur quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testVerifierClientEstUtilisateur3() {
    when(utilisateurDao.verifierClientEstUtilisateur(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> utilisateur.verifierClientEstUtilisateur(1));
  }


  // test RechercherUtilisateursInscrits quand tous est ok
  @Test
  public void testRechercherUtilisateursInscrits1() {
    when(utilisateurDao.rechercherUtilisateursInscrits("line", "wavre"))
        .thenReturn(listeUtilisateur);
    assertEquals(utilisateur.rechercherUtilisateursInscrits("line", "wavre"), listeUtilisateur);
  }

  // test RechercherUtilisateursInscrits quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testRechercherUtilisateursInscrits2() {
    when(utilisateurDao.rechercherUtilisateursInscrits("line", "wavre"))
        .thenThrow(DalException.class);
    assertThrows(DalException.class,
        () -> utilisateur.rechercherUtilisateursInscrits("line", "wavre"));
  }

  // -----------------------test
  // DevisUcc----------------------------------------------

  // test de la methode voirDevis avec le bon id utilisateur, le status du
  // l'utilisateur est u et la recherche est vide
  @Test
  public void testVoirDevis1() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(utilisateurDto);
    when(utilisateurDto.getStatut()).thenReturn("u");
    when(devisDao.recupererDevisViaIdUtilisateur(1)).thenReturn(listeDevisDto);

    assertEquals(devis.voirDevis(1, 0, 0, null, null, null), listeDevisDto);
  }

  // test de la methode voirDevis avec le bon id utilisateur, le status du
  // l'utilisateur est o et la recherche est vide
  @Test
  public void testVoirDevis2() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(utilisateurDto);
    when(utilisateurDto.getStatut()).thenReturn("o");
    when(devisDao.recupererTousLesDevis()).thenReturn(listeDevisDto);

    assertEquals(devis.voirDevis(1, 0, 0, null, null, null), listeDevisDto);
  }

  // test de la methode voirDevis lorsqu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testVoirDevis3() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.voirDevis(1, 0, 0, null, null, null));
  }

  // test de la methode voirDevis si l'utilisateur est null
  @Test
  public void testVoirDevis4() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(null);
    assertThrows(BizException.class, () -> devis.voirDevis(1, 0, 0, null, null, null));
  }

  // test la methode voir devis quand le satus = u et que la recherche n est pas vide
  @Test
  public void testVoirDevis5() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(utilisateurDto);
    when(utilisateurDto.getStatut()).thenReturn("u");
    when(devisDao.rechercherUtilisateur(1, "line", null, 0, 0, null)).thenReturn(listeDevisDto);

    assertEquals(devis.voirDevis(1, 0, 0, null, null, "line"), listeDevisDto);
  }

  // test la methode voir devis quand le satus = o et que la recherche n est pas vide
  @Test
  public void testVoirDevis6() {
    when(utilisateurDao.recupererUtilisateurViaIdUtilisateur(1)).thenReturn(utilisateurDto);
    when(utilisateurDto.getStatut()).thenReturn("o");

    when(devisDao.rechercherAdmin("line", null, 0, 0, null)).thenReturn(listeDevisDto);
    assertEquals(devis.voirDevis(1, 0, 0, null, null, "line"), listeDevisDto);
  }

  // test de la methode confirmerDevis lorsque tout est ok
  @Test
  public void testConfirmerDevis1() {
    assertTrue(devis.confirmerDevis(1));
  }

  // test de la methode confirmerDevis lorsqu'il y a un probleme
  @SuppressWarnings("unchecked")
  @Test
  public void testConfirmerDevis2() {
    when(devisDao.modifierEtat("Date de début confirmée", 1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.confirmerDevis(1));
  }

  // test de la methode IntroduireDevis
  @Test
  public void testIntroduireDevis1() {
    // DevisDto myDevisDto= mock(DevisDto.class);

    when(devisDao.introduireDevis(devisDto)).thenReturn(true);
    when(devisDao.recupererUnDevis(devisDto.getDateDevis(), devisDto.getIdClient()))
        .thenReturn(devisDto);
    when(devisDto.getIdDevis()).thenReturn(1);
    when(travailDao.introduireTravail(devisDto.getIdDevis(), amenagementDto.getId()))
        .thenReturn(true);
    assertTrue(devis.introduireDevis(devisDto, listeAmenagement, listeImage));
  }

  // test de la methode IntroduireDevis lorsque l'id devis est 0
  @Test
  public void testIntroduireDevis2() {
    // DevisDto myDevisDto= mock(DevisDto.class);
    when(devisDao.introduireDevis(devisDto)).thenReturn(true);
    when(devisDao.recupererUnDevis(devisDto.getDateDevis(), devisDto.getIdClient()))
        .thenReturn(devisDto);
    when(devisDto.getIdDevis()).thenReturn(0);

    assertThrows(BizException.class,
        () -> devis.introduireDevis(devisDto, listeAmenagement, listeImage));
  }

  // test de la methode IntroduireDevis lorsque probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testIntroduireDevis3() {
    // DevisDto myDevisDto= mock(DevisDto.class);
    when(devisDao.introduireDevis(devisDto)).thenThrow(DalException.class);

    assertThrows(DalException.class,
        () -> devis.introduireDevis(devisDto, listeAmenagement, listeImage));
  }

  // test de la methode recuperer
  @Test
  public void testrecupererPhoto1() {
    when(photoDao.recupererPhoto(1)).thenReturn(dataImage);

    assertEquals(devis.recupererPhoto(1), dataImage);
  }

  @Test
  public void testrecupererPhoto2() {

    assertEquals(devis.recupererPhoto(0), "");
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testrecupererPhoto3() {
    // DevisDto myDevisDto= mock(DevisDto.class);
    when(photoDao.recupererPhoto(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.recupererPhoto(1));
  }

  // test modifier etat quand tous est ok
  @Test
  public void testModifierEtat1() {
    when(devisDao.modifierEtat("visible", 1)).thenReturn(true);
    assertTrue(devis.modifierEtat(1, "visible"));
  }

  // test modifier etat quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testModifierEtat2() {
    when(devisDao.modifierEtat("visible", 1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.modifierEtat(1, "visible"));
  }

  // test modifier date quand tous est ok
  @Test
  public void testModifierDate1() {
    when(devisDao.ajoutDate(1, LocalDate.parse("2000-01-01"))).thenReturn(true);
    assertTrue(devis.modifierDate(1, "2000-01-01"));
  }

  // test modifier date quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testModifierDate2() {
    when(devisDao.ajoutDate(1, LocalDate.parse("2000-01-01"))).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.modifierDate(1, "2000-01-01"));
  }

  // test recuperer devis client quand tous est ok
  @Test
  public void testRecupererDevisClient1() {
    when(devisDao.recupererDevisClient(1)).thenReturn(listeDevisDto);
    assertEquals(devis.recupererDevisClient(1), listeDevisDto);
  }

  // test recuperer devis client quand tous est ok
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererDevisClient2() {
    when(devisDao.recupererDevisClient(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.recupererDevisClient(1));
  }

  // test recupererDevis quand tous est ok
  @Test
  public void testRecupererDevis1() {
    when(devisDao.recupererDevis(1)).thenReturn(devisDto);
    assertEquals(devis.recupererDevis(1), devisDto);
  }

  // test recupererDevis quand un dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererDevis2() {
    when(devisDao.recupererDevis(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> devis.recupererDevis(1));
  }



  // -----------------------test
  // ClientUcc----------------------------------------------

  // test de la methode recupererTousLesClients
  @Test
  public void testRecupererTousLesClients1() {
    when(clientDao.recupererTousLesClients()).thenReturn(listeClientDto);
    assertEquals(client.recupererTousLesClients(), listeClientDto);

  }

  // test de la methode recupererTousLesClients quand une erreur survient
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererTousLesClients2() {
    when(clientDao.recupererTousLesClients()).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> client.recupererTousLesClients());

  }

  // test de la methode TestrecupererClient quand l'idClient est present dans la
  // db
  @Test
  public void testrecupererClient1() {
    when(clientDao.recupererClient(1)).thenReturn(clientDto);
    assertEquals(client.recupererClient(1), clientDto);
  }

  // test de la methode TestrecupererClient quand l'idClient n'est pas dans la db
  @SuppressWarnings("unchecked")
  @Test
  public void testrecupererClient2() {
    when(clientDao.recupererClient(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> client.recupererClient(1));
  }

  // test de la methode introduireClient lorsque le client n'est pas present dans
  // la db et qu l'
  // inscription c'est bien passee
  @Test
  public void testIntroduireClient1() {
    when(clientDao.verifPresenceClient(clientDto.getEmail())).thenReturn(false);
    when(clientDao.inscrireClient(clientDto)).thenReturn(true);
    assertEquals(client.introduireClient(clientDto), clientDto);

  }

  // test de la methode introduireClient lorsque le client n'est pas present dans
  // la db et qu l'
  // inscription c'est mal passee
  @SuppressWarnings("unchecked")
  @Test
  public void testIntroduireClient2() {
    when(clientDao.verifPresenceClient(clientDto.getEmail())).thenReturn(false);
    when(clientDao.inscrireClient(clientDto)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> client.introduireClient(clientDto));

  }

  // test de la methode introduireClient lorsque le client est deja present dans
  // la db
  @Test
  public void testIntroduireClient3() {
    when(clientDao.verifPresenceClient(clientDto.getEmail())).thenReturn(true);
    assertThrows(BizException.class, () -> client.introduireClient(clientDto));

  }

  // test de la methode introduireClient lorsque le client est present dans la db
  // mais il y aeu un
  @SuppressWarnings("unchecked")
  // pbnlme
  @Test
  public void testIntroduireClient4() {
    when(clientDao.verifPresenceClient(clientDto.getEmail())).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> client.introduireClient(clientDto));

  }

  // test rechercher client quand la recherche est vide
  @Test
  public void testrechercherClients1() {
    when(clientDao.recupererTousLesClients()).thenReturn(listeClientDto);
    assertEquals(client.rechercherClients("", "", 0), listeClientDto);
  }

  // test rechercher client quand la recherche n est pas vide
  @Test
  public void testrechercherClients2() {
    when(clientDao.rechercherClients("line", "", 0)).thenReturn(listeClientDto);
    assertEquals(client.rechercherClients("line", "", 0), listeClientDto);
  }

  // test rechercher client quand une dalException est lancee
  @Test
  @SuppressWarnings("unchecked")
  public void testrechercherClients3() {
    when(clientDao.recupererTousLesClients()).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> client.rechercherClients("", "", 0));
  }


  // -----------------------test
  // AmenagementUcc----------------------------------------------

  // test de la methode recupererListeAmenagement quand tous est ok
  @Test
  public void testrecupererListeAmenagement1() {
    when(amenagementDao.recupererListeAmenagements()).thenReturn(listeAmenagementDto);
    assertEquals(amenagement.recupererListeAmenagements(), listeAmenagementDto);
  }

  // test de la methode recupererListeAmenagement lorsqu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testrecupererListeAmenagement2() {
    when(amenagementDao.recupererListeAmenagements()).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> amenagement.recupererListeAmenagements());
  }

  // test de la methode lorsque tout est ok
  @Test
  public void testRecupererAmenagement1() {
    when(amenagementDao.recupererAmenagement("nom")).thenReturn(amenagementDto);
    assertEquals(amenagement.recupererAmenagement("nom"), amenagementDto);
  }

  // test de la methode lorsqu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererAmenagement2() {
    when(amenagementDao.recupererAmenagement("nom")).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> amenagement.recupererAmenagement("nom"));
  }

  // test de la methode lorsque l'amenagement est null
  @Test
  public void testRecupererAmenagement3() {
    when(amenagementDao.recupererAmenagement("nom")).thenReturn(null);
    assertThrows(BizException.class, () -> amenagement.recupererAmenagement("nom"));
  }

  // test de la methode recupererListeAmenagement quand tous est ok
  @Test
  public void testrecupererListeAmenagementParDevis1() {
    when(amenagementDao.recupererListeAmenagementsParDevis(1)).thenReturn(listeAmenagementDto);
    assertEquals(amenagement.recupererListeAmenagementsParDevis(1), listeAmenagementDto);
  }

  // test de la methode recupererListeAmenagement lorsqu'un probleme survient
  @SuppressWarnings("unchecked")
  @Test
  public void testrecupererListeAmenagementParDevis2() {
    when(amenagementDao.recupererListeAmenagementsParDevis(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> amenagement.recupererListeAmenagementsParDevis(1));
  }

  // test de la methode recuperer amenagement par id quand tous est ok
  @Test
  public void testRecupererAmenagementParId1() {
    when(amenagementDao.recupererAmenagementParId(1)).thenReturn(amenagementDto);
    assertEquals(amenagement.recupererAmenagementParId(1), amenagementDto);
  }

  // test de la methode recuperer amenagement par id quand l amenagement est null
  @Test
  public void testRecupererAmenagementParId2() {
    when(amenagementDao.recupererAmenagementParId(1)).thenReturn(null);
    // BizException bizE = new BizException("Amenagement non existant");
    // bizE.setTypeError("400");
    assertThrows(BizException.class, () -> amenagement.recupererAmenagementParId(1));
  }

  // test de la methode recuperer amenagement par id quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererAmenagementParId3() {
    when(amenagementDao.recupererAmenagementParId(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> amenagement.recupererAmenagementParId(1));
  }

  // test ajouterAmenagement quand tous est ok
  @Test
  public void testAjouterAmenagement1() {
    when(amenagementDao.ajouterAmenagement("oo")).thenReturn(true);
    assertTrue(amenagement.ajouterAmenagement("oo"));
  }

  // test ajouterAmenagement quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testAjouterAmenagement2() {
    when(amenagementDao.ajouterAmenagement("oo")).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> amenagement.ajouterAmenagement("oo"));
  }

  // -----------------------test
  // PhotoUcc----------------------------------------------



  // test recupererPhotosAmenagementVisible quand tous est ok
  @Test
  public void testRecupererPhotosAmenagementVisible1() {

    when(photoDao.recupererPhotosVisibleParAmenagement(1)).thenReturn(listePhotoDto);
    when(photoDto.isEstVisible()).thenReturn(true);
    when(photoDto.getIdAmenagement()).thenReturn(1);
    assertEquals(photo.recupererPhotosAmenagementVisible(1), listePhotoDto);
  }

  // test recupererPhotosAmenagementVisible quand la liste est vide
  @Test
  public void testRecupererPhotosAmenagementVisible2() {
    when(photoDao.recupererPhotosVisibleParAmenagement(1))
        .thenReturn((ArrayList<PhotoDto>) listePhotoDtoVide);
    assertThrows(BizException.class, () -> photo.recupererPhotosAmenagementVisible(1));
  }

  // test recupererPhoto quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererPhotosAmenagementVisible3() {
    when(photoDao.recupererPhotosVisibleParAmenagement(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> photo.recupererPhotosAmenagementVisible(1));
  }

  // test recupererPhotosVisible quand tous est ok
  @Test
  public void testRecupererPhotosVisible1() {
    when(photoDao.recupererPhotosVisible()).thenReturn(listePhotoDto);
    when(photoDto.isEstVisible()).thenReturn(true);
    assertEquals(photo.recupererPhotosVisible(), listePhotoDto);
  }

  // test recupererPhotosVisible quand la liste est vide
  @Test
  public void testRecupererPhotosVisible2() {
    when(photoDao.recupererPhotosVisible()).thenReturn((ArrayList<PhotoDto>) listePhotoDtoVide);
    assertThrows(BizException.class, () -> photo.recupererPhotosVisible());

  }

  // test recupererPhoto quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererPhotosVisible3() {
    when(photoDao.recupererPhotosVisible()).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> photo.recupererPhotosVisible());
  }

  // test recupererPhotosParDevis quand tous est ok
  @Test
  public void testRecupererPhotosParDevis1() {
    when(photoDao.recupererPhotosParDevis(1)).thenReturn(listePhotoDto);
    assertEquals(photo.recupererPhotosParDevis(1), listePhotoDto);
  }

  // test recupererPhotosParDevis quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testRecupererPhotosParDevis2() {
    when(photoDao.recupererPhotosParDevis(1)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> photo.recupererPhotosParDevis(1));
  }

  // test recupererPhotosParDevis quand tous est ok
  @Test
  public void testAjoutPhotos1() {
    when(photoDao.ajoutImage(1, "", 1, true)).thenReturn(1);
    assertTrue(photo.ajoutPhotos("", 1, 1, true, true));

  }

  // test recupererPhotosParDevis quand une dalException est lancee
  @SuppressWarnings("unchecked")
  @Test
  public void testAjoutPhotos2() {
    when(photoDao.ajoutImage(1, "", 1, true)).thenThrow(DalException.class);
    assertThrows(DalException.class, () -> photo.ajoutPhotos("", 1, 1, true, true));
  }

}
