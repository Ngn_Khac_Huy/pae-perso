var listeImage = [];
var listeImageApres = [];
var imageDevisTemp;
var token;
var devisCourant;
var statut;
var slideIndex = 1;
import { affichageAcceuil } from "./index.js";
import {
    create_dynamic_HTML_table, autocomplete,
    gererAffichageEtatBouton, afficheSlideshowDevis, creerListeAmenagements
} from "./utilsHTML.js";
import { getData, confirmerDate, postData } from "./UtilsAPI.js";


const DEVIS_PROPERTIES_DETAILLE = ["idDevis", "amenagements", "etat", "dateDevis",
    "dateDebutTravaux", "montant", "duree","photoFavorite2"];
const UTILISATEUR_PROPERTIES_DETAILLE = ["nom", "prenom", "adresse", "cp",
    "ville", "email", "tel"];
const DEVIS_PROPERTIES = ["idDevis", "nomClient", "amenagements", "dateDevis", "duree", "montant", "etat",
    "dateDebutTravaux", "photoFavorite", "info devis"];

$(document).ready(function () {

    $(document).on('change', '.uploadImage', function () {
        encodeImagetoBase64(this);
    });

    //Affiche le formulaire d'introduction de devis
    $("#versIntroDevis").click(function (e) {
        $("#tableau").hide();
        token = localStorage.getItem("token");
        listeImage = [];
        $("#messageNotif").show();

        document.getElementById("espacePerso").innerHTML = "";
        $("#espacePerso").load("./html/introduireDevis.html")
        getData("/client", token, onGetIntroDevis, onError);

    });

    //Intoduit le devis lorsqu'on appuie sur le bouton
    $(document).on("click", "#devisSubmit", function (e) {
        token = localStorage.getItem("token");
        var listAmen = [];
        $(".amenagementDevis").each(function () {
            listAmen.push($(this).val());
        });

        e.preventDefault();
        const data = {
            idClient: $("#idClientDevis").val(),
            montant: $("#montantDevis").val(),
            duree: $("#dureeDevis").val(),
            nomAmenagement: listAmen,
            date: $("#dateDevis").val(),
            photo: listeImage,
        }
        postData("/devis", data, token, onPostDevis, onErrorDevis);
    });

    //Recupere les infos detailles d'un devis
    $(document).on("click", ".boutonPlusInfo", function (e) {
        token = localStorage.getItem("token");
        e.preventDefault();
        devisCourant = this.value;
        const data = {
            idDevis: this.value
        }
        getData("/devis/devisPlusInfo", token, onGetDevis2, onErrorDevis2, data);
    });

    //Confirme la date lorsqu'on appuie sur le bouton
    $(document).on("click", "#boutonConfirmerDate", function (e) {
        e.preventDefault();
        confirmerDate();
    });

    //Recupere tous les devis d'un client
    $(document).on("click", ".boutonDevisClient", function (e) {
        token = localStorage.getItem("token");
        e.preventDefault();
        const data = {
            idClient: this.value
        }
        getData("/devis/devisClient", token, onGetDevis, onError, data);
    });

    //comportement pour le slideshow
    $(document).on('click', '#prevDevisAvant', function (e) {
        plusSlides(-1, "mySlidesAvant");
    });
    $(document).on('click', '#nextDevisAvant', function (e) {
        plusSlides(1, "mySlidesAvant");
    });
    $(document).on('click', '#prevDevisApres', function (e) {
        plusSlides(-1, "mySlidesApres");
    });
    $(document).on('click', '#nextDevisApres', function (e) {
        plusSlides(1, "mySlidesApres");
    });

    //Permet d'ajouter un nouveau champ pour introduire une nouvelle image
    $(document).on("click", "#ajoutChampPhoto", function (e) {
        e.preventDefault();
        $("#inputPhoto").clone(true).insertAfter("#inputPhoto");
    });



    //Permet d'ajouter un nouveau champ pour introduire un amenagement
    $(document).on("click", "#ajoutChampAmenagement", function (e) {
        e.preventDefault();
        $("#inputAmenagement").clone(true).insertAfter("#inputAmenagement");

    });


    //Ajoute la photo en db lors du click
    $(document).on("click", "#ajoutChampPhotoDevis", function (e) {
        e.preventDefault();
        $("#inputPhoto").clone(true).insertAfter("#inputPhoto");
    });

    //Encodage des images lors d'un upload d'image

    $(document).on('change', '.uploadImageDevis', function () {
        encodeImagetoBase64viaDevis(this);
    });

    //Affiche le nom du fichier
    $(document).on("change", ".custom-file-input", function (e) {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
      });

});

//Affiche le formulaire d'intro du devis
function onGetIntroDevis(response) {
    token = localStorage.getItem("token");
    autocomplete(document.getElementById("clientDevis"), response.listeC, "", document.getElementById("idClientDevis"));
    getData("/devis/amenagement", token, onGetAmenagement, onError);

}

//Alert en cas d'erreur
function onError(err) {
    console.log(err);
    alert("echec");

}

//Fonction lorsque la requete d'ajout du client est reussi
function onPostDevis(response) {
    affichageAcceuil();
    alert("Ajout devis reussi");

}

//Fonction lorsque la requete d'ajout du devis est echoue
function onErrorDevis(err) {
    $("#messageDevis").html(err.responseJSON["error"]);
    $("#messageDevis").css("color", "red");
}

//Fonction lorsque la requete pour recuperer les amenagements reussi
function onGetAmenagement(response) {
    creerListeAmenagements(response, "amenagementDevis");
}

//creer un liste d'image a envoyer dans la db
function encodeImagetoBase64(element) {
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        listeImage.push(reader.result);
    }
    reader.readAsDataURL(file);
}

//affiche les infos détaillés d'un devis
function onGetDevis2(response) {
    let tab = [];

    if (response.success) {
        $("#espacePerso").load("./html/afficheUnDevis.html", function (e) {
            let id_devis = 0;
            let etat = "";

            e.preventDefault
            $("#infoDevis").show();
            $("#tableau").hide();
            tab.push(response.data[0]);


            id_devis = tab[0]["idDevis"];
            etat = tab[0]["etat"];

            //info du devis
            create_dynamic_HTML_table(
                "infoDevis",
                tab,
                DEVIS_PROPERTIES_DETAILLE,
                "Info du devis"
            );
            tab.pop();
            tab.push(response.data[1]);

            //info du client
            create_dynamic_HTML_table(
                "infoClient",
                tab,
                UTILISATEUR_PROPERTIES_DETAILLE,
                "Info du propriétaire"
            );
            $("#dateDiv").hide();

            //Ecran reservé a l'ouvrier
            statut = localStorage.getItem("statut");
            console.log(statut);
            if (statut == "o") {
                gererAffichageEtatBouton(etat, "boutonEtat", response);
                $("#buttonDateNew").click(function (e) {
                    modifierDate(id_devis, $("#dateNew").val());
                });
                $("#changerEtat1").click(function (e) {
                    if ($("#changerEtat1").val() == "Visible") {
                        var listechoixAmenPhoto = []
                        $(".choixAmenPhotoDevis").each(function () {
                            listechoixAmenPhoto.push($(this).val());
                        });
                        var properties = []
                        $("input[type=checkbox]").each(function () {
                            properties.push(this.checked);

                        });

                        const data = {
                            photoData: listeImageApres,
                            amen: listechoixAmenPhoto,
                            properties: properties,
                            idDevis: devisCourant,
                        }
                        console.log(data);
                        postData("/photo", data, token, onUploadImageSucces, onUploadImageError);
                        listeImageApres=[];

                    }
                    
                    nextEtat(id_devis, $("#changerEtat1").val());
                });
                $("#changerEtat2").click(function (e) {
                    nextEtat(id_devis, $("#changerEtat2").val());
                });
                $("#changerEtat3").click(function (e) {
                    nextEtat(id_devis, $("#changerEtat3").val());
                });

                $("#ajoutPhotoApresVisible").click(function (e) {
                        var listechoixAmenPhoto = []
                        $(".choixAmenPhotoDevis").each(function () {
                            listechoixAmenPhoto.push($(this).val());
                        });
                        var properties = []
                        $("input[type=checkbox]").each(function () {
                            properties.push(this.checked);

                        });

                        const data = {
                            photoData: listeImageApres,
                            amen: listechoixAmenPhoto,
                            properties: properties,
                            idDevis: devisCourant,
                        }
                        console.log(data);
                        postData("/photo", data, token,onUploadImageSucces , onUploadImageError);
                        listeImageApres=[];
                    

                });
            }

            //affiche les photos avant et apres
            afficheSlideshowDevis(response, "slideshowDevisAvant", "slideshowDevisApres");
        
            

        });
    };
}

//Fonction lorsque la requete d'affichage du devis echoue
function onErrorDevis2(err) {
    console.log(err);
    $("#messageNotif").html(err.responseJSON["error"]);
    $("#messageNotif").css("color", "red");
}

//Affiche la table lorsque le get devis reussi
function onGetDevis(response) {
    if (response.success) {
        if (response.data.length > 0) {
            $("#pasDeDevis").hide();
            create_dynamic_HTML_table(
                "tableau",
                response.data,
                DEVIS_PROPERTIES,
                "Liste des devis"
            );
        }else {
            $("#tableau").hide();
            $("#pasDeClients").show();
            
        }
    };
}

//fonction qui fait permet de changer d etat
function nextEtat(idDevis, etat) {

    let headers;
    if (token)
        headers = {
            //Authorization: "Bearer " + token
            Authorization: token
        };
    else
        headers = {
        };

    $.ajax({

        type: "put",
        url: "/devis",
        headers: headers,
        data: { id: idDevis, etat: etat },
        dataType: "json",
        success:
            function (response) {
                devisInfo(idDevis);
            },

        error: function name(err, statut, message) {

            console.log("Error :", statut, "Messsage:", message, "json : ", err["responseText"]);
            location.reload();

        }
    });
}

//permet de modifier la date de debut de travaux d un devis
function modifierDate(idDevis, date) {
    let token = localStorage.getItem("token");
    let headers;
    if (token)
        headers = {
            //Authorization: "Bearer " + token
            Authorization: token
        };
    else
        headers = {
        };

    $.ajax({

        type: "put",
        url: "/devis",
        headers: headers,
        data: { id: idDevis, date: date },
        dataType: "json",
        success:
            function (response) {
                devisInfo(idDevis);
            },
        error: function name(err, statut, message) {

            console.log("Error :", statut, "Messsage:", message, "json : ", err["responseText"]);
            location.reload();

        }
    });
}

// fonction qui permet de reload infosdevis apres un changement d'etat
function devisInfo(idDevis) {
    const data = {
        idDevis: idDevis
    }
    getData(
        "/devis/devisPlusInfo",
        token,
        onGetDevis2,
        onErrorDevis2,
        data
    );
}

//fonctionnement du slideshow src: W3School
function plusSlides(n, txt) {
    showSlides(slideIndex += n, txt);
}

function showSlides(n, txt) {
    var i;
    var slides = document.getElementsByClassName(txt);
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
}

//Enregistre une image a envoye dans la db
function encodeImagetoBase64viaDevis(element) {
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        listeImageApres.push(reader.result);
    }
    reader.readAsDataURL(file);
}

function onUploadImageSucces(response) {
    alert("Photos ajoutées");
    devisInfo(devisCourant);
}

function onUploadImageError(err) {
    console.log(err);
    alert(err.responseJSON["error"]);
    devisInfo(devisCourant);
}

export { showSlides};