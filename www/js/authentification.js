var token;
var statut;
import {postData } from "./UtilsAPI.js";

import { affichageAcceuil } from "./index.js";

$(document).ready(function () {
    
    //Connexion
    $(document).on('click', '#usubmit', function (e) {
        e.preventDefault();
        token = localStorage.getItem("token");
        const data = { pseudo: $("#uname").val(), password: $("#upwd").val() };
        postData(
            "/connexion",
            data,
            token,
            onPostCo,
            onErrorConnexion)
    });

    //Inscription
    $(document).on('click', '#cosubmit', function (e) {
        e.preventDefault();
        token = localStorage.getItem("token");
        const data = {
            pseudo: $("#copseudo").val(),
            nom: $("#conom").val(),
            prenom: $("#coprenom").val(),
            ville: $("#coville").val(),
            email: $("#coemail").val(),
            password: $("#comdp").val()
        }
        postData(
            "/inscription",
            data,
            token,
            onPostIn,
            onErrorInscription)
    });

});

//Fonction lorsque la requete de connexion est reussi
function onPostCo(response) {
    if (response.success == "true") {
        localStorage.setItem("pseudo", response.data["pseudo"]);
        localStorage.setItem("token", response.token);
        localStorage.setItem('statut', response.data["statut"]);
        affichageAcceuil();
    }
}


//Fonction lorsque la requete d'inscription  est reussi
function onPostIn(response) {
    if (response.success == "true") {
        affichageAcceuil();
        alert("Inscription reussi, confirmation en attente");
    }
}

//Fonction lorsque la requete de connexion echoue
function onErrorConnexion(err) {
    console.log(err);
    $("#message1").html(err.responseJSON["error"]);
    $("#message1").css("color", "red");
}

//Fonction lorsque la requete d'inscription echoue
function onErrorInscription(err) {
    $("#message2").html(err.responseJSON["error"]);
    $("#message2").css("color", "red");
}