let pseudo;
var slideIndex = 1;
let token;
let statut;
var listeU;
var listeC;
var amenagementNom;


const UTILISATEUR_PROPERTIES = ["nom", "prenom", "pseudo",
    "ville", "email", "confirmation"
];
import { getData, lierClient, postData } from "./UtilsAPI.js";
import {
    create_dynamic_HTML_table,
    autocomplete,
    afficheSlideshowAcceuil,
    creerListeAmenagementsAvecBouton
} from "./utilsHTML.js";

//Fonction qui affiche la page d'acceuil avec les menus
function affichageAcceuil() {
    $("#spin").show();
    $("#boutonEspacePatron").hide();
    $("#espacePerso").hide();
    $("#auth").hide();
    $("#acceuil").show();
    $("#messageNotif").hide();
    $("#tableau").hide();
    pseudo = localStorage.getItem('pseudo');
    token = localStorage.getItem('token');
    statut = localStorage.getItem('statut');
    document.getElementById("pseudoUtil").innerHTML = pseudo;
    getData("/photo/toutesPhotos", token, onGetPhotoAmenagement, onError);

    //Affiche un bouton de connexion si non connecté
    if (!token) {
        $("#profil").hide();
        $("#boutonAuthentification").load("./html/header.html #boutonCo", function(e) {
            $("#butCo").click(function(e) {
                affichageAuthentification();
            });
        });

        //Affiche le pseudo si connecté et un bouton de deconnexion
    } else {
        $("#profil").show();
        $("#auth").hide();
        $("#boutonAuthentification").load("./html/header.html #boutonDeco", function(e) {

            //deconnexion
            $("#butDeco").click(function(e) {
                localStorage.removeItem("token");
                localStorage.removeItem("pseudo");
                localStorage.removeItem("statut");
                document.getElementById("espacePerso").innerHTML = "";
                $("#tableau").hide();
                affichageAcceuil();
            });
        });
    }
    $("#nomEntreprise").load("./html/header.html #nomEntreprise");
    $("#menu").load("./html/header.html #menu", function() {
        $("#monEspaceBouton").click(function(e) {
            $("#auth").hide();
            $("#acceuil").hide();
            if (statut == null) {
                affichageAuthentification();
            } else {
                $("#boutonEspacePatron").show();
                $("#espacePerso").show();
                $("#versIntroDevis").show();
                $("#versConfirmerInscription").show();
                $("#versAjouterClient").show();
                $("#versAjouterAmenagement").show()
                if (statut == 'c') {
                    $("#versIntroDevis").hide();
                    $("#versConfirmerInscription").hide();
                    $("#versAjouterClient").hide();
                    $("#versAjouterAmenagement").hide();
                }
            }
        });
    });
    getData("/devis/amenagement", token, onGetAmenagementMenu, onError);
}

//Load la page d'authentification
function affichageAuthentification() {
    $("#acceuil").hide();
    $("#spin").hide();
    $("#auth").show();
    $("#auth").load("./html/authentification.html");
}

//fonctionnement du slideshow src: W3School
function plusSlides(n, txt) {
    showSlides(slideIndex += n, txt);
}

function showSlides(n, txt) {
    var i;
    var slides = document.getElementsByClassName(txt);
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
}

$(document).ready(function() {

    affichageAcceuil();

    //Fonctionnement des boutons du slideshows
    $(document).on('click', '#prev', function(e) {
        plusSlides(-1, "mySlides1");
    });
    $(document).on('click', '#next', function(e) {
        plusSlides(1, "mySlides1");
    });

    //Affiche les photos par amenagements
    $(document).on('click', '.choixAmen', function(e) {
        amenagementNom = this.value;
        const data = {
            amenagement: this.value
        };
        getData("/photo/amenagement", token, onGetPhotoAmenagement, onErrorPhotoAmenagement, data);
    });

    //Lie le client qd on remplie le champ correspondant
    $(document).on('click', '.boutonLierClient', function(e) {
        e.preventDefault();
        lierClient(listeU);
    });

    //ajoute un client
    $(document).on('click', '#clisubmit', function(e) {
        e.preventDefault();
        const data = {
            nom: $("#clinom").val(),
            prenom: $("#cliprenom").val(),
            rue: $("#clirue").val(),
            numero: $("#clinum").val(),
            boite: $("#cliboite").val(),
            cp: $("#clicp").val(),
            ville: $("#cliville").val(),
            email: $("#cliemail").val(),
            tel: $("#clitel").val()
        }
        postData(
            "client/client",
            data,
            token,
            onPostClient,
            onErrorClient
        )
    });

    //Ajouter un amenagement
    $("#versAjouterAmenagement").click(function(e) {
        document.getElementById("espacePerso").innerHTML = "";
        $("#tableau").hide();
        $("#espacePerso").load("./html/form.html", function() {

            $(document).on("click", "#amenagementAjout", function(e) {
                e.preventDefault();
                const data = {
                    amenagement: $("#amenagementAjoutVal").val(),
                }
                postData("/devis/amenagement", data, token, onAjoutAmenagement, onAjoutAmenagementError);
            });
        });
    });

    //Recupere la liste des utilisateurs a confirme et l'affiche
    $("#versConfirmerInscription").click(function(e) {
        document.getElementById("espacePerso").innerHTML = "";
        $("#tableau").hide();
        getData("/client", token, onGetUtilisateur, onError);
    });

    //affiche le formulaire d'ajout de client
    $("#versAjouterClient").click(function(e) {
        document.getElementById("espacePerso").innerHTML = "Ajouter un client";
        $("#tableau").hide();
        $("#espacePerso").load("./html/ajoutClient.html", function() {
            //Mettre ajx ici et les .clicks
            e.preventDefault();
        });
    });

});

//Fonction lorsque la requete d'ajout du client est reussi
function onPostClient(response) {
    if (response.success == "true") {
        affichageAcceuil();
        alert("Ajout client reussi");
    }
}

//Fonction lorsque la requete d'ajout du client echoue
function onErrorClient(err) {
    console.log(err);
    $("#message3").html(err.responseJSON["error"]);
    $("#message3").css("color", "red");
}

//Alert en cas d'erreur
function onError(err) {
    console.log(err);
    alert("echec");

}

//Affiche une alert si une erreur se produit
function onAjoutAmenagementError(err) {
    $("#messageAjoutAmen").show();
    $("#messageAjoutAmen").html(err.responseJSON["error"]);
    $("#messageAjoutAmen").css("color", "red");
}

//Affiche une alert lors de l'ajout
function onAjoutAmenagement(response) {
    $("#messageAjoutAmen").hide();
    alert("Amenagement ajouté");

}

//Affiche la table lorsque le get utilisateurs reussi
function onGetUtilisateur(response) {
    if (response.success) {
        listeC = response.listeC;
        listeU = response.listeU;
        if (response.listeU.length > 0) {

            //cree la table
            create_dynamic_HTML_table(
                "espacePerso",
                response.listeU,
                UTILISATEUR_PROPERTIES,
                "Liste des utilisateurs"
            );
        } else {
            document.getElementById("espacePerso").innerHTML = "Pas d'utilisateurs à confirmer";

        }
    };

    //initialise le autocomplete
    for (let i = 0; i < response.listeU.length; i++) {
        autocomplete(document.getElementById(response.listeU[i]["idUtilisateur"]),
            response.listeC,
            response.listeU[i]["idUtilisateur"],
            document.getElementById("idClient_" + response.listeU[i]["idUtilisateur"]));
    }
}

//Fonction lorsque la requete pour recuperer les amenagements dans le menu reussi
function onGetAmenagementMenu(response) {
    creerListeAmenagementsAvecBouton(response, "choixAmenagement")
}

//Affiche le carousel
function onGetPhotoAmenagement(response) {

    afficheSlideshowAcceuil(response, "Slideshow");
    showSlides(1, "mySlides1");
}

//Affiche alert en cas d'erreur
function onErrorPhotoAmenagement(err) {
    alert("Pas de photos disponibles pour cet aménagement");
}

export { affichageAcceuil };