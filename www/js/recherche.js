var token;
var statut;
import { getData } from "./UtilsAPI.js";
import {
    create_dynamic_HTML_table,
    creerListeAmenagements
} from "./utilsHTML.js";

const UTILISATEUR_PROPERTIES_RECHERCHE = ["nom", "prenom", "pseudo",
    "ville", "email", "statut", "dateInscription"
];

const CLIENT_PROPERTIES = ["nom", "prenom", "email", "tel"];
const CLIENT_PROPERTIES_DETAILLE = ["idClient", "nom", "prenom", "email", "cp",
"ville", "tel", "estUtilisateur", "devis client"];
const DEVIS_PROPERTIES = ["idDevis", "nomClient", "amenagements", "dateDevis", "duree", "montant", "etat",
    "dateDebutTravaux", "photoFavorite", "info devis"];


$(document).ready(function() {

    //Affiche les champs de recherches
    $("#versEffectuerRecherche").click(function(e) {
        $("#tableau").hide();
        if (document.getElementById("tableau") != null) {
            document.getElementById("tableau").innerHTML = "";
        }

        token = localStorage.getItem("token");
        $("#espacePerso").load("./html/ListeDevis.html", function() {
            $("#spinDevis").hide();

            $("#affichageVieuw").hide();
            $("#clientVieuw").hide();
            $("#utilisateurVieuw").hide();

            statut = localStorage.getItem("statut");
            if (statut == "c") {
                afficherRechercheDevis();
                $("#rechercherUtilisateur").hide();
                $("#rechercherClient").hide();
                $("#rechercherAffichage").hide();

            } else {
                $("#rechercherUtilisateur").show();
                $("#rechercherClient").show();
                $("#rechercherAffichage").show();


                //Affiche les devis champs de recherche pour les devis
                $("#rechercherAffichage").click(function(e) {
                    if (document.getElementById("tableau") != null) {
                        document.getElementById("tableau").innerHTML = "";
                    }
                    afficherRechercheDevis();

                });

                //Affiche le champs de recherche pour les clients
                $("#rechercherClient").click(function(e) {
                    
                    if (document.getElementById("tableau") != null) {
                        document.getElementById("tableau").innerHTML = "";
                    }
                    $("#clientVieuw").show();
                    $("#affichageVieuw").hide();
                    $("#utilisateurVieuw").hide();
                    $("#pasDeClients").hide();

                    //Execute la recherche sur clients
                    $("#btnRechercheClient").unbind('click').click(function(e) {
                        $("#tableau").show();
                        e.preventDefault();
                        $("#spinDevis").show();

                        const data = {
                            nom: $("#nomClient").val(),
                            cp: $("#codePostal").val(),
                            ville: $("#ville").val(),
                        };
                        console.log(data);

                        getData("/client/rechercherClients", token, onGetRechercheClients, onError, data);
                    });
                });

                //Affiche le champs de recherche pour les utilisateurs
                $("#rechercherUtilisateur").click(function(e) {
                    if (document.getElementById("tableau") != null) {
                        document.getElementById("tableau").innerHTML = "";
                    }
                    $("#utilisateurVieuw").show();
                    $("#affichageVieuw").hide();
                    $("#clientVieuw").hide();
                    $("#pasDUtilisateurs").hide();

                    //Execute la recherche sur utilisateurs
                    $("#btnRechercheUtilisateur").unbind('click').click(function(e) {
                        e.preventDefault();
                        $("#tableau").show();
                        $("#spinDevis").show();

                        const data = {
                            nom: $("#nomUtil").val(),
                            ville: $("#villeU").val(),
                        };
                        console.log(data);
                        getData("/utilisateur", token, onGetRechercheUtilisateurs, onError, data);
                    });

                });
            }

        });
    });
});

//Affiche la table lorsque le get devis reussi
function onGetRechercheDevis(response) {
    $("#spinDevis").hide();
    if (response.success) {
        if (response.data.length > 0) {
            $("#pasDeDevis").hide();

            create_dynamic_HTML_table(
                "tableau",
                response.data,
                DEVIS_PROPERTIES,
                "Liste des devis"
            );
        } else {
            $("#tableau").hide();
            if (document.getElementById("tableau") != null) {
                document.getElementById("tableau").innerHTML = "";
            }
            $("#pasDeDevis").show();

        }
    };
}

//Fonction lorsque la recherche clients est reussi
function onGetRechercheClients(response) {
    $("#spinDevis").hide();
    if (response.success) {
        console.log("Liste : "+response.data.length);
        if (response.data.length > 0) {
            $("#pasDeClients").hide();

            create_dynamic_HTML_table(
                "tableau",
                response.data,
                CLIENT_PROPERTIES_DETAILLE,
                "Liste des clients"
            );

        } else {
            $("#tableau").hide();
            if (document.getElementById("tableau") != null) {
                document.getElementById("tableau").innerHTML = "";
            }
            $("#pasDeClients").show();
            
        }
    };
}

//Fonction lorsque la recherche utilisateurs est reussi
function onGetRechercheUtilisateurs(response) {
    $("#spinDevis").hide();
    if (response.success) {
        if (response.data.length > 0) {
            $("#pasDUtilisateurs").hide();

            create_dynamic_HTML_table(
                "tableau",
                response.data,
                UTILISATEUR_PROPERTIES_RECHERCHE,
                "Liste des utilisateurs"
            );

        } else {
            $("#tableau").hide();
            if (document.getElementById("tableau") != null) {
                document.getElementById("tableau").innerHTML = "";
            }
            $("#pasDUtilisateurs").show();
        }
    };
}
//Fonction lorsque la requete pour recuperer les amenagements reussi
function onGetAmenagement(response) {
    creerListeAmenagements(response, "amenagementDevis");
}

//Alert en cas d'erreur
function onError(err) {
    console.log(err);
    alert("echec");

}

function afficherRechercheDevis() {
    
    $("#affichageVieuw").show();
    $("#clientVieuw").hide();
    $("#utilisateurVieuw").hide();
    getData("/devis/amenagement", token, onGetAmenagement, onError);
    $("#pasDeDevis").hide();

    //Execute la recherche sur devis
    $("#btnRechercheDevis").unbind('click').click(function(e) {
        e.preventDefault();
        $("#spinDevis").show();
        $("#tableau").show();
        const data = {
            nom: $("#nom_client").val(),
            date: $("#dateDev").val(),
            amenagement: $("#amenagementDevis").val(),
            montantMin: $("#montantMin").val(),
            montantMax: $("#montantMax").val()
        };
        console.log(data);
        getData("/devis/rechercheDevis", token, onGetRechercheDevis, onError, data);
    });
}