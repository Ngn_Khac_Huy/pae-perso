import { showSlides } from './devis.js'

//Repris du cours de Javascript Q1 2019-2020 M. Baroni
//Cree un table en fonction des données
function create_dynamic_HTML_table(
    targetHtmlElementId,
    arrayToPrint,
    objectPropertiesToBeSaved,
    tableName
) {
    $("#spinDevis").hide();
    // Get the div container whithin we want to create a dynamic html table
    let div_container = document.getElementById(targetHtmlElementId);
    // Clear any content in the div_container
    div_container.innerHTML = "";
    let myTable = document.createElement("table");
    // Set the classes names with bootstrap
    myTable.className = "table boxPhoto shadow-sm p-3 mb-5 bg-white rounded";
    // Append the new empty table element to the div container
    let titre = document.createElement("h1");
    titre.innerText = tableName;

    div_container.appendChild(titre);
    div_container.appendChild(myTable);


    // Set the table header
    let headerLine = document.createElement("tr");
    myTable.appendChild(headerLine);
    const objectToPrint = arrayToPrint[0];
    var i;
    for (i = 0; i < objectPropertiesToBeSaved.length; i++) {
        let th = document.createElement("th");
        th.innerHTML = objectPropertiesToBeSaved[i];
        if(objectPropertiesToBeSaved[i]=="idClient"){
            th.innerHTML ="N° de devis";
        }
        if(objectPropertiesToBeSaved[i]=="nom"){
            th.innerHTML ="Nom";
        }
        if(objectPropertiesToBeSaved[i]=="prenom"){
            th.innerHTML ="Prénom";
        }
        if(objectPropertiesToBeSaved[i]=="email"){
            th.innerHTML ="Email";
        }
        if(objectPropertiesToBeSaved[i]=="cp"){
            th.innerHTML ="Code postal";
        }
        if(objectPropertiesToBeSaved[i]=="ville"){
            th.innerHTML ="Ville";
        }
        if(objectPropertiesToBeSaved[i]=="tel"){
            th.innerHTML ="N° de GSM";
        }
        if(objectPropertiesToBeSaved[i]=="estUtilisateur"){
            th.innerHTML ="Est un utilisateur";
        }
        if(objectPropertiesToBeSaved[i]=="devis client"){
            th.innerHTML ="Devis du client";
        }
        if(objectPropertiesToBeSaved[i]=="pseudo"){
            th.innerHTML ="Pseudo";
        }
        if(objectPropertiesToBeSaved[i]=="adresse"){
            th.innerHTML ="Adresse";
        }
        if(objectPropertiesToBeSaved[i]=="confirmation"){
            th.innerHTML ="Lier à un client";
        }
        if(objectPropertiesToBeSaved[i]=="statut"){
            th.innerHTML ="Statut";
        }
        if(objectPropertiesToBeSaved[i]=="dateInscription"){
            th.innerHTML ="Date d'inscription";
        }
        if(objectPropertiesToBeSaved[i]=="idDevis"){
            th.innerHTML ="N° du devis";
        }
        if(objectPropertiesToBeSaved[i]=="amenagements"){
            th.innerHTML ="Type d'aménagements";
        }
        if(objectPropertiesToBeSaved[i]=="etat"){
            th.innerHTML ="Etat du devis";
        }
        if(objectPropertiesToBeSaved[i]=="dateDevis"){
            th.innerHTML ="Date du devis";
        }
        if(objectPropertiesToBeSaved[i]=="dateDebutTravaux"){
            th.innerHTML ="Date du début des travaux";
        }
        if(objectPropertiesToBeSaved[i]=="montant"){
            th.innerHTML ="Montant\n(en €)";
        }
        if(objectPropertiesToBeSaved[i]=="duree"){
            th.innerHTML ="Durée \n(en j.)";
        }
        if(objectPropertiesToBeSaved[i]=="nomClient"){
            th.innerHTML ="Nom du client";
        }
        if(objectPropertiesToBeSaved[i]=="photoFavorite" ||objectPropertiesToBeSaved[i]=="photoFavorite2" ){
            th.innerHTML ="Photo préférée";
        }
        if(objectPropertiesToBeSaved[i]=="info devis"){
            th.innerHTML ="Info +";
        }
        
        headerLine.appendChild(th);
    }

    // Contenu de la table en fonctions des données qui sont recue par la requete
    let idDevis;
    let idClient
    for (let x = 0; x < arrayToPrint.length; x++) {

        let myLine = document.createElement("tr");
        myTable.appendChild(myLine);
        const objectToPrint = arrayToPrint[x];

        for (i = 0; i < objectPropertiesToBeSaved.length; i++) {

            let myCell = document.createElement("td");
            myCell.innerText = objectToPrint[objectPropertiesToBeSaved[i]];

            //Affiche la date avec le bon ordre 
            if (objectPropertiesToBeSaved[i] == "dateDevis" || objectPropertiesToBeSaved[i] == "dateDebutTravaux" || objectPropertiesToBeSaved[i] == "dateInscription") {
                if (objectToPrint[objectPropertiesToBeSaved[i]] != null) {
                    myCell.innerText = objectToPrint[objectPropertiesToBeSaved[i]]["dayOfMonth"] + "/" + objectToPrint[objectPropertiesToBeSaved[i]]["monthValue"] + "/" + objectToPrint[objectPropertiesToBeSaved[i]]["year"];
                }
            }
            //Ajoute le formulaire pour introduire un nom de client
            if (objectPropertiesToBeSaved[i] == "confirmation") {
                myCell.innerText = "";
                let form = document.createElement("form");
                form.setAttribute("id", "confirmationForm_" + objectToPrint["idUtilisateur"]);
                form.setAttribute("autocomplete", "off");
                let div_container2 = document.createElement("div");
                div_container2.className = "autocomplete";
                let inputForm = document.createElement("input");
                inputForm.setAttribute("id", objectToPrint["idUtilisateur"]);
                inputForm.type = "text";
                let inputHidden = document.createElement("input");
                inputHidden.setAttribute("id", "idClient_" + objectToPrint["idUtilisateur"]);
                inputHidden.type = "hidden";
                let inputSubmit = document.createElement("button");
                inputSubmit.innerHTML = "Lier client";
                inputSubmit.type = "submit";
                inputSubmit.setAttribute("id", "boutonLierClient_" + objectToPrint["idUtilisateur"]);
                inputSubmit.setAttribute("value", objectToPrint["idUtilisateur"]);
                inputSubmit.setAttribute("class", "boutonLierClient btn btn-info");
                div_container2.appendChild(inputForm);
                form.appendChild(inputHidden);
                form.appendChild(div_container2);
                form.appendChild(inputSubmit);
                myCell.appendChild(form);
            }

            //ajoute le bouton pour voir un devis
            if (objectPropertiesToBeSaved[i] == "info devis") {
                myCell.innerText = "";

                let form = document.createElement("form")
                form.setAttribute("action", "/");
                form.setAttribute("method", "GET");
                let inputSubmit = document.createElement("button");
                inputSubmit.type = "button";
                inputSubmit.innerHTML = "?";
                inputSubmit.setAttribute("id", "boutonPlusInfo" + idDevis);
                inputSubmit.setAttribute("class", "boutonPlusInfo btn btn-info");

                inputSubmit.value = idDevis;

                form.appendChild(inputSubmit);
                myCell.appendChild(form);
            }

            //ajoute le bouton pour voir les devis d'un client
            if (objectPropertiesToBeSaved[i] == "devis client") {
                myCell.innerText = "";

                let form = document.createElement("form")
                form.setAttribute("action", "/");
                form.setAttribute("method", "GET");
                let inputSubmit = document.createElement("button");
                inputSubmit.type = "button";
                inputSubmit.innerHTML = "?";
                inputSubmit.setAttribute("id", "boutonDevisClient" + idClient);
                inputSubmit.setAttribute("class", "boutonDevisClient btn btn-info");

                inputSubmit.value = idClient;

                form.appendChild(inputSubmit);
                myCell.appendChild(form);
            }


            //Affiche si le client est utilisateur
            if (objectPropertiesToBeSaved[i] == "estUtilisateur") {
                myCell.innerText = "";
                if (objectToPrint[objectPropertiesToBeSaved[i]]) {
                    myCell.setAttribute("class", "row btn-success");
                } else {
                    myCell.setAttribute("class", "row btn-danger");
                }
            }

            if (objectPropertiesToBeSaved[i] == "idClient") {
                idClient = objectToPrint[objectPropertiesToBeSaved[i]];
            }

            if (objectPropertiesToBeSaved[i] == "idDevis") {
                idDevis = objectToPrint[objectPropertiesToBeSaved[i]];
            }

            //Affiche la photo favorite
            if (objectPropertiesToBeSaved[i] == "photoFavorite"  ) {
                myCell.innerText = "";
                
                   var photoData=objectToPrint["photoFavorite"];

                if (photoData.length > 100) {
                    let imgDevis = document.createElement("img");
                    imgDevis.setAttribute("src", photoData);
                    imgDevis.className = "photofav";
                    myCell.appendChild(imgDevis);
                } else {
                    myCell.innerText = "A définir";
                }
            }
            //Affiche la photo favorite
            if (objectPropertiesToBeSaved[i] == "photoFavorite2" ) {
                myCell.innerText = "";
                
                   var photoData=objectToPrint["photoFavorite2"];

                if (photoData.length > 100) {
                    let imgDevis = document.createElement("img");
                    imgDevis.setAttribute("src", photoData);
                    imgDevis.className = "photofav";
                    myCell.appendChild(imgDevis);
                } else {
                    myCell.innerText = "A définir";
                }
            }
            if (objectPropertiesToBeSaved[i] == "adresse") {
                let adresse = objectToPrint["rue"] + " " + objectToPrint["numero"];
                myCell.innerText = adresse;
            }
            if (objectPropertiesToBeSaved[i] == "amenagements") {}

            myLine.appendChild(myCell);

        }
    }
}

//Repris de  w3schools.com
//Fais un autocomplete pour introduire un devis et lier un client
function autocomplete(inp, arr, idUtilisateur, idClientHidden) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/

    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if ((arr[i]["nom"] + " " + arr[i]["prenom"]).substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + (arr[i]["nom"] + " " + arr[i]["prenom"]).substr(0, val.length) + "</strong>";
                b.innerHTML += (arr[i]["nom"] + " " + arr[i]["prenom"]).substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + (arr[i]["nom"] + " " + arr[i]["prenom"]) + "'>";
                b.innerHTML += "<input type='hidden' id='idClient" + idUtilisateur + "' value='" + arr[i]["idClient"] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    idClientHidden.value = this.getElementsByTagName("input")[1].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function(e) {
        closeAllLists(e.target);
    });
}

//Creer le bouton 
function creerUnbouton(etat, writeOnThebutton, targetHtmlElementId, htmlIdNew) {
    let div_container = document.getElementById(targetHtmlElementId);
    let button = document.createElement("button");
    button.value = etat;
    button.innerText = writeOnThebutton;
    button.id = htmlIdNew;
    button.classList.add("btn", "btn-info");
    button.setAttribute("style", "margin-right:2%;");
    //button.className="btn-info"
    div_container.appendChild(button);
}



//Ajoute le champ pour ajoputer les photos apres amenagement dans le devis
function ajoutChampPhotoAvecFav(response, elementHTML) {
    var div = document.getElementById(elementHTML);
    var form = document.createElement("form");
    form.id = "ajoutPhotoAmen";
    form.setAttribute("method", "post");
    var amenagements = response.data[0]["amenagements"];
    var select = document.createElement("select");
    select.id = "choixAmenPhoto";
    select.className = "choixAmenPhotoDevis form-control col";
    var elem = document.createElement("option");
    elem.textContent = " ";
    elem.value = "";
    select.appendChild(elem);
    for (var i = 0; i < amenagements.length; i++) {
        var opt = amenagements[i];
        var el = document.createElement("option");

        el.textContent = opt;
        el.value = opt;
        select.appendChild(el);
    }
    

    var divFormGroup = document.createElement("form");
    divFormGroup.id = "inputPhoto";
    divFormGroup.className = "copierPhoto row boxPhoto shadow-sm p-3 mb-5 bg-white rounded "

    var divFormGroupUpload = document.createElement("div");
    divFormGroupUpload.className = "custom-file col"

    var input = document.createElement("input");
    input.setAttribute("type", "file");
    input.id = "upFichier";
    input.className = "uploadImageDevis custom-file-input";
    input.setAttribute("name", "photoAmen");

    var labelInput = document.createElement("label");
    labelInput.setAttribute("for", "upFichier");
    labelInput.innerHTML = " Choisir un fichier ";
    labelInput.className = "custom-file-label";
    divFormGroupUpload.appendChild(input);
    divFormGroupUpload.appendChild(labelInput);


    
    var divFormGroup1= document.createElement("div");
    divFormGroup1.className="Checkbox ";

    var inputCheck = document.createElement("input");
    inputCheck.setAttribute("type", "checkbox");
    inputCheck.className = "checkboxFav ";
    inputCheck.id = "favori";

    var labelChheck = document.createElement("label");
    labelChheck.setAttribute("for", "favori");
    labelChheck.innerHTML = " ❤️ ";

    var divCheck1 = document.createElement("p");
    divCheck1.className="Checkbox-visible";
    divFormGroup1.appendChild(inputCheck);
    divFormGroup1.appendChild(divCheck1);

    var divFormGroup2 = document.createElement("div");
    divFormGroup2.className="Checkbox ";


    var inputCheck2 = document.createElement("input");
    inputCheck2.setAttribute("type", "checkbox");
    inputCheck2.className = "checkboxVis  ";
    inputCheck2.id = "Visible";

    var labelChheck2 = document.createElement("p");
    labelChheck2.setAttribute("for", "Visible");
    labelChheck2.innerHTML = " Visible ";

    var divCheck2 = document.createElement("div");
    divCheck2.className="Checkbox-visible";
    divFormGroup2.appendChild(inputCheck2);
    divFormGroup2.appendChild(divCheck2);

    var btn = document.createElement("div");
    btn.id = "ajoutChampPhotoDevis";
    btn.className = "btn btn-info";
    btn.innerHTML = "Ajouter une photo";

    divFormGroup.appendChild(select);
    divFormGroup.appendChild(divFormGroupUpload);
    divFormGroup.appendChild(divFormGroup1);
    divFormGroup.appendChild(labelChheck)
    divFormGroup.appendChild(divFormGroup2);
    divFormGroup.appendChild(labelChheck2)
    form.appendChild(divFormGroup);
    form.appendChild(btn);

    div.appendChild(form);
}

//affiche le bouton qui correspond à l'etat
function gererAffichageEtatBouton(etat, targetHtml, response) {
    if (etat === "Devis introduit") {
        creerUnbouton("Commande confirmée", "Confirmer la demande d'aménagement", targetHtml, "changerEtat1");
        creerUnbouton("Annulé", "Annuler le devis", targetHtml, "changerEtat2");
        $("#dateDiv").show();
        $("#dateReporter").hide();
        $("#dateIntroduire").show();
    }
    if (etat === "Commande confirmée") {
        creerUnbouton("Date de début confirmée", "Confirmer la date de début des travaux", targetHtml, "changerEtat1");
        creerUnbouton("Annulé", "Annuler le devis", targetHtml, "changerEtat2");
        $("#dateDiv").show();
        $("#dateReporter").show();
        $("#dateIntroduire").hide();
    }
    if (etat === "Date de début confirmée") {
        creerUnbouton("Facture de fin de chantier envoyée", "Facture de fin de chantier envoyée", targetHtml, "changerEtat1");
        creerUnbouton("Facture de milieu de travaux envoyée", "Facture de milieu de travaux envoyée", targetHtml, "changerEtat2");
        creerUnbouton("Annulé", "Annuler le devis", targetHtml, "changerEtat3");
    }
    if (etat === "Facture de milieu de travaux envoyée") {
        creerUnbouton("Facture de fin de chantier envoyée", "Facture de fin de chantier envoyée", targetHtml, "changerEtat1");
    }
    if (etat === "Facture de fin de chantier envoyée") {

        ajoutChampPhotoAvecFav(response, "formAjoutPhotoDevis");
        creerUnbouton("Visible", "Rendre la réalisation visible", targetHtml, "changerEtat1");
    }
    if (etat === "Visible") {

        ajoutChampPhotoAvecFav(response, "formAjoutPhotoDevis");
        creerUnbouton("Visible", "Ajouter les photos", targetHtml, "ajoutPhotoApresVisible");
    }
}

//creer les slideshows dans la page du devis
function afficheSlideshowDevis(response, elemHTMLAvant, elemHTMLApres) {
    //Affiche les photos
    var devisAVant = document.getElementById(elemHTMLAvant);
    var devisApres = document.getElementById(elemHTMLApres);
    var photos = response.data[2];
    var nbrPhotoAvant = 0;
    var nbrPhotoApres = 0;
    console.log(photos.length);

    for (var i = 0; i < photos.length; i++) {
        var opt = photos[i]["photoData"];
        var div1 = document.createElement("div");
        var divTxt = document.createElement("div");
        divTxt.className = "numbertext";
        console.log(photos[i]["estVisible"])
        if(photos[i]["estVisible"]){
            divTxt.innerHTML = "Visible";
        }else{
            divTxt.innerHTML = "Pas visible";
        }
        
        var img = document.createElement("img");
        img.setAttribute("src", opt);
        img.setAttribute("style", "width:100%");

        div1.appendChild(divTxt);
        div1.appendChild(img);
        if (photos[i]["idAmenagement"] > 0) {
            nbrPhotoApres++;
            div1.className = "mySlidesApres fade";
            devisApres.appendChild(div1);

        } else {
            nbrPhotoAvant++;
            div1.className = "mySlidesAvant fade";
            devisAVant.appendChild(div1);
        }
    }
    if (nbrPhotoAvant > 0) {
        showSlides(1, "mySlidesAvant");
    } else {
        $("#photoAvantContainer").html("Pas de photos ajoutées")
    }
    if (nbrPhotoApres > 0) {
        showSlides(1, "mySlidesApres");
    } else {
        $("#photoApresContainer").html("Pas de photos ajoutées")
    }


}

//creer le slideshow page d'acceuil
function afficheSlideshowAcceuil(response, elemHTML) {
    var select = document.getElementById(elemHTML);
    select.innerHTML = "";
    var options = response.data;
    $("#spin").hide();
    for (var i = 0; i < options.length; i++) {
        var opt = options[i]["photoData"];
        var div1 = document.createElement("div");
        var divTxt = document.createElement("div");
        divTxt.className = " txtSurPhotoAcceuil";
        divTxt.innerHTML = options[i]["amenagement"];
        div1.className = "mySlides1 fade";
        var img = document.createElement("img");
        img.setAttribute("src", opt);
        img.setAttribute("style", "width:100%");
        div1.appendChild(divTxt);
        div1.appendChild(img);
        select.appendChild(div1);

    }

}

//Permet de creer une liste d'amenagement de manieres dynamiques
function creerListeAmenagements(response, elementHTML) {
    var select = document.getElementById(elementHTML);
    var options = response.listeA;
    var ele = document.createElement("option");
    ele.textContent = "";
    ele.value = "";
    select.appendChild(ele);

    for (var i = 0; i < options.length; i++) {
        var opt = options[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        select.appendChild(el);
    }
}

//Permet de creer une liste d'amenagement de manieres dynamiques avec des bouton pour le menu
function creerListeAmenagementsAvecBouton(response, elementHTML) {
    var select = document.getElementById(elementHTML);
    var options = response.listeA;

    for (var i = 0; i < options.length; i++) {
        var opt = options[i];
        var el = document.createElement("a");
        el.textContent = opt;
        el.value = opt;
        el.className = "choixAmen dropdown-item";
        select.appendChild(el);
    }
}





export {
    create_dynamic_HTML_table,
    autocomplete,
    creerUnbouton,
    ajoutChampPhotoAvecFav,
    gererAffichageEtatBouton,
    afficheSlideshowDevis,
    afficheSlideshowAcceuil,
    creerListeAmenagementsAvecBouton,
    creerListeAmenagements
};