
function postData(url = "", data = {}, token, onPost, onError) {
    let headers;
    console.log("post");
    if (token)
    headers = {
        //Authorization: "Bearer " + token
        Authorization: token
    };
else
    headers = {
    };
    $.ajax({
        headers:headers,
        type: "post",
        url: url,
        data: data,
        dataType: "json",
        success: onPost,
        error: onError,
    });
}

function getData(url = "", token, onGet, onError,data={}) {
    let headers;
    if (token)
        headers = {
            "Content-Type": "application/json",
            Authorization: token
        };
    else
        headers = {
            "Content-Type": "application/json"
        };
    $.ajax({
        type: "get",
        url: url,
        data:data,
        headers: headers,
        dataType: "json",
        success: onGet,
        error: onError
    });

}


//Envoie les données pour lier un client
function lierClient(listeU) {
    let token = localStorage.getItem("token");
    for (const element of listeU) {
        let idClient = $("#idClient_" + element["idUtilisateur"]).val();
        if (idClient.length != 0) {
            let headers;
            if (token)
                headers = {
                    Authorization: token
                };
            else
                headers = {
                };
            $.ajax({
                ContentType: "application/json",
                type: "post",
                url: "/client",
                headers: headers,
                data: { idClient: $("#idClient_" + element["idUtilisateur"]).val(), idUtilisateurALier: $("#boutonLierClient_" + element["idUtilisateur"]).val() },
                dataType: "json",
                success: function (response) {

                    alert("Ajout effectué");


                },
                error: function name(err, statut, message) {

                    console.log("Error :", statut, "Messsage:", message, "json : ", err["responseText"]);


                }
            });
        }
    }
}

function confirmerDate() {
    let token = localStorage.getItem("token");
    let headers;
    if (token)
        headers = {
            //Authorization: "Bearer " + token
            Authorization: token
        };
    else
        headers = {
        };

    $.ajax({


        type: "put",
        url: "/devis",
        headers: headers,
        data: { id: $("#idDevis").val()},
        dataType: "json",
        success: function (response) {
            alert("Date confirmé");
        },
        error: function name(err, statut, message) {

            console.log("Error :", statut, "Messsage:", message, "json : ", err["responseText"]);
            location.reload();


        }
    });

}






export { getData, lierClient, confirmerDate, postData };