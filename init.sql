
DROP SCHEMA IF EXISTS projet CASCADE;

CREATE SCHEMA projet;

--CREATE TABLE

CREATE TABLE projet.clients (
  id_client SERIAL PRIMARY KEY,
  nom VARCHAR(30) NOT NULL,
  prenom VARCHAR(30) NOT NULL,
  rue VARCHAR(100) NOT NULL,
  numero INTEGER NOT NULL,
  boite VARCHAR(10) NULL,		
  cp INTEGER NOT NULL,
  ville VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  tel VARCHAR(10) NOT NULL
);

CREATE TABLE projet.utilisateurs(
  id_utilisateur SERIAL PRIMARY KEY,
  email VARCHAR(100) NOT NULL,
  mdp VARCHAR(100) NOT NULL,
  nom VARCHAR(100) NOT NULL,
  prenom VARCHAR(30) NOT NULL,
  ville VARCHAR(50) NOT NULL,
  --statut est un caractère: 'o' si ouvrier , 'u' si utilisateur
  statut VARCHAR(1) NOT NULL,
  date_inscription DATE NOT NULL,
  id_client INTEGER NULL,
  pseudo VARCHAR(50) NOT NULL
);

CREATE TABLE projet.devis(
  id_devis SERIAL PRIMARY KEY,
  etat VARCHAR(50) NOT NULL,
  date_devis DATE NOT NULL,
  date_debut_travaux DATE NULL,
  montant FLOAT NOT NULL,
  duree INTEGER NOT NULL,
  id_client INTEGER NOT NULL REFERENCES projet.clients(id_client)
  
);

CREATE TABLE projet.types_amenagements(
  id_amenagement SERIAL PRIMARY KEY,
  nom VARCHAR(100) NOT NULL,
  description varchar(400) NOT NULL
);

CREATE TABLE projet.travaux(
  id_travail SERIAL PRIMARY KEY,
  id_devis INTEGER NOT NULL REFERENCES projet.devis(id_devis),
  id_amenagement INTEGER NOT NULL REFERENCES projet.types_amenagements(id_amenagement)
);
CREATE TABLE projet.photos(
  id_photo SERIAL PRIMARY KEY,
  photo TEXT NOT NULL,
  id_devis INTEGER NOT NULL REFERENCES projet.devis(id_devis),
  id_amenagement INTEGER NULL REFERENCES projet.types_amenagements(id_amenagement),
  est_visible BOOLEAN NOT NULL	
);
ALTER TABLE projet.devis ADD COLUMN photo_favorite INTEGER NULL REFERENCES projet.photos(id_photo);

--Ajout des aménagements
INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Aménagement de jardin de ville','Aménagement de jardin de ville');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Aménagement de jardin','Aménagement de jardin');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Aménagements de parc paysagiste','Aménagements de parc paysagiste');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Création de potagers','Création de potagers');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Entretien de vergers haute-tige','Entretien de vergers haute-tige');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Entretien de vergers basse-tige','Entretien de vergers basse-tige');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Aménagement d’étang','Aménagement d’étang');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Installation de système d’arrosage automatique','Installation de système d’arrosage automatique');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Terrasses en bois','Terrasses en bois');

INSERT INTO projet.types_amenagements(id_amenagement,nom,description)
VALUES (DEFAULT, 'Terrasses en pierres naturelles','Terrasses en pierres naturelles');

--Ajout des clients
INSERT INTO projet.clients(id_client,nom,prenom,rue,numero,boite,cp,ville,email,tel)
VALUES (DEFAULT,'Line','Caroline','Rue de l''Eglise',11,null,4987,'Stoumont','caro.line@hotmail.com','080125678');

INSERT INTO projet.clients(id_client,nom,prenom,rue,numero,boite,cp,ville,email,tel)
VALUES (DEFAULT,'Ile','Théophile','Rue de Renkin',7,null,4800,'Verviers','theo.phile@proximus.be','087256974');

INSERT INTO projet.clients(id_client,nom,prenom,rue,numero,boite,cp,ville,email,tel)
VALUES (DEFAULT,'Ile','Basile','rue des Minières',45,null,4800,'Verviers','bas.ile@gmail.com','087123456');

--Ajout des ouvriers
INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
 VALUES(DEFAULT,'brigitte.lehman@vinci.be','$2a$10$Enh39EoVoiEoQ8o5mQwm1OyoBbgaH8gHvIwpPMsJaXXbnrWLNkH8.','Lehman','Brigitte','Wavre','o','2020-03-31',null,'bri' );

 INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
 VALUES(DEFAULT,'laurent.leleux@vinci.be','$2a$10$Ctvm6XDCLeTj9caSuDWl2O7MpUGAW7dZIcYRiCSJpHdbO3gW7I/I2','Leleux','Laurent','Bruxelles','o','2020-03-31',null,'lau' );

 INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
 VALUES(DEFAULT,'christophe.damas@vinci.be','$2a$10$7UtPeufWEq9ZpbfaZRFpHe5f0cItKUc0IhUmJ91DOAE0FwXlAUxrm','Damas','Christophe','Bruxelles','o','2020-03-31',null,'chri' );

--Ajout des utilisateurs
INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
VALUES(DEFAULT,'theo.phil@proximus.be','$2a$10$a.XgIL7jFI.DLh0xQfM1Vu7yk/TsgxtY49N1wRTB6CkjpHIkykV3C','Ile','Théophile','Verviers','c','2020-05-02',2,'theo');

INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
VALUES(DEFAULT,'ach.ile@gmail.com','$2a$10$UWPtFIs3xMDtwJOzW1/sQeZ9vLjRoQEnMlOk8t2EpQsI06jiGHMPS','Ile','Achile','Verviers','u','2020-03-31',null,'achil' );

INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
VALUES(DEFAULT,'bas.ile@gmail.com','$2a$10$UWPtFIs3xMDtwJOzW1/sQeZ9vLjRoQEnMlOk8t2EpQsI06jiGHMPS','Ile','Basile','Verviers','c','2020-03-31',3,'bazz' );

INSERT INTO projet.utilisateurs(id_utilisateur, email, mdp, nom, prenom, ville, statut,date_inscription, id_client, pseudo)
VALUES(DEFAULT,'caro.line@hotmail.com','$2a$10$UWPtFIs3xMDtwJOzW1/sQeZ9vLjRoQEnMlOk8t2EpQsI06jiGHMPS','Line','Caroline','Stoumont','c','2020-03-31',1,'caro' );

--Ajout des devis
--INSERT INTO projet.devis(id_devis,etat,date_devis,date_debut_travaux,montant,duree,id_client)
--VALUES(DEFAULT,'Visible','2018-11-12','2019-03-01',4260,5,1);

--INSERT INTO projet.devis(id_devis,etat,date_devis,date_debut_travaux,montant,duree,id_client)
--VALUES(DEFAULT,'Visible','2018-12-15','2019-03-15',18306,25,1);

--INSERT INTO projet.devis(id_devis,etat,date_devis,date_debut_travaux,montant,duree,id_client)
--VALUES(DEFAULT,'Commande confirmée','2019-11-12','2020-03-30',8540,10,1);

--INSERT INTO projet.devis(id_devis,etat,date_devis,date_debut_travaux,montant,duree,id_client)
--VALUES(DEFAULT,'Facture de fin de chantier envoyée','2020-01-10','2020-03-02',6123,7,2);

--INSERT INTO projet.travaux(id_travail,id_devis,id_amenagement)
--VALUES(DEFAULT,1,6);

--INSERT INTO projet.travaux(id_travail,id_devis,id_amenagement)
--VALUES(DEFAULT,2,5);

--INSERT INTO projet.travaux(id_travail,id_devis,id_amenagement)
--VALUES(DEFAULT,3,3);

--INSERT INTO projet.travaux(id_travail,id_devis,id_amenagement)
--VALUES(DEFAULT,4,1);

--INSERT INTO projet.travaux(id_travail,id_devis,id_amenagement)
--VALUES(DEFAULT,4,8);

SELECT de.id_devis ,cl.nom ,cl.prenom,de.etat,ta.nom AS type_amenagement  
FROM projet.devis de, projet.types_amenagements ta, projet.clients cl,projet.travaux tx
WHERE de.id_devis = tx.id_devis AND cl.id_client = de.id_client AND tx.id_amenagement = ta.id_amenagement